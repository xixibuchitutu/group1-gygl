(function ($) {
    $.verificationItem = function(t,p){
        var  options = $.extend(true, {

        }, p);
        var reg = $(t).attr("reg");
        if(reg) {
            var regexp = new RegExp(reg);
            if (regexp.test($(t).val())) {
                return true;
            } else {
                $(t).addClass("ui-state-error");
                $(t).focus();
                return false;
            }
        }

    };

    $.fn.verification = function (p) {
        this.removeClass("ui-state-error");
        var flag = true;
        this.each(function (index,item) {
            flag &= $.verificationItem(item,p);
        });
        return flag;

    };

    $.fn.getFlexigridiAttrByIndex=function(findIndex) {
        var descList = [];
        var  selectedDom = $(this).find(".trSelected");
        selectedDom.each(function (index,item) {
            var ch = $(item).attr("ch");
            var atttrs = ch.split("_FG$SP_");
            $(atttrs).each(function (indexCh,itemCh) {
                if(indexCh === findIndex){
                    descList.push(itemCh)
                }
            });

        });
        return descList;
    };
    $.fn.tableEditData = function (p) {
        var p = $.extend({
            width : 400,
            height:400,
            url:null,
            extParam:[],
            colModel : []
        },p);
        if(this.length < 1){
            return;
        }
        var t = this[0];
        t.p = p;

        var table = $("<table></table>");

        $(table).css("width",p.width + "px");
        var thTR = $("<tr></tr>");
        thTR.addClass("gqgl-tb-th");
        for(var index in p.colModel){
            var th = $("<th></th>")
            var item = p.colModel[index];
            th.text(item.display);
            th.css("width",item.width + "px");
            if(item.hide && item.hide === 'true'){
                th.css("display","none");
            }
            th.appendTo(thTR);
            //console.info(item);
        }
        thTR.appendTo(table);

        t.table = table;
        $(this).before(table);
    };
    $.fn.tableEditOptions = function (p) {
        if(this.length < 1){
            return;
        }
        var t = this[0];
        $.extend(t.p,p);
        return this;
    };

    $.fn.tableEditReload = function () {
        if(this.length < 1){
            return;
        }
        var that = this;
        var tableId = this.attr("id");

        var t = this[0];
        var p = t.p;
        that.html("");
        $.ajax({
            type: 'post',
            async: false,
            dataType : 'json',
            url: p.url,
            data: p.extParam,
            success: function (data) {
                var tr = $("<tr></tr>");
                $(data).each(function (key,itemTr) {
                    tr = $("<tr></tr>");
                    if((key % 2) == 0){
                        tr.addClass("table-erow");
                    }
                    var trid = tableId + "-tr-" + key;
                    tr.attr("id",trid);
                    for(var index in p.colModel){
                        var td = $("<td></td>")
                        var itemCl = p.colModel[index];
                        var vl = itemTr[itemCl.name];

                        if(itemCl.process) {
                            td.html(itemCl.process(vl, trid, itemTr));
                        }else if(itemCl.dom){
                            itemCl.dom(td,vl, trid, itemTr)
                        }else{
                            var span = $("<span></span>");
                            span.text(vl);
                            span.appendTo(td);
                            span.addClass("read");

                            var input=$("<input type='text'>");
                            input.attr("name",itemCl.name);
                            input.appendTo(td);
                            input.val(vl);
                            input.addClass("edit");
                        }


                        td.css("width",itemCl.width + "px");
                        if(itemCl.hide && itemCl.hide === 'true'){
                            td.css("display","none");
                        }
                        td.appendTo(tr);
                    }
                    tr.appendTo(that);
                });

                tr = $("<tr></tr>");
                tr.addClass("table-edit-tr");
                var key = data.length + 1;
                var trid = tableId + "-tr-" + key;
                tr.attr("id",trid);
                for(var index in p.colModel){
                    var td = $("<td></td>")
                    var itemCl = p.colModel[index];

                    if(itemCl.process){
                        td.html(itemCl.process('',trid,null));
                    }else if(itemCl.dom){
                        itemCl.dom(td,'', trid, null)
                    }else{

                        var input=$("<input type='text'>");
                        input.attr("name",itemCl.name);
                        input.appendTo(td);
                        input.addClass("edit");
                    }
                    td.css("width",itemCl.width + "px");
                    if(itemCl.hide && itemCl.hide === 'true'){
                        td.css("display","none");
                    }
                    td.appendTo(tr);
                }
                tr.appendTo(that);
            },
            error: function(msg){
                message(' <spring:message code="common.error"></spring:message>');
            }
        });

        return this;
    };

    $.fn.cloneObj = function () {
        var objList = this;
        var newList = objList.clone();
        for(var i = 0 ; i  < objList.length; i ++){
            $(newList[i]).val($(objList[i]).val());
            $(newList[i]).removeAttr("disabled");
        }
        return newList;
    }

})(jQuery);