/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 5.7.26 : Database - gqgy_data
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`gqgy_data` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `gqgy_data`;

/*Table structure for table `capacity_electric` */

DROP TABLE IF EXISTS `capacity_electric`;

CREATE TABLE `capacity_electric` (
  `elc_id` int(11) NOT NULL,
  `elc_date` date DEFAULT NULL,
  `elc_capacity` double DEFAULT NULL COMMENT '全国发电量',
  `elc_grew` double DEFAULT NULL COMMENT '同比',
  PRIMARY KEY (`elc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='全国发电量';

/*Data for the table `capacity_electric` */

insert  into `capacity_electric`(`elc_id`,`elc_date`,`elc_capacity`,`elc_grew`) values 
(1,'2020-02-20',8231,4.5),
(2,'2019-02-22',3213,2.34),
(3,'2017-03-17',4312,6.4),
(4,'2018-11-22',231,6.3),
(5,'2018-09-07',3241,2.1),
(6,'2019-01-24',5324,5.4),
(7,'2020-04-30',5444,2.3),
(8,'2018-09-26',6425,6.4),
(9,'2017-08-16',2356,2.3),
(10,'2018-02-15',6432,6.4),
(11,'2017-12-26',2231,9.5),
(12,'2020-05-20',1231,13.2),
(13,'2020-07-20',6425,24),
(14,'2019-08-22',1231,1.2),
(15,'2019-09-12',5322,5.8),
(16,'2020-07-01',532,5),
(17,'2019-06-09',123,4.4),
(18,'2018-02-11',421,6.5),
(19,'2018-03-17',5423,2.2),
(20,'2018-04-27',2332,3.5),
(21,'2008-07-09',111,6.5),
(22,'2020-05-14',5341,8.3),
(23,'2020-05-31',531,3.3),
(24,'2020-07-06',6432,5.3),
(25,'2019-06-18',2341,6.43),
(26,'2020-03-03',8654,1.3),
(27,'2020-03-26',4567,4.3),
(28,'2018-08-29',6543,2.3),
(29,'2018-07-18',3524,6.4),
(30,'2018-07-24',6534,7.4);

/*Table structure for table `cement_production` */

DROP TABLE IF EXISTS `cement_production`;

CREATE TABLE `cement_production` (
  `cement_id` int(11) NOT NULL,
  `cement_date` date DEFAULT NULL,
  `cement_production` double DEFAULT NULL,
  `cement_compare` double DEFAULT NULL,
  PRIMARY KEY (`cement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='水泥产量表';

/*Data for the table `cement_production` */

insert  into `cement_production`(`cement_id`,`cement_date`,`cement_production`,`cement_compare`) values 
(1,'2019-09-30',21764.6,4.1),
(2,'2019-08-31',21018.2,5.1),
(3,'2019-07-31',21003,7.5),
(4,'2019-06-30',20985.9,6),
(5,'2019-05-31',22696.2,7.2),
(6,'2019-04-30',21345.4,3.4),
(7,'2019-03-31',17971.1,22.2),
(8,'2018-12-31',18394,4.3),
(9,'2018-11-30',20520.6,1.6),
(10,'2018-10-31',22043.2,13.1),
(11,'2018-09-30',20780.6,5),
(12,'2018-08-31',20018.1,5),
(13,'2018-07-31',19618.1,1.6),
(14,'2018-06-30',20009.5,0),
(15,'2018-05-31',21520.5,1.9),
(16,'2018-04-30',21072.2,3.2),
(17,'2018-03-31',15442.7,-15.6),
(18,'2017-12-31',19144.1,-2.2),
(19,'2017-11-30',22051.3,4.8),
(20,'2017-10-31',21989.6,-3.1),
(21,'2017-09-30',22139.7,-2),
(22,'2017-08-31',21184.8,-3.7),
(23,'2017-07-31',21283.4,-0.9),
(24,'2017-06-30',22080.9,-0.9),
(25,'2017-05-31',22845.8,0.5),
(26,'2017-04-30',22112.8,2.4),
(27,'2017-03-31',20162.4,0.3),
(28,'2016-12-31',19979,-1.2),
(29,'2016-11-30',21350.8,3.7),
(30,'2016-10-31',22783.3,3),
(31,'2016-09-30',22392.7,2.9),
(32,'2016-08-31',21771.9,1),
(33,'2016-07-31',21410.2,0.9),
(34,'2016-06-30',22277.6,2.6),
(35,'2016-05-31',22660.5,2.9),
(36,'2016-04-30',21626,2.8),
(37,'2016-03-31',20138.6,24),
(38,'2016-02-29',23974.9,-8.2),
(39,'2015-12-31',19800,-3),
(40,'2015-11-30',20494.3,-6.6);

/*Table structure for table `coal_bspi` */

DROP TABLE IF EXISTS `coal_bspi`;

CREATE TABLE `coal_bspi` (
  `bspi_id` int(11) NOT NULL,
  `bspi_date` date DEFAULT NULL,
  `bspi_bspi` double DEFAULT NULL,
  `bspi_hbzj` double DEFAULT NULL,
  `bspi_hb` double DEFAULT NULL,
  `bspi_tbzj` double DEFAULT NULL,
  `bspi_tb` double DEFAULT NULL,
  PRIMARY KEY (`bspi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='煤炭BSPI表';

/*Data for the table `coal_bspi` */

insert  into `coal_bspi`(`bspi_id`,`bspi_date`,`bspi_bspi`,`bspi_hbzj`,`bspi_hb`,`bspi_tbzj`,`bspi_tb`) values 
(1,'2020-07-08',558,5,0.9,-14,-2.45),
(2,'2020-07-04',553,1,0.18,-18,-3.15),
(3,'2020-07-01',552,0,0,-18,-3.16),
(4,'2020-06-27',552,1,0.18,NULL,NULL),
(5,'2020-06-23',551,1,0.18,-18,-3.16),
(6,'2020-06-19',550,0,0,-20,-3.15),
(7,'2020-06-16',550,-1,-0.18,-21,-3.68),
(8,'2020-06-12',551,-9,-1.6,-20,-3.5),
(9,'2020-06-04',560,0,0,-11,-1.93),
(10,'2020-06-01',560,0,0,-10,-1.75),
(11,'2020-07-09',550,2,-0.6,-9,-0.9),
(12,'2020-07-01',556,0,0,0,0),
(13,'2020-07-03',556,6,-2.3,-14,2.1),
(14,'2020-06-04',521,1,-1,0,1.3),
(15,'2020-05-29',522,2,1.3,1,15),
(16,'2020-05-28',462,2,1.2,1,2.1),
(17,'2020-05-26',522,5,2.5,0,2.1),
(18,'2020-05-23',321,6,2.6,2,-1.5),
(19,'2020-05-21',125,9,2.4,0,-1.4),
(20,'2020-05-18',569,2,3.2,3,1.3),
(21,'2020-05-15',856,1,0.5,0,1.5),
(22,'2020-05-13',145,3,0.9,5,-1.5),
(23,'2020-05-11',564,5,1.2,6,-1.6),
(24,'2020-05-08',235,-5,1.6,0,1),
(25,'2020-05-05',856,-3,1.2,1,1.36),
(26,'2020-05-02',546,-3.1,1.3,2,-1.2),
(27,'2020-04-28',596,-2.1,1.3,5,1.5),
(28,'2020-04-24',587,2,1.2,8,1.6),
(29,'2020-04-18',544,3,1,2,1.7),
(30,'2020-04-15',598,2,2.3,4,1.8),
(31,'2020-04-10',563,5,2.6,1,1.9);

/*Table structure for table `coal_harbor` */

DROP TABLE IF EXISTS `coal_harbor`;

CREATE TABLE `coal_harbor` (
  `harbor_id` int(11) NOT NULL,
  `harbor_date` date DEFAULT NULL COMMENT '日期',
  `harbor_k5800` double DEFAULT NULL,
  `harbor_k5500` double DEFAULT NULL,
  `harbor_k5000` double DEFAULT NULL,
  `harbor_k4500` double DEFAULT NULL,
  PRIMARY KEY (`harbor_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='港口煤价表';

/*Data for the table `coal_harbor` */

insert  into `coal_harbor`(`harbor_id`,`harbor_date`,`harbor_k5800`,`harbor_k5500`,`harbor_k5000`,`harbor_k4500`) values 
(1,'2020-07-08',456,560,452,365),
(2,'2020-06-30',265,565,561,411),
(3,'2018-06-09',541,466,211,215),
(4,'2014-05-01',526,455,121,125),
(5,'2018-01-09',566,265,565,122),
(6,'2020-07-08',262,166,315,164),
(7,'2009-02-09',447,986,845,456),
(8,'2013-06-09',231,412,134,123),
(9,'2015-01-09',234,463,547,458),
(10,'2016-11-01',454,555,355,656),
(11,'2011-07-01',422,556,877,968),
(12,'2015-10-01',214,456,457,466),
(14,'2015-10-01',123,456,563,456),
(15,'2019-01-16',123,231,233,123),
(16,'2014-10-01',234,456,457,642),
(17,'2014-10-01',244,456,456,464),
(18,'2006-10-01',124,454,454,456),
(19,'2019-03-22',456,645,654,789),
(20,'2019-08-22',456,545,646,897),
(21,'2019-04-25',362,564,236,214),
(22,'2012-10-01',345,234,112,342),
(23,'2020-07-23',123,442,223,442),
(24,'2020-05-23',262,262,152,215),
(25,'2014-10-01',231,412,1231,264),
(26,'2012-10-01',231,412,123,412),
(27,'2011-11-09',362,151,613,223),
(28,'2014-11-01',231,124,125,135),
(29,'2017-06-23',263,125,154,487),
(30,'2012-07-09',569,457,545,135);

/*Table structure for table `coal_price` */

DROP TABLE IF EXISTS `coal_price`;

CREATE TABLE `coal_price` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `price_date` date DEFAULT NULL,
  `hot_value5000` double DEFAULT NULL,
  `hot_value4000` double DEFAULT NULL,
  PRIMARY KEY (`price_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='煤炭价格';

/*Data for the table `coal_price` */

insert  into `coal_price`(`price_id`,`price_date`,`hot_value5000`,`hot_value4000`) values 
(1,'2020-07-08',72,65),
(2,'2020-07-04',83,61),
(3,'2020-07-01',79,63),
(4,'2020-06-27',88,78),
(5,'2020-06-23',79,68),
(6,'2020-06-19',87,71),
(7,'2020-06-16',92,61),
(8,'2020-06-12',99,74),
(9,'2020-06-04',90,62),
(10,'2020-06-01',93,75),
(11,'2020-05-31',83,79),
(12,'2020-05-30',76,72),
(13,'2020-05-29',80,67),
(14,'2020-05-28',72,79),
(15,'2020-05-24',86,63),
(16,'2020-05-22',99,69),
(17,'2020-05-21',72,79),
(18,'2020-05-20',97,64),
(19,'2020-05-18',97,71),
(20,'2020-05-14',83,66),
(21,'2020-05-10',91,75),
(22,'2020-05-06',92,64),
(23,'2020-05-02',98,66),
(24,'2020-04-21',87,70),
(25,'2020-04-16',79,80),
(26,'2020-04-10',84,66),
(27,'2020-04-04',81,64),
(28,'2020-04-01',85,74),
(29,'2020-03-28',74,69),
(30,'2020-03-21',80,60);

/*Table structure for table `elec_data` */

DROP TABLE IF EXISTS `elec_data`;

CREATE TABLE `elec_data` (
  `elec_data_id` int(11) NOT NULL,
  `elec_data_date` date DEFAULT NULL,
  `elec_data_fdv` double DEFAULT NULL,
  `elec_data_fdy` double DEFAULT NULL,
  `elec_data_acv` double DEFAULT NULL,
  `elec_data_acy` float DEFAULT NULL,
  PRIMARY KEY (`elec_data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='发电量';

/*Data for the table `elec_data` */

insert  into `elec_data`(`elec_data_id`,`elec_data_date`,`elec_data_fdv`,`elec_data_fdy`,`elec_data_acv`,`elec_data_acy`) values 
(1,'2020-05-12',49.9,2.5,44.9,2.3),
(2,'2020-05-07',67.9,3.5,44.8,2.1),
(3,'2020-06-11',55.8,1.5,46.7,6.8),
(4,'2020-06-02',78.9,1.2,56.6,9.1),
(5,'2020-07-01',66.7,1.3,52.3,6.5),
(6,'2020-07-17',67.7,1.4,88.9,6.1),
(7,'2020-06-18',87.9,1.5,87.9,3.3),
(8,'2020-06-19',77.9,1.6,87.6,6.5),
(9,'2020-06-20',77.8,1.7,84.5,6.1),
(10,'2020-06-22',88.9,1.8,84.2,6.3),
(11,'2020-06-23',88.9,1.3,85,6.2),
(12,'2020-06-26',77,2.1,84.6,5.4),
(13,'2020-06-27',44.7,2.3,85.2,7.8),
(14,'2020-06-28',55.8,2.4,86.2,8.9),
(15,'2020-08-10',58.9,2.5,84.6,5.1),
(16,'2020-08-15',89.9,2.6,78.9,4.6),
(17,'2020-08-06',67.9,2.5,79.2,2.3),
(18,'2020-07-16',45.8,3.5,79.1,4.6),
(19,'2020-07-06',33.8,3.6,56.8,4.5),
(20,'2020-06-30',33.6,7.7,56.3,4.3),
(21,'2020-06-29',47.8,8.8,88.9,5.2),
(22,'2020-07-10',55.8,5.6,55.4,5.3),
(23,'2020-07-03',55.7,5.7,65.5,6.3),
(24,'2020-06-10',67.8,5.8,65.8,6.7),
(25,'2020-07-29',89.9,5.9,62.3,6.3),
(26,'2020-07-11',89.2,5.2,98.3,5.3),
(27,'2020-07-25',56.9,5.7,92.5,5.6),
(28,'2020-07-08',47.9,5.1,99.6,5.6),
(29,'2020-07-30',35.7,6.1,25.6,2.6),
(30,'2020-07-24',66.9,6.7,55.6,3.6);

/*Table structure for table `futures_price` */

DROP TABLE IF EXISTS `futures_price`;

CREATE TABLE `futures_price` (
  `futuresprice_id` int(11) NOT NULL,
  `futuresprice_date` date DEFAULT NULL,
  `futuresprice_month` double DEFAULT NULL,
  `futuresprice_oprice` double DEFAULT NULL,
  `futuresprice_cprice` double DEFAULT NULL,
  `futuresprice_settlement` double DEFAULT NULL,
  `futuresprice_cjl` double DEFAULT NULL,
  `futuresprice_kpl` double DEFAULT NULL,
  `futuresprice_hb` double DEFAULT NULL,
  PRIMARY KEY (`futuresprice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='期货价格';

/*Data for the table `futures_price` */

insert  into `futures_price`(`futuresprice_id`,`futuresprice_date`,`futuresprice_month`,`futuresprice_oprice`,`futuresprice_cprice`,`futuresprice_settlement`,`futuresprice_cjl`,`futuresprice_kpl`,`futuresprice_hb`) values 
(1,'2019-07-19',1909,579.4,586,584.6,207881,253502,0.27),
(2,'2019-07-15',1909,579,581,583,162020,260152,-1.19),
(3,'2019-07-08',1909,590,586.4,590,211574,231668,-1.96),
(4,'2019-07-01',1909,605.4,598.8,601.8,197404,253348,-1.12),
(5,'2019-06-24',1909,606.8,606.4,609.2,276190,294570,3.15),
(6,'2019-06-23',1909,579,581,584.6,207881,253502,0.27),
(7,'2019-06-22',1909,579,581,584.6,207881,253502,0.27),
(8,'2019-06-21',1909,579,581,584.6,207881,253502,0.27),
(9,'2019-06-20',1909,579,581,584.6,207881,253502,0.27),
(10,'2019-06-19',1909,588.2,581,584.6,207881,254214,0.35),
(11,'2019-06-18',1909,588.2,581,590,16224,254214,0.35),
(12,'2019-06-17',1909,588.2,581,590,16224,254214,0.35),
(13,'2019-06-16',1909,588.2,581,590,16224,254214,0.35),
(14,'2019-06-15',1909,588.2,581,590,16224,254214,0.35),
(15,'2019-06-14',1909,588.2,581,590,211574,254214,0.35),
(16,'2019-06-13',1909,588.2,586,590,211574,254214,0.35),
(17,'2019-06-12',1909,588.2,586,590,16224,260152,0.48),
(18,'2019-06-11',1909,547,586,590,16224,260152,0.48),
(19,'2019-06-10',1909,547,586,583,211574,260152,0.48),
(20,'2019-06-09',1909,547,586,583,211574,260152,0.48),
(21,'2019-06-08',1909,547,586,583,211574,260152,0.48),
(22,'2019-06-07',1909,547,544,583,124124,260152,0.48),
(23,'2019-06-06',1909,547,544,583,124124,260152,0.48),
(24,'2019-06-05',1909,555,544,583,124124,254214,0.35),
(25,'2019-06-04',1909,555,544,583,242411,254214,0.35),
(26,'2019-06-03',1909,555,544,590,242411,254214,0.35),
(27,'2019-06-02',1909,555,544,590,242411,254214,0.35),
(28,'2019-06-01',1909,555,544,590,242411,254214,0.35),
(29,'2019-05-30',1909,565,538,590,242411,254214,0.35),
(30,'2019-05-24',1909,575,575,590,242411,260152,0.48);

/*Table structure for table `gdp` */

DROP TABLE IF EXISTS `gdp`;

CREATE TABLE `gdp` (
  `my_date` date DEFAULT NULL,
  `gdp_value` double DEFAULT NULL,
  `gdp_up` float DEFAULT NULL,
  `gdp_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`gdp_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='gdp';

/*Data for the table `gdp` */

insert  into `gdp`(`my_date`,`gdp_value`,`gdp_up`,`gdp_id`) values 
('2018-01-01',199634,0.86,1),
('2018-04-01',245689,0.88,2),
('2018-07-01',234561,0.95,3),
('2018-10-01',174506,0.79,4),
('2019-01-01',192701,0.63,5),
('2019-04-01',204567,0.85,6),
('2019-07-01',180166,0.7,7),
('2019-10-01',200558.7,0.11,8),
('2020-01-01',211595.5,0.12,9),
('2020-04-01',234582.3,0.1,10),
('2020-07-01',198783.1,0.9,11),
('2020-10-01',220178,0.8,12);

/*Table structure for table `international_offer` */

DROP TABLE IF EXISTS `international_offer`;

CREATE TABLE `international_offer` (
  `offer_id` int(11) NOT NULL AUTO_INCREMENT,
  `offer_date` date DEFAULT NULL COMMENT '日期',
  `calorific_value` int(255) DEFAULT NULL COMMENT '热值',
  `origin_country` varchar(255) DEFAULT NULL COMMENT '来源国',
  `price` double DEFAULT NULL COMMENT '价格',
  `price_terms` varchar(255) DEFAULT NULL COMMENT '价格术语',
  `number` varchar(255) DEFAULT NULL COMMENT '数量',
  `delivery_port` varchar(255) DEFAULT NULL COMMENT '交货港口',
  `shipment_time` enum('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月') DEFAULT NULL COMMENT '装船时间',
  `transaction_time` date DEFAULT NULL COMMENT '成交时间',
  PRIMARY KEY (`offer_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='国际煤价 报盘';

/*Data for the table `international_offer` */

insert  into `international_offer`(`offer_id`,`offer_date`,`calorific_value`,`origin_country`,`price`,`price_terms`,`number`,`delivery_port`,`shipment_time`,`transaction_time`) values 
(1,'2017-12-15',3800,'印尼',46.09,'FOB','Supramax','中国','12月','2017-12-01'),
(2,'2017-11-15',3800,'印尼',44.78,'FOB','Supramax','中国','11月','2017-11-01'),
(3,'2017-01-11',4000,'澳大利亚',44.72,'CFR','Supramax','华南','3月','2017-03-01'),
(4,'2002-10-12',3900,'印尼',52.92,'CFR','Supramax','中国','11月','2002-11-01'),
(5,'2012-06-13',5000,'印尼',36.08,'FOB','CAPE','中国','7月','2012-07-01'),
(6,'2005-08-26',5400,'印尼',37.04,'CFR','SMX','中国','11月','2005-11-01'),
(7,'2000-02-01',5300,'澳大利亚',35.73,'CFR','Supramax','中国','5月','2000-05-01'),
(8,'2006-10-27',4200,'印尼',38.41,'CFR','CAPE','中国','11月','2006-11-01'),
(9,'2013-02-04',5400,'印尼',46.61,'FOB','Supramax','中国','3月','2013-03-01'),
(10,'2004-08-08',4900,'澳大利亚',42.4,'FOB','CAPE','中国','11月','2004-11-01'),
(11,'2004-03-08',4300,'印尼',51.04,'CFR','CAPE','中国','5月','2004-05-01'),
(12,'2005-01-31',4500,'印尼',39.94,'FOB','SMX','华东','3月','2005-03-01'),
(13,'2004-08-06',4300,'印尼',49.45,'FOB','PMX','日本','9月','2004-09-01'),
(14,'2001-03-13',4700,'印尼',46.16,'CFR','PMX','中国','4月','2001-04-01'),
(15,'2014-04-16',3600,'澳大利亚',40.86,'CFR','PMX','中国','7月','2014-07-01'),
(16,'2015-03-25',4300,'印尼',49.16,'CFR','SMX','中国','6月','2015-06-01'),
(17,'2007-05-24',5100,'印尼',38.34,'FOB','PMX','中国','7月','2007-07-01'),
(18,'2017-06-03',4900,'印尼',48.58,'CFR','Handy','中国','8月','2017-08-01'),
(19,'2002-01-17',4300,'澳大利亚',37.48,'CFR','CAPE','中国','3月','2002-03-01'),
(20,'2000-01-03',4700,'澳大利亚',41.61,'CFR','Supramax','华南','3月','2000-03-01'),
(21,'2003-06-12',4400,'印尼',54.01,'CFR','Handy','中国','7月','2003-07-01'),
(22,'2008-04-23',4300,'印尼',41.44,'CFR','Supramax','中国','5月','2008-05-01'),
(23,'2008-02-15',5200,'印尼',50.16,'CFR','SMX','中国','5月','2008-05-01'),
(24,'2000-09-05',3500,'印尼',54.1,'CFR','Supramax','中国','11月','2000-11-01'),
(25,'2002-01-10',3600,'印尼',44.31,'CFR','CAPE','华南','3月','2002-03-01'),
(26,'2007-03-30',3600,'印尼',43.38,'CFR','Handy','中国','6月','2007-06-01'),
(27,'2002-06-12',3700,'印尼',53.81,'CFR','Supramax','中国','8月','2002-08-01'),
(28,'2017-06-25',4200,'印尼',54.01,'FOB','Handy','中国','7月','2017-07-01'),
(29,'2013-10-03',3700,'澳大利亚',40.93,'CFR','CAPE','中国','11月','2013-11-01'),
(30,'2006-08-27',4400,'印尼',42.99,'CFR','PMX','中国','10月','2006-10-01');

/*Table structure for table `international_price` */

DROP TABLE IF EXISTS `international_price`;

CREATE TABLE `international_price` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `price_date` date DEFAULT NULL COMMENT '日期',
  `australia5500k` double DEFAULT NULL COMMENT '澳洲5500K',
  `australia5500k_yoy` double DEFAULT NULL COMMENT '同比',
  `indonesia4700k` double DEFAULT NULL COMMENT '印尼4700K',
  `indonesia4700k_yoy` double DEFAULT NULL COMMENT '同比',
  `indonesia3800k` double DEFAULT NULL COMMENT '印尼3800K',
  `Indonesia3800k_yoy` double DEFAULT NULL COMMENT '同比',
  PRIMARY KEY (`price_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='国际煤价 价格';

/*Data for the table `international_price` */

insert  into `international_price`(`price_id`,`price_date`,`australia5500k`,`australia5500k_yoy`,`indonesia4700k`,`indonesia4700k_yoy`,`indonesia3800k`,`Indonesia3800k_yoy`) values 
(1,'2002-07-29',88.6,-3.52,62.9,7.66,57.8,-0.2),
(2,'2016-07-08',92.9,-9.63,61.9,5.8,53.7,10.87),
(3,'2010-04-27',71.4,2.34,79.4,13.94,53.5,4.45),
(4,'2006-09-19',94.1,-7.49,64.5,-4.4,57.1,12.65),
(5,'2015-10-23',82.6,-1.54,74.7,12.57,47.3,9.61),
(6,'2013-10-22',95.3,3.19,60.5,-4.71,57.1,7.64),
(7,'2003-04-01',92.3,4.85,74.9,12.61,51.8,9.21),
(8,'2010-01-07',90.4,4.23,79.5,6.17,50.5,6.44),
(9,'2008-07-15',79.7,4.08,72,-2.11,43.5,14.85),
(10,'2000-06-30',81,-4.53,67.4,9.63,53.2,16.47),
(11,'2008-01-23',85,-7.47,74.2,3.53,42.4,9.99),
(12,'2006-06-30',77.2,-7.7,70.7,9.12,46.5,-0.02),
(13,'2008-03-17',92.4,-6.05,75.7,7.56,57.7,3.4),
(14,'2010-03-09',77,-4.61,64.6,9.94,57.6,7.69),
(15,'2014-12-09',76.5,1.58,73.4,9.91,54.4,3.79),
(16,'2009-01-15',83.4,-6.4,75.6,3.15,58.4,14.67),
(17,'2009-03-17',91.1,-8.21,63.9,-1.32,56.5,9.33),
(18,'2018-12-22',75.5,-7.64,75.9,6.12,45.2,1.44),
(19,'2012-09-08',97.3,-0.39,72.8,-3.57,44.8,2.79),
(20,'2012-10-08',73,9.01,66.7,8.31,49.5,-1.03),
(21,'2004-10-22',84.9,0.27,62.8,4.04,50.2,1.5),
(22,'2002-09-15',74,-3.88,77.1,-3.64,56.4,10.25),
(23,'2019-06-17',72.2,7.51,65.2,8.03,44.9,10.66),
(24,'2000-11-20',83,-4.07,60.8,-0.32,42.9,10.41),
(25,'2004-07-21',78.7,-7.56,63.5,11.1,54,14.26),
(26,'2015-07-07',87.6,-9.13,78.3,0.06,59,12.14),
(27,'2019-01-28',79.3,4.02,68.3,1.96,47.1,-0.72),
(28,'2000-06-08',86.7,0.71,71,8.34,53.8,1.38),
(29,'2006-09-13',92.4,-9.96,61.5,13.21,42.3,0.86),
(30,'2019-03-15',99.3,-7.52,74,12.09,46.1,6.39);

/*Table structure for table `international_quote` */

DROP TABLE IF EXISTS `international_quote`;

CREATE TABLE `international_quote` (
  `offer_id` int(11) NOT NULL AUTO_INCREMENT,
  `offer_date` date DEFAULT NULL COMMENT '日期',
  `calorific_value` int(255) DEFAULT NULL COMMENT '热值',
  `origin_country` varchar(255) DEFAULT NULL COMMENT '来源国',
  `price` double DEFAULT NULL COMMENT '价格',
  `price_terms` varchar(255) DEFAULT NULL COMMENT '价格术语',
  `number` varchar(255) DEFAULT NULL COMMENT '数量',
  `delivery_port` varchar(255) DEFAULT NULL COMMENT '交货港口',
  `shipment_time` enum('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月') DEFAULT NULL COMMENT '装船时间',
  PRIMARY KEY (`offer_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='国际煤价-成交';

/*Data for the table `international_quote` */

insert  into `international_quote`(`offer_id`,`offer_date`,`calorific_value`,`origin_country`,`price`,`price_terms`,`number`,`delivery_port`,`shipment_time`) values 
(1,'2017-11-10',3800,'印尼',35.19,'FOB','Supramax','中国','11月'),
(2,'2017-06-02',3800,'印尼',36.5,'FOB','Supramax','中国','6月'),
(3,'2017-05-09',5500,'澳洲',68,'FOB','CAP','中国','6月'),
(4,'2017-05-08',3800,'印尼',39,'FOB','Supramax','中国','6月'),
(5,'2017-04-17',3800,'印尼',43,'FOB','Supramax','中国','5月'),
(6,'2017-04-11',3800,'印尼',45,'FOB','Supramax','中国','5月'),
(7,'2017-03-30',5500,'澳大利亚',73,'FOB','CAP','中国','4月'),
(8,'2017-03-15',3800,'印尼',47,'FOB','Supramax','中国','4月'),
(9,'2017-03-06',3800,'印尼',46,'FOB','Supramax','中国','4月'),
(10,'2017-02-21',5500,'澳大利亚',65.5,'FOB','CAP','中国','4月'),
(11,'2017-02-15',3800,'印尼',45.5,'FOB','Supramax','中国','3月'),
(12,'2017-02-10',5500,'澳洲',63.5,'FOB','PMX','中国','2月'),
(13,'2017-02-07',5500,'澳洲',63,'FOB','PMX','中国','2月'),
(14,'2017-02-06',3800,'印尼',44,'FOB','Supramax','中国','2月'),
(15,'2017-01-20',3800,'印尼',44,'CFR','Supramax','中国','1月'),
(16,'2017-01-18',3800,'印尼',36,'FOB','Supramax','中国','1月'),
(17,'2017-01-06',3800,'印尼',36.5,'FOB','Supramax','中国','1月'),
(18,'2016-12-21',5500,'澳大利亚',67,'CFR','Supramax','中国','1月'),
(19,'2016-12-13',3800,'印尼',40.5,'FOB','Supramax','中国','1月'),
(20,'2016-12-08',3800,'印尼',40,'FOB','PMX','中国','1月'),
(21,'2016-12-07',5500,'澳大利亚',69,'CFR','cape','中国','1月'),
(22,'2016-12-06',3800,'印尼',41,'FOB','PMX','中国','12月'),
(23,'2016-12-05',4700,'印尼',59,'FOB','Supramax','中国','12月'),
(24,'2016-12-01',4700,'印尼',63,'FOB','PMX','中国','12月'),
(25,'2016-11-29',3800,'印尼',42,'FOB','PMX','中国','12月'),
(26,'2016-11-25',5500,'澳洲',70,'FOB','cape','中国','12月'),
(27,'2016-11-23',5500,'澳洲',72,'FOB','cape','中国','12月'),
(28,'2016-11-22',3800,'印尼',45,'FOB','PMX','中国','12月'),
(29,'2016-11-21',5500,'澳洲',83,'FOB','cape','中国','12月'),
(30,'2016-11-15',5500,'澳洲',86,'FOB','cape','中国','12月');

/*Table structure for table `pmi` */

DROP TABLE IF EXISTS `pmi`;

CREATE TABLE `pmi` (
  `my_date` date DEFAULT NULL,
  `pmi_value` double DEFAULT NULL,
  `pmi_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`pmi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

/*Data for the table `pmi` */

insert  into `pmi`(`my_date`,`pmi_value`,`pmi_id`) values 
('2020-01-01',49.2,1),
('2020-02-01',47.8,2),
('2020-03-01',44.5,3),
('2019-10-01',49.3,4),
('2019-09-01',49.8,5),
('2019-08-01',49.5,6),
('2019-07-01',49.5,7),
('2019-06-01',49.4,8),
('2019-05-01',49.4,9),
('2019-04-01',50.1,10),
('2019-03-01',50.5,11),
('2019-02-01',49.2,12),
('2019-11-01',49.3,13),
('2019-12-01',48.3,14),
('2019-01-01',46.2,15),
('2018-12-01',45.3,16),
('2018-11-01',47.2,17),
('2018-10-01',48.3,18),
('2018-09-01',49.2,19),
('2018-08-01',46.9,20),
('2018-07-01',47,21),
('2018-06-01',46.2,22),
('2018-05-01',44.3,23),
('2018-04-01',48.2,24),
('2018-03-01',49.2,25),
('2018-02-01',46.7,26),
('2018-01-01',49.2,27),
('2017-12-01',46.2,28),
('2017-11-01',48.2,29),
('2017-10-01',46.2,30);

/*Table structure for table `pt_department` */

DROP TABLE IF EXISTS `pt_department`;

CREATE TABLE `pt_department` (
  `DEP_UUID` varchar(32) NOT NULL DEFAULT '',
  `ORGAN_UUID` varchar(255) DEFAULT NULL,
  `BRANCH_NAME` varchar(50) DEFAULT NULL,
  `BELONG_CENTER` varchar(32) DEFAULT NULL,
  `MODIFIERID` varchar(32) DEFAULT NULL,
  `MODTIME` date DEFAULT NULL,
  `DEL_FLAG` char(1) DEFAULT NULL,
  `REMARK` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DEP_UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门表';

/*Data for the table `pt_department` */

insert  into `pt_department`(`DEP_UUID`,`ORGAN_UUID`,`BRANCH_NAME`,`BELONG_CENTER`,`MODIFIERID`,`MODTIME`,`DEL_FLAG`,`REMARK`) values 
('017fb8718274496f8da330a61ae85707','108d96767e7d4789b770666b2a29a668','集团测','-1',NULL,'2022-05-05','0',NULL),
('018fbb65b0eb4ba195657240fee941e6','20cd1ab32a21409daf025d6b19063efe','南京分公司','017fb8718274496f8da330a61ae85707',NULL,'2022-05-05','0',NULL),
('0191e887adfa4a2893326ee59ddaaf97','20cd1ab32a21409daf025d6b19063efe','北京分公司','017fb8718274496f8da330a61ae85707',NULL,'2022-05-05','0',NULL),
('019de774593b458883977225233195c7','20cd1ab32a21409daf025d6b19063efe','西安分公司','017fb8718274496f8da330a61ae85707',NULL,'2022-05-05','0',NULL),
('01b79caaaea9480086e24ddde72a1cb7','d68b62ed4e60495c93f2477298205745','西安云服务实施一部','019de774593b458883977225233195c7',NULL,'2022-05-05','0',NULL),
('01dd534f880c49b89e6a79e205fc10e1','d68b62ed4e60495c93f2477298205745','南京云服务实施二部','018fbb65b0eb4ba195657240fee941e6',NULL,'2022-05-05','0',NULL),
('01fb216fd8ed4d0fa25c7dda6f9a57aa','d68b62ed4e60495c93f2477298205745','北京云服务实施三部','0191e887adfa4a2893326ee59ddaaf97',NULL,'2022-05-05','0',NULL),
('020b58d85ea1444e9a874a9b8f0b9ace','d68b62ed4e60495c93f2477298205745','西安集成企业交付实施一部','019de774593b458883977225233195c7',NULL,'2022-05-05','0',NULL),
('0215bd5730c54c4d8dc77a31a36b8d7a','d68b62ed4e60495c93f2477298205745','南京集成企业交付实施二部','018fbb65b0eb4ba195657240fee941e6',NULL,'2022-05-05','0',NULL),
('02182c9250374962a6e783cd335c4636','d68b62ed4e60495c93f2477298205745','北京集成企业交付实施三部','0191e887adfa4a2893326ee59ddaaf97',NULL,'2022-05-05','0',NULL),
('022420e2a99d4e40a239c5cbd891a4e2','2d9cfaaa20424472b0cc7a5dcf2113ac','市场部','017fb8718274496f8da330a61ae85707',NULL,'2022-05-05','0',NULL),
('02344c9d06bc49eb9b5ffa1ac342a1e5','4c2e58a997cf4546ab1377d5512dd8a9','设计部','017fb8718274496f8da330a61ae85707',NULL,'2022-05-05','0',NULL),
('023b102ef4f242ee9bbe616160c2bae7','593d5b59c0ac4c0c9813151f196fd147','网络部','017fb8718274496f8da330a61ae85707',NULL,'2022-05-05','0',NULL),
('0242e6410f4149e4886334ba0fa732b5','8a49ccd17e4d46699dbbefe502d5dee2','财务部','017fb8718274496f8da330a61ae85707',NULL,'2022-05-05','0',NULL),
('0246395c606e4237838c4e2159498514','a52f6bb4e24344f6b8f2e3a48d28a007','客服部','017fb8718274496f8da330a61ae85707',NULL,'2022-05-05','0',NULL),
('02474d1d9c2f492d9f881aae0973749c','d68b62ed4e60495c93f2477298205745','南京荣耀产品实施一部','018fbb65b0eb4ba195657240fee941e6',NULL,'2022-05-05','0',NULL),
('02546be164154cbe82edf806fe01e59a','d68b62ed4e60495c93f2477298205745','北京荣耀产品实施二部','0191e887adfa4a2893326ee59ddaaf97',NULL,'2022-05-05','0',NULL),
('0256535b08ee4f78908d6ea18fa1e1f6','d68b62ed4e60495c93f2477298205745','西安荣耀产品实施三部','019de774593b458883977225233195c7',NULL,'2022-05-05','0',NULL);

/*Table structure for table `pt_dictionary` */

DROP TABLE IF EXISTS `pt_dictionary`;

CREATE TABLE `pt_dictionary` (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_parent_id` bigint(20) DEFAULT '0',
  `dict_group` varchar(64) NOT NULL COMMENT '字典分组',
  `dic_code` varchar(32) NOT NULL COMMENT '代码',
  `description` varchar(64) DEFAULT NULL COMMENT '备注',
  `dic_sort` int(11) DEFAULT NULL COMMENT '显示排序',
  PRIMARY KEY (`dict_id`)
) ENGINE=InnoDB AUTO_INCREMENT=303 DEFAULT CHARSET=utf8 COMMENT='字典数据表';

/*Data for the table `pt_dictionary` */

insert  into `pt_dictionary`(`dict_id`,`dict_parent_id`,`dict_group`,`dic_code`,`description`,`dic_sort`) values 
(5,0,'sex','0','女',1),
(6,0,'sex','1','男',2),
(7,0,'group','0','全部',1),
(8,0,'group','1','区组',2),
(9,0,'menu.https','0','0 非https',1),
(10,0,'menu.https','1','1 https',2),
(11,0,'organ.type','0','组织类型  集团侧',1),
(12,0,'organ.type','1','组织类型  阳光专区供应商',2),
(13,0,'organ.type','2','组织类型  电厂侧',3),
(14,0,'organ.type','3','组织类型  分公司侧',4),
(15,0,'menu.level','1','一级菜单',1),
(16,0,'menu.level','2','二级菜单',2),
(17,0,'menu.level','3','三级菜单',3),
(18,0,'menu.level','4','四级菜单',4),
(201,0,'menu.type','0','菜单目录',1),
(202,0,'menu.type','1','菜单',2),
(203,0,'menu.type','2','功能',3),
(301,0,'lang.type','1','英文',1),
(302,0,'lang.type','2','中文',2);

/*Table structure for table `pt_dictionary_i18n` */

DROP TABLE IF EXISTS `pt_dictionary_i18n`;

CREATE TABLE `pt_dictionary_i18n` (
  `dict_id` bigint(20) NOT NULL COMMENT '字典编码',
  `dict_name` varchar(64) DEFAULT NULL COMMENT '字典名称',
  `language_id` bigint(20) NOT NULL COMMENT '语言ID',
  PRIMARY KEY (`dict_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典数据国际化表';

/*Data for the table `pt_dictionary_i18n` */

insert  into `pt_dictionary_i18n`(`dict_id`,`dict_name`,`language_id`) values 
(5,'female',1),
(5,'女',2),
(6,'male',1),
(6,'男',2),
(7,'all',1),
(7,'全部',2),
(8,'group',1),
(8,'区组',2),
(9,'No',1),
(9,'否',2),
(10,'Yes',1),
(10,'是',2),
(11,'Group side',1),
(11,'集团侧',2),
(12,'Suppliers of sunshine zone',1),
(12,'阳光专区供应商',2),
(13,'Power plant side',1),
(13,'电厂侧',2),
(14,'Branch side',1),
(14,'分公司侧',2),
(15,'level 1',1),
(15,'1 级',2),
(16,'level 2',1),
(16,'2 级',2),
(17,'level 3',1),
(17,'3 级',2),
(18,'level 4',1),
(18,'4级',2),
(201,'dir',1),
(201,'目录',2),
(202,'menu',1),
(202,'菜单',2),
(203,'fuction',1),
(203,'功能',2),
(301,'english',1),
(301,'英文',2),
(302,'chinese',1),
(302,'中文',2);

/*Table structure for table `pt_duty` */

DROP TABLE IF EXISTS `pt_duty`;

CREATE TABLE `pt_duty` (
  `DUTYID` varchar(32) NOT NULL COMMENT '岗位ID',
  `NAME` varchar(255) DEFAULT NULL COMMENT '岗位名称',
  `ORGAN_UUID` varchar(255) DEFAULT NULL COMMENT '组织ID',
  `ROLE_UUID` varchar(255) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`DUTYID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='岗位表（角色和单位的挂接）';

/*Data for the table `pt_duty` */

insert  into `pt_duty`(`DUTYID`,`NAME`,`ORGAN_UUID`,`ROLE_UUID`) values 
('000a640891fe4a409ea6d35cfc5e4b30','董事长','000a640891fe4a409ea6d35cfc5e4b30',NULL),
('001e8c3f1035427ab9a19f96aa3b853d','董事','000a640891fe4a409ea6d35cfc5e4b30',NULL),
('00206d81aeb04175b7db82c5999032a5','总经理','000a640891fe4a409ea6d35cfc5e4b30',NULL),
('003c26ef0b6a4403a19ecfb76632e579','副总经理','000a640891fe4a409ea6d35cfc5e4b30',NULL),
('0059fdfef2254635ba624a8d06cc672c','高级人力资源总监','000a640891fe4a409ea6d35cfc5e4b30',NULL),
('00647528c5fe468c975b62c4ae0873be','招聘主管','20cd1ab32a21409daf025d6b19063efe',NULL),
('006e8eb40bdb4c6682b0964cf4c0c3ad','企业文化总监','20cd1ab32a21409daf025d6b19063efe',NULL),
('0071dbae4a90438e881413ea3a824c10','招聘顾问','20cd1ab32a21409daf025d6b19063efe',NULL),
('0079a9deb7a54bd79741942219b84c2a','高级人事行政经理','20cd1ab32a21409daf025d6b19063efe',NULL),
('008439af2ab341388df344c497287062','高级销售总监','2d9cfaaa20424472b0cc7a5dcf2113ac',NULL),
('008766eea87644e89d25a00b89b48c1e','销售运营主管','2d9cfaaa20424472b0cc7a5dcf2113ac',NULL),
('00964a553276429f84c28cf378eb69d8','销售助理','2d9cfaaa20424472b0cc7a5dcf2113ac',NULL),
('0096e3096b084a219cc139ac1bb18bbc','产品结构设计师','4c2e58a997cf4546ab1377d5512dd8a9',NULL),
('00b356a1fc754438a844b9fa2c67f28c','高级品牌设计师','4c2e58a997cf4546ab1377d5512dd8a9',NULL),
('00bb01c637b749b59bfcee6ede425ece','网络总管','593d5b59c0ac4c0c9813151f196fd147',NULL),
('00c9f18143b84389a31c8c8be93401f8','视觉设计师','4c2e58a997cf4546ab1377d5512dd8a9',NULL),
('00d439297a0549639db163bb57affb8e','网络信息管理员','593d5b59c0ac4c0c9813151f196fd147',NULL),
('00db0e2ef759429f8382034d3f4b5cf1','网络运维系统工程师','593d5b59c0ac4c0c9813151f196fd147',NULL),
('00df4c86c95346168de9205561f517fb','网络工程师','593d5b59c0ac4c0c9813151f196fd147',NULL),
('00f95d3173ac4764acad784b671ec8a7','财务会计','8a49ccd17e4d46699dbbefe502d5dee2',NULL),
('00faa5bd942b47c0b69aee8eec5aa3ea','财务经理','8a49ccd17e4d46699dbbefe502d5dee2',NULL),
('01056446b1314c9da7c171667dcc1b41','订购客服','a52f6bb4e24344f6b8f2e3a48d28a007',NULL),
('010ad8eb636f453a90c6002dd0c7030c','电商客服','a52f6bb4e24344f6b8f2e3a48d28a007',NULL),
('01148eac302d413ea7d905c62c819f09','售前客服','a52f6bb4e24344f6b8f2e3a48d28a007',NULL),
('012ad16e846b4e1a83ba26a48897dd2e','售后客服','a52f6bb4e24344f6b8f2e3a48d28a007',NULL),
('0134cd7819794f63abe601cfdb213862','测试软件工程师','d68b62ed4e60495c93f2477298205745',NULL),
('01365936dfc1400eb450e0afcb0bbac9','高级软件工程师','d68b62ed4e60495c93f2477298205745',NULL),
('013e89254912498eb620b33d82f99e79','系统软件工程师','d68b62ed4e60495c93f2477298205745',NULL),
('0146e6d999a94f1e92e699046c025545','助理软件工程师','d68b62ed4e60495c93f2477298205745',NULL),
('016dd3ca38384c8f85282beecc055f56','系统软件工程师','d68b62ed4e60495c93f2477298205745',NULL),
('017674266f2944cfba7cc8dcd5dc5213','软件研发经理','d68b62ed4e60495c93f2477298205745',NULL),
('20cd1ab32a21409daf025d6b19063efe','分公司经理','20cd1ab32a21409daf025d6b19063efe',NULL);

/*Table structure for table `pt_language` */

DROP TABLE IF EXISTS `pt_language`;

CREATE TABLE `pt_language` (
  `lang_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '语言ID',
  `lang_type` varchar(30) DEFAULT NULL COMMENT '语言类型',
  `lang_desc` varchar(20) DEFAULT NULL COMMENT '语言描述',
  PRIMARY KEY (`lang_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='语言表';

/*Data for the table `pt_language` */

insert  into `pt_language`(`lang_id`,`lang_type`,`lang_desc`) values 
(1,'en','English'),
(2,'zh','汉语');

/*Table structure for table `pt_menu` */

DROP TABLE IF EXISTS `pt_menu`;

CREATE TABLE `pt_menu` (
  `menu_id` varchar(32) NOT NULL,
  `menu_name` varchar(32) DEFAULT NULL COMMENT '菜单名称',
  `parent_id` varchar(32) DEFAULT NULL COMMENT '父菜单ID',
  `menu_type` int(10) DEFAULT NULL COMMENT '菜单类型，0:菜单目录 1:菜单 2:页面功能',
  `menu_level` int(11) DEFAULT NULL COMMENT '菜单目录级数 1 2 3',
  `menu_url` varchar(256) DEFAULT NULL COMMENT '菜单url',
  `privilege_code` varchar(32) DEFAULT NULL COMMENT '菜单权限',
  `is_https` int(11) DEFAULT NULL,
  `menu_icon` varchar(255) DEFAULT NULL COMMENT '菜单图标类名',
  `menu_seq` varchar(10) DEFAULT NULL COMMENT '排序',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';

/*Data for the table `pt_menu` */

insert  into `pt_menu`(`menu_id`,`menu_name`,`parent_id`,`menu_type`,`menu_level`,`menu_url`,`privilege_code`,`is_https`,`menu_icon`,`menu_seq`,`create_time`) values 
('1','咨询中心','0',0,1,'temp','',NULL,'glyphicon glyphicon-align-justify','2',NULL),
('10','后台管理','2',0,2,'temp','',NULL,'fa fa-credit-card','2',NULL),
('11','系统配置','2',0,2,'temp','',NULL,'fa fa-credit-card','3',NULL),
('12','价格','7',1,3,'priceinfo/page','gqgy_data_price',NULL,'fa fa-file-text-o','1',NULL),
('13','报盘','7',1,3,'offerinfo/page','gqgy_data_offer',NULL,'fa fa-file-text-o','1',NULL),
('14','成交','7',0,3,'quoteinfo/page','gqgy_data_quote',NULL,'fa fa-file-text-o','1',NULL),
('15','GDP','8',1,3,'gdpinfo/page','gqgy_data_gdp',NULL,'fa fa-file-text-o','1',NULL),
('16','全国水泥产量','8',1,3,'sto/page','gqgy_data_sto',NULL,'fa fa-file-text-o','1',NULL),
('17','全国发电量','8',1,3,'electrc/page','gqgy_data_electrc',NULL,'fa fa-file-text-o','1',NULL),
('18','PMI','8',1,3,'pmiinfo/page','gqgy_data_pmi',NULL,'fa fa-file-text-o','1',NULL),
('19','发电量','8',1,3,'electricityinfo/page','gqgy_data_electricity',NULL,'fa fa-file-text-o','1',NULL),
('2','系统管理','0',0,1,'temp','',NULL,'glyphicon glyphicon-align-justify','3',NULL),
('20','组织','9',0,3,'organinfo/page','gqgy_data_organ',NULL,'fa fa-file-text-o','',NULL),
('21','部门','9',0,3,'temp.jsp','gqgy_data_department',NULL,'fa fa-file-text-o','',NULL),
('22','岗位','9',0,3,'post/page','gqgy_data_post',NULL,'fa fa-file-text-o','',NULL),
('23','角色管理','10',0,3,'role/rolePage','gqgy_data_role',NULL,'fa fa-file-text-o','',NULL),
('231','查询角色','23',2,4,NULL,'gqgy_data_role_query',NULL,NULL,'1',NULL),
('232','分配权限','23',2,4,NULL,'gqgy_data_role_allow',NULL,NULL,'0',NULL),
('233','增加角色','23',2,4,NULL,'gqgy_data_role_add',NULL,NULL,'2',NULL),
('234','角色详情','23',2,4,NULL,'gqgy_data_role_detail',NULL,NULL,'3',NULL),
('235','修改角色','23',2,4,NULL,'gqgy_data_role_update',NULL,NULL,'4',NULL),
('236','删除角色','23',2,4,NULL,'gqgy_data_role_delete',NULL,NULL,'5',NULL),
('24','用户管理','10',0,3,'user/userPage','gqgy_data_user',NULL,'fa fa-file-text-o','',NULL),
('241','查询用户','24',2,4,NULL,'gqgy_data_user_query',NULL,NULL,'0',NULL),
('242','分配角色','24',2,4,NULL,'gqgy_data_user_allow',NULL,NULL,'1',NULL),
('25','菜单','11',0,3,'menu/menuPage','gqgy_data_menu',NULL,'fa fa-file-text-o','',NULL),
('26','配置','11',0,3,'dictionary/page','gqgy_date_dictionary',NULL,'fa fa-file-text-o','',NULL),
('27','日志','11',0,3,'temp','',NULL,'fa fa-file-text-o','',NULL),
('3','价格','1',0,2,'temp','',NULL,'fa fa-credit-card','2',NULL),
('4','环渤海指数BSPI','3',1,3,'bspiinfo/page','gqgy_data_bspi',NULL,'fa fa-file-text-o','1',NULL),
('5','期货价格','3',1,3,'fpinfo/page','gqgy_data_fp',NULL,'fa fa-file-text-o','1',NULL),
('6','港口煤价','3',1,3,'hrbor/page','gqgy_data_hrbor',NULL,'fa fa-file-text-o','1',NULL),
('7','国际煤价','1',0,2,'temp','',NULL,'fa fa-credit-card','1',NULL),
('8','数据','1',0,2,'temp','',NULL,'fa fa-credit-card','3',NULL),
('9','组织架构','2',0,2,'temp','',NULL,'fa fa-credit-card','1',NULL);

/*Table structure for table `pt_menu_i18n` */

DROP TABLE IF EXISTS `pt_menu_i18n`;

CREATE TABLE `pt_menu_i18n` (
  `menu_id` varchar(32) NOT NULL DEFAULT '0' COMMENT '菜单ID',
  `lang_id` int(10) NOT NULL DEFAULT '0' COMMENT '语言ID',
  `menu_name` varchar(64) DEFAULT NULL COMMENT '国际化 菜单',
  PRIMARY KEY (`menu_id`,`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单国际化表';

/*Data for the table `pt_menu_i18n` */

insert  into `pt_menu_i18n`(`menu_id`,`lang_id`,`menu_name`) values 
('1',1,'ConsultingCenter'),
('1',2,'咨询中心'),
('10',1,'userRole'),
('10',2,'后台管理'),
('11',1,'system'),
('11',2,'系统配置'),
('12',1,'Price'),
('12',2,'价格'),
('13',1,'Offer'),
('13',2,'报盘'),
('14',1,'quote'),
('14',2,'成交'),
('15',1,'GDP'),
('15',2,'GDP'),
('16',1,'NCO'),
('16',2,'水泥产量'),
('17',1,'NPG'),
('17',2,'全国发电量'),
('18',1,'PMI'),
('18',2,'PMI'),
('19',1,'Power'),
('19',2,'发电量'),
('2',1,'management'),
('2',2,'系统管理'),
('20',1,'organ'),
('20',2,'组织'),
('21',1,'depart'),
('21',2,'部门'),
('22',1,'post'),
('22',2,'岗位'),
('23',1,'role'),
('23',2,'角色管理'),
('24',1,'user'),
('24',2,'用户管理'),
('25',1,'menu'),
('25',2,'菜单'),
('26',1,'config'),
('26',2,'配置'),
('27',1,'log'),
('27',2,'日志'),
('3',1,'price'),
('3',2,'价格'),
('4',1,'BSPI'),
('4',2,'环渤海指数'),
('5',1,'Futures'),
('5',2,'期货价格'),
('6',1,'PortCoal'),
('6',2,'港口煤价'),
('7',1,'ICP'),
('7',2,'国际煤价'),
('8',1,'data'),
('8',2,'数据'),
('9',1,'framework'),
('9',2,'组织架构');

/*Table structure for table `pt_organ` */

DROP TABLE IF EXISTS `pt_organ`;

CREATE TABLE `pt_organ` (
  `ORGAN_UUID` varchar(255) NOT NULL,
  `ORGAN_NAME` varchar(32) DEFAULT NULL,
  `PARENT_UUID` varchar(255) DEFAULT NULL,
  `DEL_FLAG` int(11) DEFAULT NULL COMMENT '是否删除',
  `MODIFIERID` varchar(255) DEFAULT NULL COMMENT '创建人ID',
  `MODTIME` datetime NOT NULL COMMENT '是否阳光用户',
  `DESCRIPTION` varchar(400) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`ORGAN_UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='组织表';

/*Data for the table `pt_organ` */

insert  into `pt_organ`(`ORGAN_UUID`,`ORGAN_NAME`,`PARENT_UUID`,`DEL_FLAG`,`MODIFIERID`,`MODTIME`,`DESCRIPTION`) values 
('108d96767e7d4789b770666b2a29a668','集团侧','-1',0,NULL,'2022-05-04 13:59:43',NULL),
('20cd1ab32a21409daf025d6b19063efe','分公司','108d96767e7d4789b770666b2a29a668',0,NULL,'2022-05-05 14:03:03',NULL),
('2d9cfaaa20424472b0cc7a5dcf2113ac','市场部','108d96767e7d4789b770666b2a29a668',0,NULL,'2022-05-05 14:03:12',NULL),
('4c2e58a997cf4546ab1377d5512dd8a9','设计部','108d96767e7d4789b770666b2a29a668',0,NULL,'2022-05-12 14:03:08',NULL),
('593d5b59c0ac4c0c9813151f196fd147','网络部','108d96767e7d4789b770666b2a29a668',0,NULL,'2022-05-05 14:03:55',NULL),
('8a49ccd17e4d46699dbbefe502d5dee2','财务部','108d96767e7d4789b770666b2a29a668',0,NULL,'2022-05-05 14:05:45',NULL),
('a52f6bb4e24344f6b8f2e3a48d28a007','客服部','108d96767e7d4789b770666b2a29a668',0,NULL,'2022-05-05 14:10:54',NULL),
('d68b62ed4e60495c93f2477298205745','软件研发团队','20cd1ab32a21409daf025d6b19063efe',0,NULL,'2022-05-05 14:12:20',NULL);

/*Table structure for table `pt_role` */

DROP TABLE IF EXISTS `pt_role`;

CREATE TABLE `pt_role` (
  `role_uuid` varchar(255) NOT NULL COMMENT '角色资源ID',
  `role_name` varchar(32) DEFAULT NULL COMMENT '角色名称',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

/*Data for the table `pt_role` */

insert  into `pt_role`(`role_uuid`,`role_name`,`remarks`) values 
('025efd36f5394bdc81b6a67565905642','网络人员','网络人员'),
('0273627428ea4d58a8a7b66d43bc8cb3','网络人员','普通人员'),
('027e7cf3da444a5fb87cd38413642006','设计人员','设计人员'),
('02c530502b4743419e2ae16844a9ee33','行政人员','行政人员'),
('02cdbe97a5c04b998633756a41f3abe8','软件研发','软件研发'),
('02ce90675f7e4fa29c919dd8ac09f0d6','财务人员','财务人员'),
('999','系统管理员','系统管理员');

/*Table structure for table `pt_role_menu` */

DROP TABLE IF EXISTS `pt_role_menu`;

CREATE TABLE `pt_role_menu` (
  `ROLE_UUID` varchar(255) NOT NULL COMMENT '角色资源ID',
  `menu_id` int(20) NOT NULL DEFAULT '0' COMMENT '菜单ID',
  PRIMARY KEY (`ROLE_UUID`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色菜单关联表';

/*Data for the table `pt_role_menu` */

/*Table structure for table `pt_role_user` */

DROP TABLE IF EXISTS `pt_role_user`;

CREATE TABLE `pt_role_user` (
  `USER_UUID` varchar(255) NOT NULL,
  `ROLE_UUID` varchar(255) NOT NULL,
  PRIMARY KEY (`USER_UUID`,`ROLE_UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色关联表';

/*Data for the table `pt_role_user` */

/*Table structure for table `pt_security_log` */

DROP TABLE IF EXISTS `pt_security_log`;

CREATE TABLE `pt_security_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `user_uuid` varchar(255) NOT NULL,
  `log_system` int(11) NOT NULL COMMENT '子系统',
  `log_module` int(11) NOT NULL COMMENT '模块',
  `log_action` int(11) NOT NULL COMMENT '操作动作',
  `log_contents` varchar(256) NOT NULL COMMENT '操作内容',
  `create_time` datetime NOT NULL COMMENT '操作时间',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='安全日志';

/*Data for the table `pt_security_log` */

/*Table structure for table `pt_sys_config` */

DROP TABLE IF EXISTS `pt_sys_config`;

CREATE TABLE `pt_sys_config` (
  `cfg_id` int(11) NOT NULL AUTO_INCREMENT,
  `cfg_name` varchar(64) NOT NULL,
  `cfg_value` varchar(64) NOT NULL,
  `remarks` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`cfg_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统配置表';

/*Data for the table `pt_sys_config` */

insert  into `pt_sys_config`(`cfg_id`,`cfg_name`,`cfg_value`,`remarks`) values 
(14,'jdbc.driver','com.mysql.jdbc.Driver','mysql驱动名'),
(15,'jdbc.url','jdbc:mysql://127.0.0.1:3306/gdrt_data?useUnicode=true&characterE','远程数据库标识符'),
(16,'jdbc.username','gdrt_data','mysql数据库用户名'),
(17,'jdbc.password','Huawei_123','mysql数据库密码'),
(18,'project.url','http://localhost:8080/gqgy/sm/login','项目入口'),
(19,'project.name','gqgy','项目名称'),
(20,'isAuthCode','false','开启登录的验证码'),
(21,'siteLanguage','zh','默认语方类型'),
(22,'test.info3','info3','测试数据'),
(23,'test.info4','info4','测试数据');

/*Table structure for table `pt_user` */

DROP TABLE IF EXISTS `pt_user`;

CREATE TABLE `pt_user` (
  `USER_UUID` varchar(255) NOT NULL,
  `DEPID` varchar(32) DEFAULT NULL COMMENT '部门ID',
  `USER_NAME` varchar(32) DEFAULT NULL COMMENT '登录名',
  `PASSWORD` varchar(32) DEFAULT NULL COMMENT '密码',
  `EMAIL` varchar(32) DEFAULT NULL COMMENT '邮件地址',
  `MOBILE` varchar(255) DEFAULT NULL COMMENT '电话',
  `NICE_NAME` varchar(64) DEFAULT NULL COMMENT '真实姓名',
  `REGISTERDATE` datetime DEFAULT NULL COMMENT '注册日期',
  `REMARK` varchar(512) DEFAULT NULL COMMENT '备注',
  `DEL_FLAG` int(11) NOT NULL COMMENT '是否删除标识 0:正常 1:删除',
  `MODTIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `MODIFIERID` varchar(32) DEFAULT NULL COMMENT '修改人ID',
  `IS_SUM` int(11) DEFAULT NULL COMMENT '是否阳光用户（0否1是）',
  `locked` int(11) NOT NULL DEFAULT '0' COMMENT '是否已锁定',
  `last_input_error` datetime DEFAULT NULL COMMENT '最后输入错误时间',
  `input_error_num` tinyint(2) NOT NULL DEFAULT '0' COMMENT '输入错误临时字段',
  PRIMARY KEY (`USER_UUID`),
  UNIQUE KEY `mobileIndex` (`MOBILE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

/*Data for the table `pt_user` */

insert  into `pt_user`(`USER_UUID`,`DEPID`,`USER_NAME`,`PASSWORD`,`EMAIL`,`MOBILE`,`NICE_NAME`,`REGISTERDATE`,`REMARK`,`DEL_FLAG`,`MODTIME`,`MODIFIERID`,`IS_SUM`,`locked`,`last_input_error`,`input_error_num`) values 
('001fddf8608c4cd1bbac81dde4e0c701','','zhaowei','ae00cfce6cf6566c7dc53015ca184c1a','zhaowei@isoftstone.com','13131363553','赵伟','2020-07-30 16:16:20',NULL,0,'2022-07-04 20:09:53',NULL,0,0,NULL,0),
('011d83b63034470ab6225a2ead643cf7','','hanguobing','ae00cfce6cf6566c7dc53015ca184c1a','hanguobing@isoftstone.com','13605350862','韩国兵','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('01635ed157e445bba6725675bdd8ff67','','renjunxy','ae00cfce6cf6566c7dc53015ca184c1a','renjunxy@isoftstone.com','13846028986','任俊','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('02d7938e06934f48b92baec5348a8609','','yanwenyi','ae00cfce6cf6566c7dc53015ca184c1a','yanwenyi@isoftstone.com','13897145171','严文毅','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('0311e42abab4467a80aefd2ebd18ab19','','zhangjifang','ae00cfce6cf6566c7dc53015ca184c1a','zhangjifang@isoftstone.com','13124524956','张吉方','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('04325342590745fdad116befc6e812a8','','lijuan','ae00cfce6cf6566c7dc53015ca184c1a','lijuan@isoftstone.com','13213283541','李娟','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('04a2c1b1fa1f4873be7d67c5733d2981','','luochun','ae00cfce6cf6566c7dc53015ca184c1a','luochun@isoftstone.com','13183712498','罗春','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('055103a1d7f5488fb1cc0745cbda4d9c','','chenwenjie','ae00cfce6cf6566c7dc53015ca184c1a','chenwenjie@isoftstone.com','13114784084','陈文杰','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('058f83fb909a43749c52f14116952670','','keengai','ae00cfce6cf6566c7dc53015ca184c1a','keengai@isoftstone.com','13209879957','柯恩概','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('06013e11d26e4ccab314b1c174b91e63','','zhaoyz','ae00cfce6cf6566c7dc53015ca184c1a','zhaoyz@isoftstone.com','13115968208','赵远征','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('06a20d3d24e0498d9a490503dfcbd9b9','','yuanzhihong','ae00cfce6cf6566c7dc53015ca184c1a','yuanzhihong@isoftstone.com','13559513201','原志红','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('0797c94659eb4e32a3941c012b7ebf10','','maojun','ae00cfce6cf6566c7dc53015ca184c1a','maojun@isoftstone.com','13167868826','毛军','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('093e97d0e47145a6a3822453f98326bd','','tuchaoyang','ae00cfce6cf6566c7dc53015ca184c1a','tuchaoyang@isoftstone.com','13169509946','涂朝阳','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('09595229a16f4fdcae8fdf98dabe21b3','','yuant','ae00cfce6cf6566c7dc53015ca184c1a','yuant@isoftstone.com','13379951768','袁涛','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('0961af8780fc48b3bfd38b6a44fb19c5','','kongzhanhui','ae00cfce6cf6566c7dc53015ca184c1a','kongzhanhui@isoftstone.com','13354505681','孔占辉','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('09a0813d51cf4e0bac8ba367514dafb8','','bimosi','ae00cfce6cf6566c7dc53015ca184c1a','bimosi@isoftstone.com','13116380059','毕默思','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('0a39169737f5486188c670c37055743b','','shentao','ae00cfce6cf6566c7dc53015ca184c1a','shentao@isoftstone.com','13152522791','沈涛','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('0a3a90505c974e11bf6be3949b1dacec','','sunpengpeng','ae00cfce6cf6566c7dc53015ca184c1a','sunpengpeng@isoftstone.com','13186628566','孙芃芃','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('0a61822c17fd46de8cbedb3d0002c9a1','','zhangjinguang','ae00cfce6cf6566c7dc53015ca184c1a','zhangjinguang@isoftstone.com','13116307287','张仅广','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('0a82744a13544d848f392336c9fe4869','','wufangtong','ae00cfce6cf6566c7dc53015ca184c1a','wufangtong@isoftstone.com','13203443629','吴方童','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('0b08038ce01e49288e10e8b3cf599bfd','','futigang','ae00cfce6cf6566c7dc53015ca184c1a','futigang@isoftstone.com','13479965379','付体刚','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('0b1d54b1173340b695373f5b70821d44','','zhangkui','ae00cfce6cf6566c7dc53015ca184c1a','zhangkui@isoftstone.com','13144595977','张葵','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('0d8372ae480d4689a21c37f24030032f','','yangbo','ae00cfce6cf6566c7dc53015ca184c1a','yangbo@isoftstone.com','13141291016','杨波','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('0d91ae8cdc974f45949d61743db4adb8','','zhaogang','ae00cfce6cf6566c7dc53015ca184c1a','zhaogang@isoftstone.com','13196039753','赵刚','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('0e10a6f6aec142fbaa589019a01215da','','dangyinjie','ae00cfce6cf6566c7dc53015ca184c1a','dangyinjie@isoftstone.com','13187326021','党颖杰','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('0ef04e5632724ff88e98c19e1679aff7','','pengmin','ae00cfce6cf6566c7dc53015ca184c1a','pengmin@isoftstone.com','13166580218','彭民','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('0ef1e4f60507425bb4b5a2175034899f','','zhoujie','ae00cfce6cf6566c7dc53015ca184c1a','zhoujie@isoftstone.com','13279671855','周洁','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('0f7e1ecc72b34801b5319c4a82275e87','','wuguangyu','ae00cfce6cf6566c7dc53015ca184c1a','wuguangyu@isoftstone.com','13176693840','武光煜','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('0f9ba7ca61c84482af83613033a59344','','lizhongwei','ae00cfce6cf6566c7dc53015ca184c1a','lizhongwei@isoftstone.com','13190914854','李忠伟','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('10189fc76d2144c788962162183497aa','','sundonghai','ae00cfce6cf6566c7dc53015ca184c1a','sundonghai@isoftstone.com','13605611312','孙东海','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('101e70ad064442b2aa4a673306dde6b4','','xubaowei','ae00cfce6cf6566c7dc53015ca184c1a','xubaowei@isoftstone.com','13205121028','许保卫','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('105f4e5a8c134930b15559edd34048ea','','qiumingda','ae00cfce6cf6566c7dc53015ca184c1a','qiumingda@isoftstone.com','13764361611','蔡丽娜','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('11fa0a60ac5444ebbdf9c6048891bf28','','majian','ae00cfce6cf6566c7dc53015ca184c1a','majian@isoftstone.com','13533708438','马剑','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('12037d5d995a42a19753211dcaea35bd','','zhangliping','ae00cfce6cf6566c7dc53015ca184c1a','zhangliping@isoftstone.com','13202432376','张利平','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('1257c1d3eebc4f4db10fbd05279426c7','','jiangshiding','ae00cfce6cf6566c7dc53015ca184c1a','jiangshiding@isoftstone.com','13211042155','蒋时丁','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('1258404ff23a4fe7a0bba8f64015ae0d','','liaofeng','ae00cfce6cf6566c7dc53015ca184c1a','liaofeng@isoftstone.com','13832857464','廖峰','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('127be92238e54e15a34ec5ad4339fa7d','','tangzhongchao','ae00cfce6cf6566c7dc53015ca184c1a','tangzhongchao@isoftstone.com','13729906102','唐忠超','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('12a026f21cae4412b9377a8e2395c567','','fujinyou','ae00cfce6cf6566c7dc53015ca184c1a','fujinyou@isoftstone.com','13475556065','傅金友','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('12dd8bbf6151477ca87c92b9bdade0c6','','wangbiao','ae00cfce6cf6566c7dc53015ca184c1a','wangbiao@isoftstone.com','13174153701','王彪','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('12f16a02f96f43369c5fb1d27a464c22','','liweiming','ae00cfce6cf6566c7dc53015ca184c1a','liweiming@isoftstone.com','13141142128','李卫民','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('133fa78663f34b58bdc9c7fcaabcb8cb','','zhanghaibin','ae00cfce6cf6566c7dc53015ca184c1a','zhanghaibin@isoftstone.com','13214477191','张海滨','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('135e05c9c50b489bbbfa5acef19b1bf0','','zhangyunjie','ae00cfce6cf6566c7dc53015ca184c1a','zhangyunjie@isoftstone.com','13214579959','张运杰','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('135e3d4f38ac41fcb205e1287acad845','','haobo','ae00cfce6cf6566c7dc53015ca184c1a','haobo@isoftstone.com','13210498120','郝波','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('1398f60d22ef4c37b5c62f3dd8a74ea0','','xiaogj','ae00cfce6cf6566c7dc53015ca184c1a','xiaogj@isoftstone.com','13233709971','肖光俊','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('13a7441474ea4598875ea7ce5c8a65ad','','liuyabin','ae00cfce6cf6566c7dc53015ca184c1a','liuyabin@isoftstone.com','13173810146','刘亚斌','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('145b31712798481299e407f9df34d744','','dangyuhai','ae00cfce6cf6566c7dc53015ca184c1a','dangyuhai@isoftstone.com','13196049771','党玉海','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('151e5f758b264ee49efb853e031e027b','','wenjg','ae00cfce6cf6566c7dc53015ca184c1a','wenjg@isoftstone.com','13714962327','温靖国','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('15b1ef70324343fc8100e6b110200d45','','zggd_test','ae00cfce6cf6566c7dc53015ca184c1a','zggd_test@isoftstone.com','13175003667','测试用户','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('15f6273fdc754978a9f7d871dd4d02db','','dujuntao','ae00cfce6cf6566c7dc53015ca184c1a','dujuntao@isoftstone.com','13151241695','杜军涛','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('177149e3a41b4683a2aade481bcf2946','','cenxiaolu','ae00cfce6cf6566c7dc53015ca184c1a','cenxiaolu@isoftstone.com','13492165773','岑小路','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('17ac9b7abcc24e0db9f0d2b085ec4456','','yangyanchun','ae00cfce6cf6566c7dc53015ca184c1a','yangyanchun@isoftstone.com','13222456454','杨艳春','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('180f3655bc914942b08f9f52cee7ad4d','','hanchanglong','ae00cfce6cf6566c7dc53015ca184c1a','hanchanglong@isoftstone.com','13115925415','韩长龙','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('1829b1ecd0ce4d1fb734b8dba545c801','','chenqing','ae00cfce6cf6566c7dc53015ca184c1a','chenqing@isoftstone.com','13100628098','陈清','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('18d5e5ff3d104c4e9c0bf07c2e56d71f','','caijimin','ae00cfce6cf6566c7dc53015ca184c1a','caijimin@isoftstone.com','13146760337','才济民','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('1943961b19764eebb66d8a3a0e4324c5','','tanxuguang','ae00cfce6cf6566c7dc53015ca184c1a','tanxuguang@isoftstone.com','13721909068','檀旭光','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('1a36b4c2dc6a4c5eb680fd1b8439ae98','','zhangwei','ae00cfce6cf6566c7dc53015ca184c1a','zhangwei@isoftstone.com','13925030234','张伟','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('1a3df7c2f01541d8ad192cda96ac18ff','','liujiguang','ae00cfce6cf6566c7dc53015ca184c1a','liujiguang@isoftstone.com','13 54701742','刘继光','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('1a89a19338dc4afabee3b104aab72d82','','heyunzhong','ae00cfce6cf6566c7dc53015ca184c1a','heyunzhong@isoftstone.com','13234492752','何运忠','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('1bb7b0797b2240f7ac0c05d5f97d696e','','hejunliang','ae00cfce6cf6566c7dc53015ca184c1a','hejunliang@isoftstone.com','13204533426','和军梁','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('1c5bd24fe2e54b8daee18ce10a92c4a2','','yueheng','ae00cfce6cf6566c7dc53015ca184c1a','yueheng@isoftstone.com','13212705634','岳衡','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('1cb3253577f948e3b4287a8014bda341','','wangpu','ae00cfce6cf6566c7dc53015ca184c1a','wangpu@isoftstone.com','13126495609','王璞','2020-07-30 16:16:20',NULL,0,'2022-07-04 15:44:27',NULL,0,0,NULL,0),
('999','','admin','ae00cfce6cf6566c7dc53015ca184c1a','houbaoqing@isoftstone.com','13786802220','辛风华','2020-07-30 16:16:20',NULL,0,'2022-07-04 20:05:59',NULL,0,0,NULL,0);

/*Table structure for table `pt_user_duty` */

DROP TABLE IF EXISTS `pt_user_duty`;

CREATE TABLE `pt_user_duty` (
  `USER_UUID` varchar(255) NOT NULL DEFAULT '' COMMENT '用户ID',
  `DUTYID` varchar(32) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`USER_UUID`,`DUTYID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户岗位关联表';

/*Data for the table `pt_user_duty` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
