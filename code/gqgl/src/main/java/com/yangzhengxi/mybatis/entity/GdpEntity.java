package com.yangzhengxi.mybatis.entity;

import com.yangzhengxi.mybatis.annotation.Columns;

import java.io.Serializable;

/**
 * @Author yyds
 * @Date 2022/7/4
 * @ClassName:GdpEntity
 * @Version 1.0
 */
public class GdpEntity implements Serializable {
    @Columns("my_date")
    private String myDate;
    @Columns("gdp_value")
    private Double gdpValue;
    @Columns("gdp_up")
    private Float gdpUp;
    @Columns("gdp_id")
    private Integer gdpId;

    public String getMyDate() {
        return myDate;
    }

    public void setMyDate(String myDate) {
        this.myDate = myDate;
    }

    public Double getGdpValue() {
        return gdpValue;
    }

    public void setGdpValue(Double gdpValue) {
        this.gdpValue = gdpValue;
    }

    public Float getGdpUp() {
        return gdpUp;
    }

    public void setGdpUp(Float gdpUp) {
        this.gdpUp = gdpUp;
    }

    public Integer getGdpId() {
        return gdpId;
    }

    public void setGdpId(Integer gdpId) {
        this.gdpId = gdpId;
    }

    @Override
    public String toString() {
        return "GdpEntity{" +
                "myDate='" + myDate + '\'' +
                ", gdpValue=" + gdpValue +
                ", gdpUp=" + gdpUp +
                ", gdpId=" + gdpId +
                '}';
    }
}
