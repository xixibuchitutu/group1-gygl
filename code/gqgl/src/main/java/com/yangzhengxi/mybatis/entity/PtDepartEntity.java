package com.yangzhengxi.mybatis.entity;

import com.yangzhengxi.mybatis.annotation.Columns;

import java.io.Serializable;
import java.sql.Date;


/**
 * 菜单实体类
 */

public class PtDepartEntity implements Serializable {
    @Columns("DEP_UUID")
    private String depUuid;
    @Columns("ORGAN_UUID")
    private String organUuid;
    @Columns("BRANCH_NAME")
    private String branchName;
    @Columns("BELONG_CENTER")
    private String belongCenter;
    @Columns("MODIFIERID")
    private String modifierId;
    @Columns("MODTIME")
    private Date modTime;
    @Columns("DEL_FLAG")
    private String delFlag;
    @Columns("REMARK")
    private String remark;
    @Columns("USER_UUID")
    private String userUuid;
    @Columns("DEPID")
    private String depId;
    @Columns("USER_NAME")
    private String userName;
    @Columns("PASSWORD")
    private String password;
    @Columns("EMAIL")
    private String email;
    @Columns("MOBILE")
    private  String mobile;
    @Columns("NICE_NAME")
    private String niceName;
    @Columns("REGISTERDATE")
    private  String registerDate;
    @Columns("IS_SUM")
    private String isSum;


    public String getDepUuid() {
        return depUuid;
    }

    public void setDepUuid(String depUuid) {
        this.depUuid = depUuid;
    }

    public String getOrganUuid() {
        return organUuid;
    }

    public void setOrganUuid(String organUuid) {
        this.organUuid = organUuid;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBelongCenter() {
        return belongCenter;
    }

    public void setBelongCenter(String belongCenter) {
        this.belongCenter = belongCenter;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId;
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getRemark() {
        return remark;
    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    public String getDepId() {
        return depId;
    }

    public void setDepId(String depId) {
        this.depId = depId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNiceName() {
        return niceName;
    }

    public void setNiceName(String niceName) {
        this.niceName = niceName;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    public String getIsSum() {
        return isSum;
    }

    public void setIsSum(String isSum) {
        this.isSum = isSum;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "PtDepartEntity{" +
                "depUuid='" + depUuid + '\'' +
                ", organUuid='" + organUuid + '\'' +
                ", branchName='" + branchName + '\'' +
                ", belongCenter='" + belongCenter + '\'' +
                ", modifierId='" + modifierId + '\'' +
                ", modTime=" + modTime +
                ", delFlag='" + delFlag + '\'' +
                ", remark='" + remark + '\'' +
                ", userUuid='" + userUuid + '\'' +
                ", depId='" + depId + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", mobile='" + mobile + '\'' +
                ", niceName='" + niceName + '\'' +
                ", registerDate='" + registerDate + '\'' +
                ", isSum='" + isSum + '\'' +
                '}';
    }
}
