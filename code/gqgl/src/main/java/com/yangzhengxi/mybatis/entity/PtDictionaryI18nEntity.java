package com.yangzhengxi.mybatis.entity;


import com.yangzhengxi.mybatis.annotation.Columns;

import java.io.Serializable;

public class PtDictionaryI18nEntity implements Serializable {
    /**
     * 字典表id
     */
    @Columns("dict_id")
    private String dictId;
    /**
     * 字典表名称
     */
    @Columns("dict_name")
    private String dictName;
    /**
     * 字典表语言
     */
    @Columns("language_id")
    private Integer languageId;

    public String getDictId() {
        return dictId;
    }

    public void setDictId(String dictId) {
        this.dictId = dictId;
    }

    public String getDictName() {
        return dictName;
    }

    public void setDictName(String dictName) {
        this.dictName = dictName;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    @Override
    public String toString() {
        return "PtDictionaryI18n{" +
                "dictId='" + dictId + '\'' +
                ", dictName='" + dictName + '\'' +
                ", languageId=" + languageId +
                '}';
    }
}
