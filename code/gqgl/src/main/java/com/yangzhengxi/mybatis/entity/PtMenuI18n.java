package com.yangzhengxi.mybatis.entity;


import com.yangzhengxi.mybatis.annotation.Columns;

import java.io.Serializable;

public class PtMenuI18n implements Serializable {
    /**
     * 菜单ID
     */
    @Columns("menu_id")
    private String menuId;
    /**
     * 语言ID
     */
    @Columns("lang_id")
    private Integer langId;
    /**
     * 国际化菜单名称
     */
    @Columns("menu_name")
    private String menuName;

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public Integer getLangId() {
        return langId;
    }

    public void setLangId(Integer langId) {
        this.langId = langId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    @Override
    public String toString() {
        return "PtMenuI18n{" +
                "menuId='" + menuId + '\'' +
                ", langId=" + langId +
                ", menuName='" + menuName + '\'' +
                '}';
    }
}

