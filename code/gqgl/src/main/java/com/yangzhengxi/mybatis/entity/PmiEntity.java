package com.yangzhengxi.mybatis.entity;

import com.yangzhengxi.mybatis.annotation.Columns;

import java.io.Serializable;

public class PmiEntity implements Serializable {
    @Columns("my_date")
    private String myDate;
    @Columns("pmi_value")
    private String pmiValue;
    @Columns("pmi_id")
    private Integer pmiId;

    public String getMyDate() {
        return myDate;
    }
    public void setMyDate(String myDate) {
        this.myDate = myDate;
    }

    public String getPmiValue() {
        return pmiValue;
    }
    public void setPmiValue(String pmiValue) {
        this.pmiValue = pmiValue;
    }

    public Integer getPmiId() {
        return pmiId;
    }
    public void setPmiId(Integer pmiId) {
        this.pmiId = pmiId;
    }

    @Override
    public String toString() {
        return "PmiEntity{" +
                "myDate='" + myDate + '\'' +
                ", pmiValue=" + pmiValue +
                ", pmiId=" + pmiId +
                '}';
    }
}
