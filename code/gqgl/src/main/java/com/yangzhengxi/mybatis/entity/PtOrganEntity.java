package com.yangzhengxi.mybatis.entity;

import com.yangzhengxi.mybatis.annotation.Columns;
import lombok.Data;

import java.io.Serializable;

@Data
public class PtOrganEntity implements Serializable {
   @Columns("ORGAN_UUID")
   String organUuid;
   @Columns("ORGAN_NAME")
   String organName;
   @Columns("PARENT_UUID")
   String parentUuid;
   @Columns("DEL_FLAG")
   Integer delFlage;
   @Columns("MODIFIERID")
   String modifierid;
   @Columns("MODTIME")
   String moditime;
   @Columns("DESCRIPTION")
   String description;
   public String getOrganUuid() {
      return organUuid;
   }

   public void setOrganUuid(String organUuid) {
      this.organUuid = organUuid;
   }

   public String getOrganName() {
      return organName;
   }

   public void setOrganName(String organName) {
      this.organName = organName;
   }

   public String getParentUuid() {
      return parentUuid;
   }

   public void setParentUuid(String parentUuid) {
      this.parentUuid = parentUuid;
   }

   public Integer getDelFlage() {
      return delFlage;
   }

   public void setDelFlage(Integer delFlage) {
      this.delFlage = delFlage;
   }

   public String getModifierid() {
      return modifierid;
   }

   public void setModifierid(String modifierid) {
      this.modifierid = modifierid;
   }

   public String getModitime() {
      return moditime;
   }

   public void setModitime(String moditime) {
      this.moditime = moditime;
   }

   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }
}
