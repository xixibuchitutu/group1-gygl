package com.yangzhengxi.mybatis.entity;

import java.io.Serializable;
import java.math.BigInteger;

public class PtDictionaryEntity implements Serializable {
    private BigInteger dictId;
    private String dictGroup;
    private String dicCode;
    private String description;
    private Integer dicSort;
    private BigInteger dictParentId;
    public BigInteger getDictParentId() {
        return dictParentId;
    }

    public void setDictParentId(BigInteger dictParentId) {
        this.dictParentId = dictParentId;
    }
    public BigInteger getDictId() {
        return dictId;
    }

    public void setDictId(BigInteger dictId) {
        this.dictId = dictId;
    }

    public String getDictGroup() {
        return dictGroup;
    }

    public void setDictGroup(String dictGroup) {
        this.dictGroup = dictGroup;
    }

    public String getDicCode() {
        return dicCode;
    }

    public void setDicCode(String dicCode) {
        this.dicCode = dicCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDicSort() {
        return dicSort;
    }

    public void setDicSort(Integer dicSort) {
        this.dicSort = dicSort;
    }

    @Override
    public String toString() {
        return "PtDictionaryEntity{" +
                "dictId='" + dictId + '\'' +
                ", dictGroup='" + dictGroup + '\'' +
                ", dicCode='" + dicCode + '\'' +
                ", description='" + description + '\'' +
                ", dicSort=" + dicSort +
                '}';
    }
}
