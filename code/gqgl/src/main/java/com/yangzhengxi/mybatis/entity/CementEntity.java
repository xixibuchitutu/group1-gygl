package com.yangzhengxi.mybatis.entity;

import com.yangzhengxi.mybatis.annotation.Columns;

import java.io.Serializable;

/**
 * @Author yyds
 * @Date 2022/7/4
 * @ClassName:GdpEntity
 * @Version 1.0
 */
public class CementEntity implements Serializable {
    @Columns("cement_date")
    private String cementDate;
    @Columns("cement_production")
    private String cementProduction;
    @Columns("cement_compare")
    private String cementCompare;
    @Columns("cement_id")
    private Integer cementId;

    public String getCementDate() {
        return cementDate;
    }

    public void setCementDate(String cementDate) {
        this.cementDate = cementDate;
    }

    public String getCementProduction() {
        return cementProduction;
    }

    public void setCementProduction(String cementProduction) {
        this.cementProduction = cementProduction;
    }

    public String getCementCompare() {
        return cementCompare;
    }

    public void setCementCompare(String cementCompare) {
        this.cementCompare = cementCompare;
    }

    public Integer getCementId() {
        return cementId;
    }

    public void setCementId(Integer cementId) {
        this.cementId = cementId;
    }

    @Override
    public String toString() {
        return "CementEntity{" +
                "cementDate='" + cementDate + '\'' +
                ", cementProduction='" + cementProduction + '\'' +
                ", cementCompare='" + cementCompare + '\'' +
                ", cementId=" + cementId +
                '}';
    }
}
