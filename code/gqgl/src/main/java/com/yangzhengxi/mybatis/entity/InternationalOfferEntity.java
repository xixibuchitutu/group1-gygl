package com.yangzhengxi.mybatis.entity;

import com.yangzhengxi.mybatis.annotation.Columns;

import java.io.Serializable;

public class InternationalOfferEntity implements Serializable {
    @Columns("offer_id")
    private Integer offerId;
    @Columns("offer_date")
    private String offerDate;
    @Columns("calorific_value")
    private Integer calorificValue;
    @Columns("origin_country")
    private String originCountry;
    @Columns("price")
    private Double price;
    @Columns("price_terms")
    private String priceTerms;
    @Columns("number")
    private String number;
    @Columns("delivery_port")
    private String deliveryPort;
    @Columns("shipment_time")
    private String shipmentTime;


    public Integer getOfferId() {
        return offerId;
    }

    public void setOfferId(Integer offerId) {
        this.offerId = offerId;
    }

    public String getOfferDate() {
        return offerDate;
    }

    public void setOfferDate(String offerDate) {
        this.offerDate = offerDate;
    }

    public Integer getCalorificValue() {
        return calorificValue;
    }

    public void setCalorificValue(Integer calorificValue) {
        this.calorificValue = calorificValue;
    }

    public String getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(String originCountry) {
        this.originCountry = originCountry;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getPriceTerms() {
        return priceTerms;
    }

    public void setPriceTerms(String priceTerms) {
        this.priceTerms = priceTerms;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDeliveryPort() {
        return deliveryPort;
    }

    public void setDeliveryPort(String deliveryPort) {
        this.deliveryPort = deliveryPort;
    }

    public String getShipmentTime() {
        return shipmentTime;
    }

    public void setShipmentTime(String shipmentTime) {
        this.shipmentTime = shipmentTime;
    }



    @Override
    public String toString() {
        return "InternationalOfferEntity{" +
                "offerId=" + offerId +
                ", offerDate='" + offerDate + '\'' +
                ", calorificValue=" + calorificValue +
                ", originCountry='" + originCountry + '\'' +
                ", price=" + price +
                ", priceTerms='" + priceTerms + '\'' +
                ", number='" + number + '\'' +
                ", deliveryPort='" + deliveryPort + '\'' +
                ", shipmentTime='" + shipmentTime + '\'' +
                '}';
    }
}
