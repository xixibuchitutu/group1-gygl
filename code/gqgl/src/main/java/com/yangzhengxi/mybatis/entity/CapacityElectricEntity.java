package com.yangzhengxi.mybatis.entity;

import com.yangzhengxi.mybatis.annotation.Columns;

import java.io.Serializable;

public class CapacityElectricEntity implements Serializable {
    @Columns("elc_id")
    private Integer elcId;
    @Columns("elc_date")
    private String elcDate;
    @Columns("elc_capacity")
    private Double elcCapacity;
    @Columns("elc_grew")
    private Double elcGrew;

    public Integer getElcId() {
        return elcId;
    }

    public void setElcId(Integer elcId) {
        this.elcId = elcId;
    }

    public String getElcDate() {
        return elcDate;
    }

    public void setElcDate(String elcDate) {
        this.elcDate = elcDate;
    }

    public Double getElcCapacity() {
        return elcCapacity;
    }

    public void setElcCapacity(Double elcCapacity) {
        this.elcCapacity = elcCapacity;
    }

    public Double getElcGrew() { return elcGrew; }

    public void setElcGrew(Double elcGrew) {
        this.elcGrew = elcGrew;
    }

    @Override
    public String toString() {
        return "CapacityElectricEntity{" +
                "elcId=" + elcId +
                ", elcDate='" + elcDate + '\'' +
                ", elcCapacity=" + elcCapacity +
                ", elcGrew=" + elcGrew +
                '}';
    }
}
