package com.yangzhengxi.mybatis.entity;

import com.yangzhengxi.mybatis.annotation.Columns;

import java.io.Serializable;
import java.util.Date;


public class PtLogEntity implements Serializable {
    @Columns("log_id")
    private int logId;
    @Columns("request_url")
    private String requestUrl;
    @Columns("start_time")
    private Date startTime;
    @Columns("end_time")
    private Date endTime;
    @Columns("run_time")
    private long runTime;
    @Columns("method_name")
    private String methodName;
    @Columns("method_param")
    private String methodParam;
    @Columns("log_contents")
    private String logContents;
    @Columns("user_code")
    private String userCode;

    public int getLogId() {
        return logId;
    }

    public void setLogId(int logId) {
        this.logId = logId;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public long getRunTime() {
        return runTime;
    }

    public void setRunTime(long runTime) {
        this.runTime = runTime;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getMethodParam() {
        return methodParam;
    }

    public void setMethodParam(String methodParam) {
        this.methodParam = methodParam;
    }

    public String getLogContents() {
        return logContents;
    }

    public void setLogContents(String logContents) {
        this.logContents = logContents;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    @Override
    public String toString() {
        return "PtLogEntity{" +
                "logId=" + logId +
                ", requestUrl='" + requestUrl + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", methodName='" + methodName + '\'' +
                ", methodParam='" + methodParam + '\'' +
                ", logContents='" + logContents + '\'' +
                ", userCode='" + userCode + '\'' +
                '}';
    }
}
