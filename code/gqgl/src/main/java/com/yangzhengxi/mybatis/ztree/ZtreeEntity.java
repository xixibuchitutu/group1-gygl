package com.yangzhengxi.mybatis.ztree;


import java.io.Serializable;
public class ZtreeEntity implements Serializable {
    private String id = "0";
    private String pId = "-1";
    private Boolean checked = false;
    private Boolean open = true;
    private String name = "";

    public ZtreeEntity(){

    }

    public ZtreeEntity(String id,String name,String pId){
        this.id = id;
        this.name = name;
        this.pId = pId;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public Boolean getOpen() {
        return open;
    }

    public void setOpen(Boolean open) {
        this.open = open;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ZtreeEntity{" +
                "id='" + id + '\'' +
                ", pId='" + pId + '\'' +
                ", checked=" + checked +
                ", open=" + open +
                ", name='" + name + '\'' +
                '}';
    }
}
