package com.yangzhengxi.mybatis.entity;


import com.yangzhengxi.mybatis.annotation.Columns;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户信息实体类
 */
public class PtUserEntity implements Serializable {
    /**
     * 用户编码
     */
    @Columns("USER_UUID")
    private String userUuid;
    /**
     * 用户密码
     */
    private String password;
    /**
     * 部门ID
     */
    private String depid;
    /**
     * 用户名称
     */
    @Columns("USER_NAME")
    private String userName;
    /**
     * 邮件地址
     */
    @Columns("EMAIL")
    private String email;
    /**
     * 电话
     */
    @Columns("MOBILE")
    private String mobile;
    /**
     * 真实姓名
     */
    @Columns("NICE_NAME")
    private String niceName;
    /**
     * 注册日期
     */
    private String registerdate;
    /**
     * 备注
     */
    private String remark;
    /**
     * 修改时间
     */
    private String modtime;
    /**
     * 修改人ID
     */
    private String modifierid;

    @Columns("DUTYID")
    private String dutyId;
    @Columns("NAME")
    private String name;
    @Columns("ORGAN_UUID")
    private String organUuid;

    public String getOrganUuid() {
        return organUuid;
    }

    public void setOrganUuid(String organUuid) {
        this.organUuid = organUuid;
    }

    private int locked;

    private int inputErrorNum;

    private Date lastInputError;

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    public String getDepid() {
        return depid;
    }

    public void setDepid(String depid) {
        this.depid = depid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNiceName() {
        return niceName;
    }

    public void setNiceName(String niceName) {
        this.niceName = niceName;
    }

    public String getRegisterdate() {
        return registerdate;
    }

    public void setRegisterdate(String registerdate) {
        this.registerdate = registerdate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getModtime() {
        return modtime;
    }

    public void setModtime(String modtime) {
        this.modtime = modtime;
    }

    public String getModifierid() {
        return modifierid;
    }

    public void setModifierid(String modifierid) {
        this.modifierid = modifierid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getDutyId() {
        return dutyId;
    }

    public void setDutyId(String dutyId) {
        this.dutyId = dutyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "PtUserEntity{" +
                "userUuid='" + userUuid + '\'' +
                ", password='" + password + '\'' +
                ", depid='" + depid + '\'' +
                ", userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", mobile='" + mobile + '\'' +
                ", niceName='" + niceName + '\'' +
                ", registerdate='" + registerdate + '\'' +
                ", remark='" + remark + '\'' +
                ", modtime='" + modtime + '\'' +
                ", modifierid='" + modifierid + '\'' +
                ", dutyId='" + dutyId + '\'' +
                ", name='" + name + '\'' +
                ", organUuid='" + organUuid + '\'' +
                ", locked=" + locked +
                ", inputErrorNum=" + inputErrorNum +
                ", lastInputError=" + lastInputError +
                '}';
    }

    public int getLocked() {
        return locked;
    }

    public void setLocked(int locked) {
        this.locked = locked;
    }

    public int getInputErrorNum() {
        return inputErrorNum;
    }

    public void setInputErrorNum(int inputErrorNum) {
        this.inputErrorNum = inputErrorNum;
    }

    public Date getLastInputError() {
        return lastInputError;
    }

    public void setLastInputError(Date lastInputError) {
        this.lastInputError = lastInputError;
    }

}