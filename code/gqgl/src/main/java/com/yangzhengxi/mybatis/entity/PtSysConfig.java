package com.yangzhengxi.mybatis.entity;


import com.yangzhengxi.mybatis.annotation.Columns;

import java.io.Serializable;

/**
 * 系统配置表
 */
public class PtSysConfig implements Serializable {
    @Columns("cfg_id")
    private Integer cfgId;
    @Columns("cfg_name")
    private String cfgName;
    @Columns("cfg_value")
    private String cfgValue;
    @Columns("remarks")
    private String remarks;

    public Integer getCfgId() {
        return cfgId;
    }

    public void setCfgId(Integer cfgId) {
        this.cfgId = cfgId;
    }

    public String getCfgName() {
        return cfgName;
    }

    public void setCfgName(String cfgName) {
        this.cfgName = cfgName;
    }

    public String getCfgValue() {
        return cfgValue;
    }

    public void setCfgValue(String cfgValue) {
        this.cfgValue = cfgValue;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public String toString() {
        return "PtSysConfig{" +
                "cfgId=" + cfgId +
                ", cfgName='" + cfgName + '\'' +
                ", cfgValue='" + cfgValue + '\'' +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}
