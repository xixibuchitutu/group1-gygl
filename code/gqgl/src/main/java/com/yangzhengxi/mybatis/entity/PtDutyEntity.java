package com.yangzhengxi.mybatis.entity;


import java.io.Serializable;

/**
 * @Author yyds
 * @Date 2022/6/29
 * @ClassName:PtOrganEntity
 * @Version 1.0
 */
public class PtDutyEntity implements Serializable {
    private String dutyId;

    private String name;

    private String organUuid;

    private String roleUuid;

    private String branchName;


    public String getDutyId() {
        return dutyId;
    }

    public void setDutyId(String dutyId) {
        this.dutyId = dutyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganUuid() {
        return organUuid;
    }

    public void setOrganUuid(String organUuid) {
        this.organUuid = organUuid;
    }

    public String getRoleUuid() {
        return roleUuid;
    }

    public void setRoleUuid(String roleUuid) {
        this.roleUuid = roleUuid;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    @Override
    public String toString() {
        return "PtDutyEntity{" +
                "dutyId='" + dutyId + '\'' +
                ", name='" + name + '\'' +
                ", organUuid='" + organUuid + '\'' +
                ", roleUuid='" + roleUuid + '\'' +
                ", branchName='" + branchName + '\'' +
                '}';
    }
}


