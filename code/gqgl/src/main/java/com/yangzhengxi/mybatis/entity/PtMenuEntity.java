package com.yangzhengxi.mybatis.entity;

import java.io.Serializable;

/**
 * 菜单实体类
 */
public class PtMenuEntity implements Serializable {
    private String menuId;
    private String menuName;
    private String parentId;
    private Integer menuLevel;
    private String menuUrl;
    private String privilegeCode;
    private String menuIcon;
    private String menuType;
    private String menuSeq;

    public String getMenuId() {
        return menuId;
    }

    public Integer getMenuLevel() {
        return menuLevel;
    }

    public void setMenuLevel(Integer menuLevel) {
        this.menuLevel = menuLevel;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getPrivilegeCode() {
        return privilegeCode;
    }

    public void setPrivilegeCode(String privilegeCode) {
        this.privilegeCode = privilegeCode;
    }

    public String getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

    public String getMenuType() {
        return menuType;
    }

    public void setMenuType(String menuType) {
        this.menuType = menuType;
    }

    public String getMenuSeq() {
        return menuSeq;
    }

    public void setMenuSeq(String menuSeq) {
        this.menuSeq = menuSeq;
    }

    @Override
    public String toString() {
        return "PtMenuEntity{" +
                "menuId='" + menuId + '\'' +
                ", menuName='" + menuName + '\'' +
                ", parentId='" + parentId + '\'' +
                ", menuLevel=" + menuLevel +
                ", menuUrl='" + menuUrl + '\'' +
                ", menuType='" + menuType + '\'' +
                ", menuSeq='" + menuSeq + '\'' +
                ", privilegeCode='" + privilegeCode + '\'' +
                ", menuIcon='" + menuIcon + '\'' +
                '}';
    }
}

