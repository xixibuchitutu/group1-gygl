package com.yangzhengxi.mybatis.entity;

import com.yangzhengxi.mybatis.annotation.Columns;

import java.io.Serializable;

public class PtRoleEntity implements Serializable {
    @Columns("role_uuid")
    private String roleUuid;
    @Columns("role_name")
    private String roleName;
    @Columns("remarks")
    private String remarks;

    public String getRoleUuid() {
        return roleUuid;
    }

    public void setRoleUuid(String roleUuid) {
        this.roleUuid = roleUuid;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public String toString() {
        return "PtRoleEntity{" +
                "roleUuid='" + roleUuid + '\'' +
                ", roleName='" + roleName + '\'' +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}
