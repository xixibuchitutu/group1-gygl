package com.yangzhengxi.chart.Entity;

import com.yangzhengxi.mybatis.annotation.Columns;

public class ElectricEntity {
    @Columns("")
    Integer elecDataId;
    @Columns("")
    String elecDataDate;
    @Columns("")
    String elecDataFdv;
    @Columns("")
    String elecDataFdy;
    @Columns("")
    String elecDataAcv;
    @Columns("")
    String elecDataAcy;

    public Integer getElecDataId() {
        return elecDataId;
    }

    public void setElecDataId(Integer elecDataId) {
        this.elecDataId = elecDataId;
    }

    public String getElecDataDate() {
        return elecDataDate;
    }

    public void setElecDataDate(String elecDataDate) {
        this.elecDataDate = elecDataDate;
    }

    public String getElecDataFdv() {
        return elecDataFdv;
    }

    public void setElecDataFdv(String elecDataFdv) {
        this.elecDataFdv = elecDataFdv;
    }

    public String getElecDataFdy() {
        return elecDataFdy;
    }

    public void setElecDataFdy(String elecDataFdy) {
        this.elecDataFdy = elecDataFdy;
    }

    public String getElecDataAcv() {
        return elecDataAcv;
    }

    public void setElecDataAcv(String elecDataAcv) {
        this.elecDataAcv = elecDataAcv;
    }

    public String getElecDataAcy() {
        return elecDataAcy;
    }

    public void setElecDataAcy(String elecDataAcy) {
        this.elecDataAcy = elecDataAcy;
    }

    @Override
    public String toString() {
        return "ElectricEntity{" +
                "elecDataId=" + elecDataId +
                ", elecDataDate='" + elecDataDate + '\'' +
                ", elecDataFdv='" + elecDataFdv + '\'' +
                ", elecDataFdy='" + elecDataFdy + '\'' +
                ", elecDataAcv='" + elecDataAcv + '\'' +
                ", elecDataAcy='" + elecDataAcy + '\'' +
                '}';
    }
}
