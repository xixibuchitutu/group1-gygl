package com.yangzhengxi.chart.service.impl;

import com.yangzhengxi.chart.Entity.ElectricEntity;
import com.yangzhengxi.chart.repostiory.ElectricDao;
import com.yangzhengxi.chart.service.ElectricService;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ElectricServiceImpl implements ElectricService {
    @Resource
    private ElectricDao capacityElectricDao;


    @Override
    public List<ElectricEntity> querycariInfo() {
        return capacityElectricDao.querycariInfo();
    }

    @Override
    public PadingRstType<ElectricEntity> querycarInfoListByPage(ElectricEntity capacityElectricEntity, PaddingEntity padding) {
        padding.deal(ElectricEntity.class);
        PadingRstType<ElectricEntity> rstType = new PadingRstType<>();
        rstType.setPage(padding.getPage());
        List<ElectricEntity> list = capacityElectricDao.querycarInfoListByPage(capacityElectricEntity,padding);
        rstType.setRawRecords(list);
        Integer total =  capacityElectricDao.querycarrInfoListByTotal(capacityElectricEntity);;
        rstType.setTotal(total);
        rstType.putItems();

        return rstType;
    }

    @Override
    public List<ElectricEntity> querycariListByDate(String start, String end) {
        return  capacityElectricDao.querycariListByDate(start,end);
    }
}
