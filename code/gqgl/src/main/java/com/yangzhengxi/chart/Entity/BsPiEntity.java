package com.yangzhengxi.chart.Entity;

import com.yangzhengxi.mybatis.annotation.Columns;
import lombok.Data;

@Data
public class BsPiEntity {
   @Columns("")
   private Integer bsPiId;
 @Columns("")
 private String bsPiDate;
 @Columns("")
 private String bsPiBsPi;
 @Columns("")
 private String bsPiHbZj;
 @Columns("")
 private  String bsPiHb;
 @Columns("")

 private String bsPiTbZj;
 @Columns("")

 private String bsPiTb;

 public String getBsPiDate() {
  return bsPiDate;
 }

 public void setBsPiDate(String bsPiDate) {
  this.bsPiDate = bsPiDate;
 }

 public String getBsPiBsPi() {
  return bsPiBsPi;
 }

 public void setBsPiBsPi(String bsPiBsPi) {
  this.bsPiBsPi = bsPiBsPi;
 }

 public String getBsPiHbZj() {
  return bsPiHbZj;
 }

 public void setBsPiHbZj(String bsPiHbZj) {
  this.bsPiHbZj = bsPiHbZj;
 }

 public String getBsPiHb() {
  return bsPiHb;
 }

 public void setBsPiHb(String bsPiHb) {
  this.bsPiHb = bsPiHb;
 }

 public String getBsPiTbZj() {
  return bsPiTbZj;
 }

 public void setBsPiTbZj(String bsPiTbZj) {
  this.bsPiTbZj = bsPiTbZj;
 }

 public String getBsPiTb() {
  return bsPiTb;
 }

 public void setBsPiTb(String bsPiTb) {
  this.bsPiTb = bsPiTb;
 }

 @Override
 public String toString() {
  return "BsPiEntity{" +
          "bsPiId=" + bsPiId +
          ", bsPiDate='" + bsPiDate + '\'' +
          ", bsPiBsPi=" + bsPiBsPi +
          ", bsPiHbZj=" + bsPiHbZj +
          ", bsPiHb=" + bsPiHb +
          ", bsPiTbZj=" + bsPiTbZj +
          ", bsPiTb=" + bsPiTb +
          '}';
 }

 public Integer getBsPiId() {
  return bsPiId;
 }

 public void setBsPiId(Integer bsPiId) {
  this.bsPiId = bsPiId;
 }
}
