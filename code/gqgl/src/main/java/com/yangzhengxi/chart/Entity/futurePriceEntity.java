package com.yangzhengxi.chart.Entity;

import com.yangzhengxi.mybatis.annotation.Columns;

public class futurePriceEntity {
    @Columns
    Integer futureSprIceId;
    @Columns
    String futureSprIceDate;
    @Columns
    String futureSprIceMonth;
    @Columns
    String futureSprIceOprIce;
    @Columns
    String futureSprIceCprIce;
    @Columns
    String futureSprIceSettlement;
    @Columns
    String  futureSprIceCjl;
    @Columns
    String futureSprIceKpl;
    @Columns
    String futureSprIceHb;

    @Override
    public String toString() {
        return "futurePriceEntity{" +
                "futureSprIceId=" + futureSprIceId +
                ", futureSprIceDate='" + futureSprIceDate + '\'' +
                ", futureSprIceMonth='" + futureSprIceMonth + '\'' +
                ", futureSprIceOprIce='" + futureSprIceOprIce + '\'' +
                ", futureSprIceCprIce='" + futureSprIceCprIce + '\'' +
                ", futureSprIceSettlement='" + futureSprIceSettlement + '\'' +
                ", futureSprIceCjl='" + futureSprIceCjl + '\'' +
                ", futureSprIceKpl='" + futureSprIceKpl + '\'' +
                ", futureSprIceHb='" + futureSprIceHb + '\'' +
                '}';
    }

    public Integer getFutureSprIceId() {
        return futureSprIceId;
    }

    public void setFutureSprIceId(Integer futureSprIceId) {
        this.futureSprIceId = futureSprIceId;
    }

    public String getFutureSprIceDate() {
        return futureSprIceDate;
    }

    public void setFutureSprIceDate(String futureSprIceDate) {
        this.futureSprIceDate = futureSprIceDate;
    }

    public String getFutureSprIceMonth() {
        return futureSprIceMonth;
    }

    public void setFutureSprIceMonth(String futureSprIceMonth) {
        this.futureSprIceMonth = futureSprIceMonth;
    }

    public String getFutureSprIceOprIce() {
        return futureSprIceOprIce;
    }

    public void setFutureSprIceOprIce(String futureSprIceOprIce) {
        this.futureSprIceOprIce = futureSprIceOprIce;
    }

    public String getFutureSprIceCprIce() {
        return futureSprIceCprIce;
    }

    public void setFutureSprIceCprIce(String futureSprIceCprIce) {
        this.futureSprIceCprIce = futureSprIceCprIce;
    }

    public String getFutureSprIceSettlement() {
        return futureSprIceSettlement;
    }

    public void setFutureSprIceSettlement(String futureSprIceSettlement) {
        this.futureSprIceSettlement = futureSprIceSettlement;
    }

    public String getFutureSprIceCjl() {
        return futureSprIceCjl;
    }

    public void setFutureSprIceCjl(String futureSprIceCjl) {
        this.futureSprIceCjl = futureSprIceCjl;
    }

    public String getFutureSprIceKpl() {
        return futureSprIceKpl;
    }

    public void setFutureSprIceKpl(String futureSprIceKpl) {
        this.futureSprIceKpl = futureSprIceKpl;
    }

    public String getFutureSprIceHb() {
        return futureSprIceHb;
    }

    public void setFutureSprIceHb(String futureSprIceHb) {
        this.futureSprIceHb = futureSprIceHb;
    }
}
