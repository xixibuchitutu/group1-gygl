package com.yangzhengxi.chart.controller;

import com.yangzhengxi.chart.Entity.coalHarborEntity;
import com.yangzhengxi.chart.service.coalHarborService;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/hrbor")
public class coalHarborController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private coalHarborService coalHarborService;

    @RequestMapping("/page")
    public String harbor(){
        return "Harbor/Harbor";
    }

    @RequestMapping("/queryhrboriInfo")
    @ResponseBody
    public List<coalHarborEntity> queryhrborInfo(){
        List<coalHarborEntity> coalHarborEntity=coalHarborService.queryhrboriInfo();
        return coalHarborEntity;
    }

    @RequestMapping("/queryhrborInfoListByPage")
    @ResponseBody
    public PadingRstType<coalHarborEntity> queryhrborInfoListByPage(coalHarborEntity coalHarborEntity, PaddingEntity padding){

        PadingRstType<coalHarborEntity> rstType = coalHarborService.queryhrborInfoListByPage(coalHarborEntity,padding);
        return rstType;
    }

    @RequestMapping("/queryhrboriListByDate")
    @ResponseBody
    public   List<coalHarborEntity> queryhrboriListByDate(@RequestParam("start") String start, @RequestParam("end") String end){
        List<coalHarborEntity> listbspi=coalHarborService.queryhrboriListByDate(start,end);
        return listbspi;
    }

}
