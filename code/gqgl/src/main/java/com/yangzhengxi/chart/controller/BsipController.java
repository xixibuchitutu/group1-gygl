package com.yangzhengxi.chart.controller;

import com.yangzhengxi.chart.Entity.BsPiEntity;
import com.yangzhengxi.chart.service.bsPiService;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/bspiinfo")
public class BsipController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private bsPiService bspiService;

    @RequestMapping("/page")
    public String bspi(){
        return "bspi/bspi1";
}
 @RequestMapping("/bspiinfo/queryBsPiInfo")
 @ResponseBody
public Map<String, List<BsPiEntity>> queryBsPiInfo(){
     List<BsPiEntity> listbspi=bspiService.queryBsPiInfo();
     List<String> bsPiBsPi=new ArrayList<>();
     List<String> bsPiTb=new ArrayList<>();
     List<String> bsPiHbZj=new ArrayList<>();
     List<String> bsPiHb=new ArrayList<>();
     List<String> bsPtBzj=new ArrayList<>();
     List<String> bsPiDate=new ArrayList<>();
     for(BsPiEntity data:listbspi){
         bsPiBsPi.add(data.getBsPiBsPi());
         bsPiHbZj.add(data.getBsPiBsPi());
         bsPiHb.add(data.getBsPiBsPi());
         bsPtBzj.add(data.getBsPiBsPi());
         bsPiTb.add(data.getBsPiBsPi());
         bsPiDate.add(data.getBsPiDate());
     }
Map map=new HashMap();
map.put("bsPiBsPi",bsPiBsPi);
     map.put("bsPiTb",bsPiTb);
     map.put("bsPiHbZj",bsPiHbZj);
     map.put("bsPiHb",bsPiHb);
     map.put("bsPtBzj",bsPtBzj);
     map.put("bsPiDate",bsPiDate);
     return map;
}
    @RequestMapping("/bspiinfo/queryBsPiInfo1")
    @ResponseBody
    public   List<BsPiEntity> queryBsPiInfo1(){
        List<BsPiEntity> listbspi=bspiService.queryBsPiInfo();

        return listbspi;
    }

    @RequestMapping("/queryBsPiListByDate")
    @ResponseBody
    public   List<BsPiEntity> queryBsPiListByDate(@RequestParam("start") String start, @RequestParam("end") String end){
        List<BsPiEntity> listbspi=bspiService.queryBsPiListByDate(start,end);
        return listbspi;
    }

    @RequestMapping("/queryBsPiInfoListByPage")
    @ResponseBody
    public PadingRstType<BsPiEntity> queryBsPiInfoListByPage(BsPiEntity bsPiEntity, PaddingEntity padding){

        PadingRstType<BsPiEntity> rstType = bspiService.queryBsPiInfoListByPage(bsPiEntity,padding);
        return rstType;
    }
}