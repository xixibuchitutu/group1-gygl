package com.yangzhengxi.chart.service.impl;

import com.yangzhengxi.chart.Entity.BsPiEntity;
import com.yangzhengxi.chart.service.bsPiService;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class bsPiSeriviceImpl implements bsPiService {
    @Resource
    private com.yangzhengxi.chart.repostiory.bsPiDao bsPiDao;
    @Override
    public List<BsPiEntity> queryBsPiInfo() {
        return bsPiDao.queryBsPiInfo();
    }

    @Override
    public List<BsPiEntity> queryBsPiListByDate(String start, String end) {
        return bsPiDao.queryBsPiListByDate(start,end);
    }

    @Override
    public PadingRstType<BsPiEntity> queryBsPiInfoListByPage(BsPiEntity BsPiEntity, PaddingEntity padding) {

            padding.deal(com.yangzhengxi.chart.Entity.BsPiEntity.class);
            PadingRstType<com.yangzhengxi.chart.Entity.BsPiEntity> rstType = new PadingRstType<>();
            rstType.setPage(padding.getPage());
            List<com.yangzhengxi.chart.Entity.BsPiEntity> list = bsPiDao.queryBsPiInfoListByPage(BsPiEntity,padding);
            rstType.setRawRecords(list);
            Integer total =  bsPiDao.queryBspInfoTotal(BsPiEntity);;
            rstType.setTotal(total);
            rstType.putItems();

            return rstType;

    }


}
