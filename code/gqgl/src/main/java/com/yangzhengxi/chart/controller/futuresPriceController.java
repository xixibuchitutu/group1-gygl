package com.yangzhengxi.chart.controller;

import com.yangzhengxi.chart.Entity.futurePriceEntity;
import com.yangzhengxi.chart.service.futuresPriceService;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/fpinfo")
public class futuresPriceController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private com.yangzhengxi.chart.service.futuresPriceService futuresPriceService;

    @RequestMapping("/page")
    public String futuresPrice(){
        return "futuresPrice/futuresPrice";
    }

    @RequestMapping("/fpinfo/queryfpInfo")
    @ResponseBody
    public List<futurePriceEntity> queryBsPiInfo1(){
        List<futurePriceEntity> futurePriceEntity=futuresPriceService.queryfutureiInfo();
        return futurePriceEntity;
    }

    @RequestMapping("/queryfutureInfoListByPage")
    @ResponseBody
    public PadingRstType<futurePriceEntity> queryfutureInfoListByPage(futurePriceEntity bsPiEntity, PaddingEntity padding){

        PadingRstType<futurePriceEntity> rstType = futuresPriceService.queryfutureInfoListByPage(bsPiEntity,padding);
        return rstType;
    }

    @RequestMapping("/queryfutureiListByDate")
    @ResponseBody
    public   List<futurePriceEntity> queryfutureiListByDate(@RequestParam("start") String start, @RequestParam("end") String end){
        List<futurePriceEntity> listbspi=futuresPriceService.queryfutureiListByDate(start,end);
        return listbspi;
    }
}
