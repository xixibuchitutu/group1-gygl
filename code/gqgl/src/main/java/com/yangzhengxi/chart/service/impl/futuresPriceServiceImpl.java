package com.yangzhengxi.chart.service.impl;

import com.yangzhengxi.chart.Entity.futurePriceEntity;
import com.yangzhengxi.chart.service.futuresPriceService;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class futuresPriceServiceImpl implements futuresPriceService {
    @Resource
    private com.yangzhengxi.chart.repostiory.futurePriceDao futurePriceDao;
    @Override
    public List<futurePriceEntity> queryfutureiInfo() {
        return futurePriceDao.queryfutureiInfo();
    }

    @Override
    public PadingRstType<futurePriceEntity> queryfutureInfoListByPage(futurePriceEntity bsPiEntity, PaddingEntity padding) {
        padding.deal(futurePriceEntity.class);
        PadingRstType<futurePriceEntity> rstType = new PadingRstType<>();
        rstType.setPage(padding.getPage());
        List<futurePriceEntity> list = futurePriceDao.queryfutureInfoListByPage(bsPiEntity,padding);
        rstType.setRawRecords(list);
        Integer total =  futurePriceDao.queryfutureInfoListByTotal(bsPiEntity);;
        rstType.setTotal(total);
        rstType.putItems();

        return rstType;


    }

    @Override
    public List<futurePriceEntity> queryfutureiListByDate(String start, String end) {
        return futurePriceDao.queryfutureiListByDate(start,end);

    }
}
