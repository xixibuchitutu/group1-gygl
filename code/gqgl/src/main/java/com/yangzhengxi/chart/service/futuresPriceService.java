package com.yangzhengxi.chart.service;

import com.yangzhengxi.chart.Entity.futurePriceEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;

import java.util.List;


public interface futuresPriceService {
    List<futurePriceEntity> queryfutureiInfo();

    PadingRstType<futurePriceEntity> queryfutureInfoListByPage(futurePriceEntity futurePriceEntity, PaddingEntity padding);

    List<futurePriceEntity> queryfutureiListByDate(String start, String end);
}
