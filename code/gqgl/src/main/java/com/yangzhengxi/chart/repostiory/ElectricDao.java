package com.yangzhengxi.chart.repostiory;

import com.yangzhengxi.chart.Entity.ElectricEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ElectricDao {

    List<ElectricEntity> querycariInfo();

    List<ElectricEntity> querycarInfoListByPage(@Param("entity") ElectricEntity capacityElectricEntity, PaddingEntity padding);

    Integer querycarrInfoListByTotal(@Param("entity") ElectricEntity capacityElectricEntity);

    List<ElectricEntity> querycariListByDate(@Param("start") String start, @Param("end") String end);
}
