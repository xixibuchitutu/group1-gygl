package com.yangzhengxi.chart.service;

import com.yangzhengxi.chart.Entity.BsPiEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;

import java.util.List;

public interface bsPiService {
    

    List<BsPiEntity> queryBsPiInfo();

    List<BsPiEntity> queryBsPiListByDate(String start, String end);


    PadingRstType<BsPiEntity> queryBsPiInfoListByPage(BsPiEntity BsPiEntity, PaddingEntity padding);
}
