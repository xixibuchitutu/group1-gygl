package com.yangzhengxi.chart.Entity;

import com.yangzhengxi.mybatis.annotation.Columns;

public class coalHarborEntity {
    @Columns
    Integer harborId;
    @Columns
    String harborDate;
    @Columns
    String harborK5800;
    @Columns
    String harborK5500;
    @Columns
    String  harborK5000;
    @Columns
    String harborK4500;

    @Override
    public String toString() {
        return "coalHarborEntity{" +
                "harborId=" + harborId +
                ", harborDate='" + harborDate + '\'' +
                ", harborK5800='" + harborK5800 + '\'' +
                ", harborK5500='" + harborK5500 + '\'' +
                ", harborK5000='" + harborK5000 + '\'' +
                ", harborK4500='" + harborK4500 + '\'' +
                '}';
    }

    public Integer getHarborId() {
        return harborId;
    }

    public void setHarborId(Integer harborId) {
        this.harborId = harborId;
    }

    public String getHarborDate() {
        return harborDate;
    }

    public void setHarborDate(String harborDate) {
        this.harborDate = harborDate;
    }

    public String getHarborK5800() {
        return harborK5800;
    }

    public void setHarborK5800(String harborK5800) {
        this.harborK5800 = harborK5800;
    }

    public String getHarborK5500() {
        return harborK5500;
    }

    public void setHarborK5500(String harborK5500) {
        this.harborK5500 = harborK5500;
    }

    public String getHarborK5000() {
        return harborK5000;
    }

    public void setHarborK5000(String harborK5000) {
        this.harborK5000 = harborK5000;
    }

    public String getHarborK4500() {
        return harborK4500;
    }

    public void setHarborK4500(String harborK4500) {
        this.harborK4500 = harborK4500;
    }
}
