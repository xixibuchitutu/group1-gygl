package com.yangzhengxi.chart.repostiory;

import com.yangzhengxi.chart.Entity.futurePriceEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface futurePriceDao {
    List<futurePriceEntity> queryfutureiInfo();
    List<futurePriceEntity> queryfutureInfoListByPage(@Param("entity") futurePriceEntity futurePriceEntity, PaddingEntity padding);
    Integer queryfutureInfoListByTotal(@Param("entity") futurePriceEntity futurePriceEntity);;
    List<futurePriceEntity> queryfutureiListByDate(@Param("start") String start, @Param("end") String end);

}
