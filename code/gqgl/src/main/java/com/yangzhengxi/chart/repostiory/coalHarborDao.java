package com.yangzhengxi.chart.repostiory;

import com.yangzhengxi.chart.Entity.coalHarborEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface coalHarborDao {
    List<coalHarborEntity> queryhrboriInfo();

    List<coalHarborEntity> querycoalharborInfoListByPage(@Param("entity") coalHarborEntity coalHarborEntity, PaddingEntity padding);

    Integer querycoalharborInfoListByTotal(@Param("entity") coalHarborEntity coalHarborEntity);

    List<coalHarborEntity> queryhrboriListByDate(@Param("start") String start, @Param("end") String end);
}
