package com.yangzhengxi.chart.repostiory;

import com.yangzhengxi.chart.Entity.BsPiEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface bsPiDao {
 List<BsPiEntity> queryBsPiInfo();

    List<BsPiEntity> queryBsPiListByDate(@Param("start") String start, @Param("end") String end);

    List<BsPiEntity> queryBsPiInfoListByPage(@Param("entity") BsPiEntity BsPiEntity, PaddingEntity PaddingEntity);

    Integer queryBspInfoTotal(@Param("BsPiEntity") BsPiEntity BsPiEntity);
}
