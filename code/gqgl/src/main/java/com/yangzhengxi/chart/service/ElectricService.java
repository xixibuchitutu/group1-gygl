package com.yangzhengxi.chart.service;

import com.yangzhengxi.chart.Entity.ElectricEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;

import java.util.List;

public interface ElectricService {


    List<ElectricEntity> querycariInfo();

    PadingRstType<ElectricEntity> querycarInfoListByPage(ElectricEntity capacityElectricEntity, PaddingEntity padding);

    List<ElectricEntity> querycariListByDate(String start, String end);
}
