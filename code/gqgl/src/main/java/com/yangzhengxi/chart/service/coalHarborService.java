package com.yangzhengxi.chart.service;

import com.yangzhengxi.chart.Entity.coalHarborEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;

import java.util.List;

public interface coalHarborService {
    List<coalHarborEntity> queryhrboriInfo();

    PadingRstType<coalHarborEntity> queryhrborInfoListByPage(coalHarborEntity coalHarborEntity, PaddingEntity padding);

    List<coalHarborEntity> queryhrboriListByDate(String start, String end);
}
