package com.yangzhengxi.chart.service.impl;

import com.yangzhengxi.chart.Entity.coalHarborEntity;
import com.yangzhengxi.chart.service.coalHarborService;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class coalHarborServiceimpl implements coalHarborService
{    @Resource
com.yangzhengxi.chart.repostiory.coalHarborDao coalHarborDao;
    @Override
    public List<coalHarborEntity> queryhrboriInfo() {
        return coalHarborDao.queryhrboriInfo();
    }

    @Override
    public PadingRstType<coalHarborEntity> queryhrborInfoListByPage(coalHarborEntity coalHarborEntity, PaddingEntity padding) {
        padding.deal(com.yangzhengxi.chart.Entity.coalHarborEntity.class);
        PadingRstType<com.yangzhengxi.chart.Entity.coalHarborEntity> rstType = new PadingRstType<>();
        rstType.setPage(padding.getPage());
        List<com.yangzhengxi.chart.Entity.coalHarborEntity> list = coalHarborDao.querycoalharborInfoListByPage(coalHarborEntity,padding);
        rstType.setRawRecords(list);
        Integer total =  coalHarborDao.querycoalharborInfoListByTotal(coalHarborEntity);;
        rstType.setTotal(total);
        rstType.putItems();

        return rstType;
    }

    @Override
    public List<coalHarborEntity> queryhrboriListByDate(String start, String end) {
        return coalHarborDao.queryhrboriListByDate(start,end);
    }
}
