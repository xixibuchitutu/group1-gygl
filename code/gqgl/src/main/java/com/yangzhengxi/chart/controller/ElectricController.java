package com.yangzhengxi.chart.controller;

import com.yangzhengxi.chart.Entity.ElectricEntity;
import com.yangzhengxi.chart.service.ElectricService;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/electricityinfo")
public class ElectricController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private ElectricService capacityElectricService;

    @RequestMapping("/page")
    public String capacityElectricService(){
        return "capaCity/capaCity";
    }

    @RequestMapping("/querycariInfo")
    @ResponseBody
    public List<ElectricEntity> querycariInfo(){
        List<ElectricEntity> capacityElectricEntity=capacityElectricService.querycariInfo();
        return capacityElectricEntity;
    }

    @RequestMapping("/querycarInfoListByPage")
    @ResponseBody
    public PadingRstType<ElectricEntity> querycarInfoListByPage(ElectricEntity capacityElectricEntity, PaddingEntity padding){

        PadingRstType<ElectricEntity> rstType = capacityElectricService.querycarInfoListByPage(capacityElectricEntity,padding);
        return rstType;
    }

    @RequestMapping("/querycariListByDate")
    @ResponseBody
    public   List<ElectricEntity> querycariListByDate(@RequestParam("start") String start, @RequestParam("end") String end){
        List<ElectricEntity> listbspi=capacityElectricService.querycariListByDate(start,end);
        return listbspi;
    }

}
