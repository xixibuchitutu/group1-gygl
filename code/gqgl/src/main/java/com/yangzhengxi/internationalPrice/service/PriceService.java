package com.yangzhengxi.internationalPrice.service;

import com.yangzhengxi.internationalPrice.entity.SearchPriceEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;

import java.util.List;

public interface PriceService {
    List<SearchPriceEntity> queryPriceInfo();

    PadingRstType<SearchPriceEntity> queryPriceInfoListByPage(SearchPriceEntity SearchPriceEntity, PaddingEntity padding);

    List<SearchPriceEntity> queryPriceListByDate(String start, String end);
}
