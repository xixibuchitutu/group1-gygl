package com.yangzhengxi.internationalPrice.service.impl;

import com.yangzhengxi.internationalPrice.entity.SearchPriceEntity;
import com.yangzhengxi.internationalPrice.service.PriceService;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service()
public class PriceServiceImpl implements PriceService {
    @Resource
    com.yangzhengxi.internationalPrice.repository.PriceDao PriceDao;
    @Override
    public List<SearchPriceEntity> queryPriceInfo() {
        return PriceDao.queryPriceInfo();
    }

    @Override
    public PadingRstType<SearchPriceEntity> queryPriceInfoListByPage(SearchPriceEntity SearchPriceEntity, PaddingEntity padding) {
        padding.deal(com.yangzhengxi.internationalPrice.entity.SearchPriceEntity.class);
        PadingRstType<com.yangzhengxi.internationalPrice.entity.SearchPriceEntity> rstType = new PadingRstType<>();
        rstType.setPage(padding.getPage());
        List<com.yangzhengxi.internationalPrice.entity.SearchPriceEntity> list = PriceDao.queryPriceInfoListByPage(SearchPriceEntity,padding);
        rstType.setRawRecords(list);
        Integer total =  PriceDao.queryPriceInfoListByTotal(SearchPriceEntity);;
        rstType.setTotal(total);
        rstType.putItems();

        return rstType;
    }

    @Override
    public List<SearchPriceEntity> queryPriceListByDate(String start, String end) {
        return PriceDao.queryPriceListByDate(start,end);
    }
}
