package com.yangzhengxi.internationalPrice.controller;

import com.yangzhengxi.internationalPrice.entity.SearchPriceEntity;
import com.yangzhengxi.internationalPrice.service.PriceService;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/priceinfo")
public class PriceController {
    @Autowired
    PriceService Priceservice;
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private PriceService PriceService;

    @RequestMapping("/page")
    public String harbor(){
        return "priceinfo/page";
    }

    @RequestMapping("/queryPriceInfo")
    @ResponseBody
    public List<SearchPriceEntity> queryPriceInfo(){
        List<SearchPriceEntity> SearchPriceEntity=PriceService.queryPriceInfo();
        return SearchPriceEntity;
    }

    @RequestMapping("/queryPriceInfoListByPage")
    @ResponseBody
    public PadingRstType<SearchPriceEntity> queryPriceInfoListByPage(SearchPriceEntity SearchPriceEntity, PaddingEntity padding){

        PadingRstType<SearchPriceEntity> rstType = PriceService.queryPriceInfoListByPage(SearchPriceEntity,padding);
        return rstType;
    }

    @RequestMapping("/queryPriceListByDate")
    @ResponseBody
    public   List<SearchPriceEntity> queryPriceListByDate(@RequestParam("start") String start, @RequestParam("end") String end){
        List<SearchPriceEntity> listbspi=PriceService.queryPriceListByDate(start,end);
        return listbspi;
    }

}
