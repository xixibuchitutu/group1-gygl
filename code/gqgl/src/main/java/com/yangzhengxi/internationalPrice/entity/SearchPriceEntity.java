package com.yangzhengxi.internationalPrice.entity;

import com.yangzhengxi.mybatis.annotation.Columns;

public class SearchPriceEntity{
/*    private String beginDate;

    private String endDate;

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }*/


    @Columns("price_id")
    private  String priceId;
    @Columns("price_date")
    private  String priceDate;
    @Columns("australia5500k")
    private  String australia5500k;
    @Columns("australia5500k_yoy")
    private  String australia5500kYoy;
    @Columns("australia4700k")
    private  String indonesia4700k;
    @Columns("australia5500k_yoy")
    private  String indonesia4700kYoy;
    @Columns("australia3800k")
    private  String indonesia3800k;
    @Columns("australia5500k_yoy")
    private  String indonesia3800kYoy;

    public String getPriceId() {
        return priceId;
    }
    public void setPriceId(String priceId) {
        this.priceId = priceId;
    }

    public String getPriceDate() {
        return priceDate;
    }
    public void setPriceDate(String priceDate) {
        this.priceDate = priceDate;
    }

    public String getAustralia5500k() {
        return australia5500k;
    }
    public void setAustralia5500k(String australia5500k) {
        this.australia5500k = australia5500k;
    }

    public String getAustralia5500kYoy() {
        return australia5500kYoy;
    }
    public void setAustralia5500kYoy(String australia5500kYoy) {
        this.australia5500kYoy = australia5500kYoy;
    }

    public String getIndonesia4700k() {
        return indonesia4700k;
    }
    public void setIndonesia4700k(String indonesia4700k) {
        this.indonesia4700k = indonesia4700k;
    }

    public String getIndonesia4700kYoy() {
        return indonesia4700kYoy;
    }
    public void setIndonesia4700kYoy(String indonesia4700kYoy) {
        this.indonesia4700kYoy = indonesia4700kYoy;
    }

    public String getIndonesia3800k() {
        return indonesia3800k;
    }
    public void setIndonesia3800k(String indonesia3800k) {
        this.indonesia3800k = indonesia3800k;
    }

    public String getIndonesia3800kYoy() {
        return indonesia3800kYoy;
    }
    public void setIndonesia3800kYoy(String indonesia3800kYoy) {
        this.indonesia3800kYoy = indonesia3800kYoy;
    }

    @Override
    public String toString() {
        return "SearchPriceEntity{" +
                "priceId='" + priceId + '\'' +
                ", priceDate='" + priceDate + '\'' +
                ", australia5500k='" + australia5500k + '\'' +
                ", australia5500kYoy='" + australia5500kYoy + '\'' +
                ", indonesia4700k='" + indonesia4700k + '\'' +
                ", indonesia4700kYoy='" + indonesia4700kYoy + '\'' +
                ", indonesia3800k='" + indonesia3800k + '\'' +
                ", indonesia3800kYoy='" + indonesia3800kYoy + '\'' +
                '}';
    }
}

