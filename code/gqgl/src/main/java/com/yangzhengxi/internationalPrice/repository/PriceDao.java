package com.yangzhengxi.internationalPrice.repository;

import com.yangzhengxi.internationalPrice.entity.SearchPriceEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PriceDao {
    List<SearchPriceEntity> queryPriceInfo();

    List<SearchPriceEntity> queryPriceInfoListByPage(@Param("entity") SearchPriceEntity SearchPriceEntity, PaddingEntity padding);

    Integer queryPriceInfoListByTotal(@Param("entity") SearchPriceEntity SearchPriceEntity);

    List<SearchPriceEntity> queryPriceListByDate(@Param("start") String start, @Param("end") String end);
}
