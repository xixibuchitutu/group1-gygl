package com.yangzhengxi.role.service.Impl;


import com.yangzhengxi.mybatis.entity.PtRoleEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.role.entity.SearchRoleEntity;
import com.yangzhengxi.role.respository.RoleDao;
import com.yangzhengxi.role.service.RoleService;
import com.yangzhengxi.spring.util.DataUitl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    @Resource
    private RoleDao roleDao;
    @Override
    public PadingRstType<PtRoleEntity> queryRoleInfoListByPage(SearchRoleEntity search, PaddingEntity padding) {
        padding.deal(PtRoleEntity.class);
        PadingRstType<PtRoleEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(padding.getPage());
        List<PtRoleEntity> list = roleDao.queryRoleInfoListByPage(search,padding);
        padingRstType.setRawRecords(list);
        Integer total = roleDao.queryRoleInfoTotal(search);
        padingRstType.setTotal(total);
        padingRstType.putItems();

        return padingRstType;
    }

    @Override
    public List<ZtreeEntity> queryMenuTree(String roleUuid) {
        List<ZtreeEntity> list = roleDao.queryMenuTree();
        List<String> menuIds = roleDao.queryMenuByRoleId(roleUuid);
        for(ZtreeEntity ztreeEntity: list){
            if(menuIds.contains(ztreeEntity.getId())){
                ztreeEntity.setChecked(true);
            }
        }
        return list;
    }

    @Override
    public void saveRoleRefMenu(String roleUuid, String menuIds) {
        roleDao.deleteMenuByRoleId(roleUuid);
        String[] menuIdArray = menuIds.split(",");
        for (String menuId: menuIdArray) {
            roleDao.insertMenuRefRole(roleUuid,menuId);
        }

    }
    @Override
    public void addRoleInfo(PtRoleEntity ptRoleEntity) {
        ptRoleEntity.setRoleUuid(DataUitl.getUuid());
        roleDao.insertRoleInfo(ptRoleEntity);
    }
    @Override
    public void modifyRoleInfo(PtRoleEntity ptRoleEntity) {
        roleDao.modifyRoleInfo(ptRoleEntity);
    }
    @Override
    public void deleteRoleById(String roleUuidArray) {
        String[] roleUuids = roleUuidArray.split(",");
        roleDao.deleteRoleById(roleUuids);
    }
}
