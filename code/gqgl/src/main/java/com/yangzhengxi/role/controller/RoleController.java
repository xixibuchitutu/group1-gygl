package com.yangzhengxi.role.controller;


import com.yangzhengxi.spring.controller.BaseController;
import com.yangzhengxi.mybatis.entity.PtRoleEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.role.entity.SearchRoleEntity;
import com.yangzhengxi.role.service.RoleService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/role")
public class RoleController extends BaseController {

    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private RoleService roleService;
    @Secured("ROLE_gqgy_data_role")
    @RequestMapping("/rolePage")
    public String rolePage(){
        return "role/rolePage";
    }

    @Secured("ROLE_gqgy_data_role_query")
    @RequestMapping("/rolePageDetail")
    public String rolePageDetail(@RequestParam("roleUuid") String roleUuid,HttpServletRequest request){
        logger.info("roleUuid:" + roleUuid);
        request.getSession().setAttribute("roleUuid",roleUuid );
        return "role/rolePageDetail";
    }

    @Secured("ROLE_gqgy_data_role_query")
    @RequestMapping("/queryRoleInfoListByPage")
    @ResponseBody
    public PadingRstType<PtRoleEntity> queryRoleInfoListByPage(SearchRoleEntity search, PaddingEntity padding){
        logger.info(search);
        logger.info(padding);
        PadingRstType<PtRoleEntity> padingRstType = roleService.queryRoleInfoListByPage(search,padding);

        return padingRstType;
    }
    //ROLE_gqgy_data_role_allow
    @Secured("ROLE_gqgy_data_role_allow")
    @RequestMapping("/queryMenuTree")
    @ResponseBody
    public List<ZtreeEntity> queryMenuTree(@RequestParam("roleUuid") String roleUuid){
        logger.info("roleUuid:" + roleUuid);
        List<ZtreeEntity>  list= roleService.queryMenuTree(roleUuid);
        return list;
    }
    @Secured("ROLE_gqgy_data_role_allow")
    @RequestMapping("/saveRoleRefMenu")
    @ResponseBody
    public String saveRoleRefMenu(@RequestParam("") String roleUuid,@RequestParam("menuIds") String menuIds){
        logger.info("roleUuid:"+roleUuid + " menuIds:" + menuIds);
        roleService.saveRoleRefMenu(roleUuid,menuIds);
        return jsonSuccess("role.menu.allocate.success");
    }
    @Secured("ROLE_gqgy_data_role_add")
    @RequestMapping("/addRoleInfo")
    @ResponseBody
    public String addRoleInfo(PtRoleEntity ptRoleEntity){
        logger.info(ptRoleEntity);
        roleService.addRoleInfo(ptRoleEntity);
        return jsonSuccess("role.add.success");
    }

    @Secured("ROLE_gqgy_data_role_update")
    @RequestMapping("/modifyRoleInfo")
    @ResponseBody
    public String modifyRoleInfo(PtRoleEntity ptRoleEntity){
        logger.info(ptRoleEntity);
        roleService.modifyRoleInfo(ptRoleEntity);
        return jsonSuccess("role.modify.success");
    }

    @Secured("ROLE_gqgy_data_role_delete")
    @RequestMapping("/deleteRoleById")
    @ResponseBody
    public String deleteRoleById(@RequestParam("roleUuidArray") String roleUuidArray){
        logger.info("roleUuidArray:" + roleUuidArray);
        roleService.deleteRoleById(roleUuidArray);
        return jsonSuccess("role.delete.success");
    }




}
