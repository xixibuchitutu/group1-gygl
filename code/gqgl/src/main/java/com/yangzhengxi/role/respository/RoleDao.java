package com.yangzhengxi.role.respository;

import com.yangzhengxi.mybatis.entity.PtRoleEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.role.entity.SearchRoleEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface RoleDao {
    List<PtRoleEntity> queryRoleInfoListByPage(@Param("search") SearchRoleEntity search, @Param("padding") PaddingEntity padding);

    Integer queryRoleInfoTotal(@Param( "search")SearchRoleEntity search);

    List<ZtreeEntity> queryMenuTree();

    void deleteMenuByRoleId(@Param("roleUuid") String roleUuid);

    void insertMenuRefRole(@Param("roleUuid")String roleUuid,@Param("menuId") String menuId);

    List<String> queryMenuByRoleId(@Param("roleUuid") String roleUuid);
    void insertRoleInfo(@Param("entity")PtRoleEntity ptRoleEntity);
    void modifyRoleInfo(@Param("entity")PtRoleEntity ptRoleEntity);
    void deleteRoleById(@Param("roleUuids") String[] roleUuids);
}
