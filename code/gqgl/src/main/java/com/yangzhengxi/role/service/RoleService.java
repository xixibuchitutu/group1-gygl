package com.yangzhengxi.role.service;


import com.yangzhengxi.mybatis.entity.PtRoleEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.role.entity.SearchRoleEntity;

import java.util.List;

public interface RoleService {
    PadingRstType<PtRoleEntity> queryRoleInfoListByPage(SearchRoleEntity search, PaddingEntity padding);
    List<ZtreeEntity> queryMenuTree(String roleUuid);
    void saveRoleRefMenu(String roleUuid, String menuIds);
    void addRoleInfo(PtRoleEntity ptRoleEntity);
    void modifyRoleInfo(PtRoleEntity ptRoleEntity);
    void deleteRoleById(String roleUuidArray);
}