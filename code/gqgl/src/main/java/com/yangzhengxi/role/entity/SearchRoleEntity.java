package com.yangzhengxi.role.entity;

import java.io.Serializable;

public class SearchRoleEntity implements Serializable {
    private String roleName;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Override
    public String toString() {
        return "SearchRoleEntity{" +
                "roleName='" + roleName + '\'' +
                '}';
    }
}
