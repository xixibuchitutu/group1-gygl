package com.yangzhengxi.login.entity;


import com.yangzhengxi.mybatis.entity.PtMenuEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MenuLevelEntity  implements Serializable {
    private String menuId;
    private String menuName;
    private Integer menuLevel;
    private String menuUrl;
    private String privilegeCode;
    private String menuIcon;
    private List<MenuLevelEntity> children = null;
    public MenuLevelEntity(){

    }
    public MenuLevelEntity(PtMenuEntity ptMenuEntity){
        this.menuId = ptMenuEntity.getMenuId();
        this.menuName = ptMenuEntity.getMenuName();
        this.menuLevel = ptMenuEntity.getMenuLevel();
        this.menuUrl = ptMenuEntity.getMenuUrl();
        this.privilegeCode = ptMenuEntity.getPrivilegeCode();
        this.menuIcon = ptMenuEntity.getMenuIcon();

    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public Integer getMenuLevel() {
        return menuLevel;
    }

    public void setMenuLevel(Integer menuLevel) {
        this.menuLevel = menuLevel;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getPrivilegeCode() {
        return privilegeCode;
    }

    public void setPrivilegeCode(String privilegeCode) {
        this.privilegeCode = privilegeCode;
    }

    public String getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

    public List<MenuLevelEntity> getChildren() {
        return children;
    }

    public void setChildren(List<MenuLevelEntity> children) {
        this.children = children;
    }
    public void addChildren(MenuLevelEntity menuLevelEntity) {
        if(this.children == null){
            this.children = new ArrayList<>();
        }
        this.children.add(menuLevelEntity);
    }

    @Override
    public String toString() {
        return "MenuLevelEntity{" +
                "menuId='" + menuId + '\'' +
                ", menuName='" + menuName + '\'' +
                ", menuLevel=" + menuLevel +
                ", menuUrl='" + menuUrl + '\'' +
                ", privilegeCode='" + privilegeCode + '\'' +
                ", menuIcon='" + menuIcon + '\'' +
                ", children=" + children +
                '}';
    }

    public Object getUserId() {
        return menuId;
    }

    public Object getUsername() {
        return menuName;
    }

    public Object getUserNameCN() {
        return menuName;
    }
}
