package com.yangzhengxi.login.control;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/test")
public class TestController {
    @Secured("ROLE_TEST1")
    @RequestMapping("/test1")
    public String test1(){
        return "test/test1";
    }
    @Secured("ROLE_TEST2")
    @RequestMapping("/test2")
    public String test2(){
        return "test/test2";
    }
}
