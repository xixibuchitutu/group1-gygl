package com.yangzhengxi.login.control;

import com.yangzhengxi.login.exception.MoreMailException;
import com.yangzhengxi.login.exception.NotMailException;
import com.yangzhengxi.login.service.AnonService;
import com.yangzhengxi.spring.controller.BaseController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/anon")
public class AnonController extends BaseController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private AnonService anonService;
    /**
     * 获取验证码
     * @return
     */
    @RequestMapping("/getLoginVfcCode/{fileName}")
    @ResponseBody
    public ResponseEntity<byte[]> getLoginVfcCode(HttpServletRequest request){

        ResponseEntity<byte[]> responseEntity = anonService.getLoginVfcCode(request);
        return  responseEntity;
    }

    @RequestMapping("/authVfcCode")
    @ResponseBody
    public String authVfcCode (HttpServletRequest request,@RequestParam("vfcCode") String vfcCode){
        logger.info("vfcCode:" + vfcCode);
        boolean rstB = anonService.authVfcCode(vfcCode,request);
        if(rstB){
            return jsonSuccess("login.auth.vfc.code.success");
        }
        return jsonFail("login.auth.vfc.code.fail");
        //login.auth.vfc.code.fail
    }


    /**
     * 获取验证码
     * @return
     */
    @RequestMapping("/getMailVfcCode")
    @ResponseBody
    public String getMailVfcCode(@RequestParam("email") String email,HttpServletRequest request){
        logger.info("email:" + email);
        String verificationCode = null;
        try {
            verificationCode = anonService.getMailVfcCode(email);
        } catch (NotMailException e) {
            e.printStackTrace();
            return jsonFail("mail.not.exit.fail");
        } catch (MoreMailException e) {
            e.printStackTrace();
            return jsonFail("mail.exit.more.fail");
        }
        if(verificationCode == null || "".equals(verificationCode)){
            return jsonFail("mail.verification.code.fail");
        }
        request.getSession().setAttribute("emailCode",verificationCode);
        return jsonSuccess("mail.verification.code.success");
    }
    @RequestMapping("/authEmailCode")
    @ResponseBody
    public String authEmailCode(@RequestParam("vfcCode") String vfcCode,HttpServletRequest request){
        logger.info("vfcCode:" + vfcCode);
        String emailCode = (String) request.getSession().getAttribute("emailCode");
        logger.info("emailCode:" + emailCode);

        if(!vfcCode.equals(emailCode)){
            return jsonFail("mail.auth.code.fail");
        }
        return jsonSuccess("mail.auth.code.success");

    }


}
