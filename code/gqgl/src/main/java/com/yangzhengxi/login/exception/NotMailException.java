package com.yangzhengxi.login.exception;

public class NotMailException extends Exception {
    public NotMailException(String message){
        super(message);
    }
}
