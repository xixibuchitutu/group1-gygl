package com.yangzhengxi.login.control;

import com.alibaba.fastjson.JSON;
import com.yangzhengxi.login.entity.MenuLevelEntity;
import com.yangzhengxi.mybatis.entity.PtMenuEntity;
import com.yangzhengxi.user.entity.SecurityUser;
import com.yangzhengxi.user.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class MainController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private UserService userService;

    @RequestMapping("/")
    public String main(HttpServletRequest request){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal instanceof SecurityUser){
            SecurityUser securityUser = (SecurityUser) principal;
            String userUuid = securityUser.getUserUuid();
            List<MenuLevelEntity> list = userService.queryMenuLevelList(userUuid);
            request.getSession().setAttribute("menu",list);
        }else{
            List<MenuLevelEntity> list = userService.queryMenuLevelList();
            request.getSession().setAttribute("menu",list);
        }

        logger.info("main");
        return "login/main";
    }

    @RequestMapping("/index")
    public String index(){
        return "login/index";
    }

    @RequestMapping("/temp")
    public String temp(){
        return "login/temp";
    }

    @RequestMapping("/queryMenuList")
    @ResponseBody
    public List<PtMenuEntity> queryMenuList(){
        List<PtMenuEntity> list = userService.queryMenuList();
        return  list;
    }


    @RequestMapping("/queryMenuLevelList")
    @ResponseBody
    public String queryMenuLevelList(){
        List<MenuLevelEntity> list = userService.queryMenuLevelList();
        return JSON.toJSONString(list);
    }
    @RequestMapping("/loadServiceMenu")
    @ResponseBody
    public List<MenuLevelEntity> loadServiceMenu(@RequestParam("menuId") String menuId,HttpServletRequest request){
        List<MenuLevelEntity> list = (List<MenuLevelEntity>) request.getSession().getAttribute("menu");
        for(MenuLevelEntity item: list){
            if(menuId.equals(item.getMenuId())){
                return item.getChildren();
            }
        }
        return null;
    }



}
