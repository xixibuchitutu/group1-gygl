package com.yangzhengxi.login.service.impl;


import com.yangzhengxi.login.exception.MoreMailException;
import com.yangzhengxi.login.exception.NotMailException;
import com.yangzhengxi.login.service.AnonService;
import com.yangzhengxi.mybatis.entity.PtUserEntity;
import com.yangzhengxi.spring.entity.MailEntity;
import com.yangzhengxi.spring.util.DataUitl;
import com.yangzhengxi.spring.util.MailUtil;
import com.yangzhengxi.user.repository.UserDao;
import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Locale;

@Service
public class AnonServiceImpl implements AnonService {
    @Resource
    private UserDao userDao;

    @Value("${mail.host.name}")
    private String mailHostName;
    @Value("${mail.from.user}")
    private String mailFromUser;
    @Value("${mail.from.pwd}")
    private String mailFromPwd;

    @Resource
    protected MessageSource messageSource;
    @Override
    public ResponseEntity<byte[]> getLoginVfcCode(HttpServletRequest request) {

        String vfcCode = DataUitl.getVfcCode(4);

        request.getSession().setAttribute("vfcCode",vfcCode);
        byte[] contents = DataUitl.getVfcImg(vfcCode);
        HttpHeaders headers =
                new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);//设置MIME类型

        return new ResponseEntity<byte[]>(
                contents,
                headers,
                HttpStatus.OK);
    }

    @Override
    public boolean authVfcCode(String vfcCode, HttpServletRequest request) {
        HttpSession session = request.getSession();
        String isAuthCode = (String) session.getAttribute("isAuthCode");
        if("false".equals(isAuthCode)){
            return true;
        }
        String vfcCodeSession = (String) session.getAttribute("vfcCode");
        if(vfcCode.equals(vfcCodeSession)){
            return true;
        }
        return false;
    }

    @Override
    public String getMailVfcCode(String email) throws NotMailException, MoreMailException {
        List<PtUserEntity> list = userDao.queryUserInfoByMail(email);
        if(list == null || list.size() == 0){
            throw  new NotMailException("not mail");
        }else if(list.size() > 1){
            throw new MoreMailException("more mail:" + list.size());
        }

        Locale locale = LocaleContextHolder.getLocale();
        String mailSubject =  messageSource.getMessage("mail.verification.subject",null,locale);
        String verificationCode = DataUitl.getVfcCode(6);
        String mailContent = messageSource.getMessage("mail.verification.content",new String[]{verificationCode},locale);
        MailEntity mailEntity = new MailEntity();
        mailEntity.setMailHostName(mailHostName);
        mailEntity.setMailFromUser(mailFromUser);
        mailEntity.setMailFromPwd(mailFromPwd);
        mailEntity.setMaiToUser(email);
        mailEntity.setMailSubject(mailSubject);
        mailEntity.setMailContent(mailContent);
        String rst="";
        try {
            MailUtil.sendMail(mailEntity);
            rst = verificationCode;

        } catch (EmailException e) {
            e.printStackTrace();
        }
        return rst;
    }
}


