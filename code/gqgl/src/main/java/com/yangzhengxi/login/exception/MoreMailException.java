package com.yangzhengxi.login.exception;

public class MoreMailException extends Exception {

    public MoreMailException(String message){
        super(message);
    }
}