package com.yangzhengxi.login.control;


import com.yangzhengxi.dto.UserDTO;
import com.yangzhengxi.mybatis.entity.PtUserEntity;
import com.yangzhengxi.user.repository.UserDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

@Controller
@RequestMapping("/login")
public class LoginController {
    private Logger logger = LogManager.getLogger(this.getClass());

    @Autowired
    private UserDao userDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping("/login")
    public String login(HttpServletRequest request) {
        Locale locale = LocaleContextHolder.getLocale();
        request.getSession().setAttribute("siteLanguage", locale.toString());
        logger.info("login");
        return "login/login";

    }

    @RequestMapping("/main")
    public String main() {
        logger.info("main");
        return "redirect:/";

    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register() {
        logger.info("register");
        return "login/addUser";

    }

    @PostMapping(value = "/register")
    public String registerLogic(@Validated(UserDTO.Register.class) UserDTO userDTO) {
        PtUserEntity ptUserEntity = new PtUserEntity();
        ptUserEntity.setUserName(userDTO.getUsername());
        ptUserEntity.setNiceName(userDTO.getNiceName());
        ptUserEntity.setEmail(userDTO.getEmail());
        ptUserEntity.setMobile(userDTO.getMobile());
        ptUserEntity.setRemark(userDTO.getRemark());
        ptUserEntity.setUserUuid(UUID.randomUUID().toString());
        ptUserEntity.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        userDao.createUser(ptUserEntity);
        return "login/login";
    }
    @RequestMapping("/fail")
    public String fail(HttpServletRequest request){
        HttpSession session = request.getSession();
        do{
            String erorrNum = (String)session.getAttribute("error.num.start.auth");
            if(erorrNum == null){
                break;
            }
            Integer erorrNumInt = Integer.parseInt(erorrNum) ;

            Integer loginErro = (Integer) session.getAttribute("loginErro");
            if(loginErro == null){
                loginErro = 1;
            }
            if(loginErro >= erorrNumInt){
                session.setAttribute("isAuthCode","true");
            }
            loginErro++;
            session.setAttribute("loginErro",loginErro);

        }while (false);

        return "redirect:/login/login?error";
    }

}
