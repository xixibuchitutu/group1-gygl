package com.yangzhengxi.login.service;


import com.yangzhengxi.login.exception.MoreMailException;
import com.yangzhengxi.login.exception.NotMailException;
import com.yangzhengxi.login.exception.NotMailException;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface AnonService {
    ResponseEntity<byte[]> getLoginVfcCode(HttpServletRequest request);

    boolean authVfcCode(String vfcCode, HttpServletRequest request);

    String getMailVfcCode(String email) throws NotMailException, MoreMailException;;
}