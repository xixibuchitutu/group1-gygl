package com.yangzhengxi.capacityElectric.service.impl;

import com.yangzhengxi.capacityElectric.entity.SearchElectricEntity;
import com.yangzhengxi.capacityElectric.respository.CapaticyElectricDao;

import com.yangzhengxi.capacityElectric.service.CapacityElectricService;
import com.yangzhengxi.mybatis.entity.CapacityElectricEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.spring.controller.BaseController;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CapaticyElectricServiceImpl extends BaseController implements CapacityElectricService {
    @Resource
    private CapaticyElectricDao CapaticyElectricDao;

    @Override
    public PadingRstType<CapacityElectricEntity> queryElectricInfoListByPage(SearchElectricEntity search, PaddingEntity padding) {
        padding.deal(CapacityElectricEntity.class);
        PadingRstType<CapacityElectricEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(padding.getPage());
        List<CapacityElectricEntity> list = CapaticyElectricDao.queryElectricInfoListByPage(search,padding);
        padingRstType.setRawRecords(list);
        Integer total = CapaticyElectricDao.queryElectricInfoTotal(search);
        padingRstType.setTotal(total);
        padingRstType.putItems();

        return padingRstType;
    }

    @Override
    public List queryDateData(SearchElectricEntity search) {
        return CapaticyElectricDao.queryDateData(search);
    }

    @Override
    public List queryUpData(SearchElectricEntity search) {
        return CapaticyElectricDao.queryUpData(search);
    }

    @Override
    public List queryCapacityData(SearchElectricEntity search) {
        return CapaticyElectricDao.queryCapacityData(search);
    }
}
