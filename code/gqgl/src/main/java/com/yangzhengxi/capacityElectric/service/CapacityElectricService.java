package com.yangzhengxi.capacityElectric.service;

import com.yangzhengxi.capacityElectric.entity.SearchElectricEntity;
import com.yangzhengxi.mybatis.entity.CapacityElectricEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;

import java.util.List;

public interface CapacityElectricService {
    PadingRstType<CapacityElectricEntity> queryElectricInfoListByPage(SearchElectricEntity search, PaddingEntity padding);

    List queryDateData(SearchElectricEntity search);

    List queryUpData(SearchElectricEntity search);

    List queryCapacityData(SearchElectricEntity search);
}