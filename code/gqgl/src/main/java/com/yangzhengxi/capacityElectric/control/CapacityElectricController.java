package com.yangzhengxi.capacityElectric.control;

import com.yangzhengxi.capacityElectric.entity.SearchElectricEntity;
import com.yangzhengxi.capacityElectric.service.CapacityElectricService;

import com.yangzhengxi.mybatis.entity.CapacityElectricEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.spring.controller.BaseController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/electrc")
public class CapacityElectricController extends BaseController {

    private Logger logger = LogManager.getLogger(this.getClass());

    @Resource
    private CapacityElectricService electricService;

    @RequestMapping("/page")
    public String electricPage() {
        return "electrc/page";
    }

    @Secured("ROLE_gqgy_data_electrc")
    @RequestMapping("/queryElectricInfoListByPage")
    @ResponseBody
    public PadingRstType<CapacityElectricEntity> queryElectricInfoListByPage(SearchElectricEntity search, PaddingEntity padding){
        logger.info(search);
        logger.info(padding);
        PadingRstType<CapacityElectricEntity> padingRstType = electricService.queryElectricInfoListByPage(search,padding);

        return padingRstType;
    }

    @RequestMapping("/queryDateData")
    @ResponseBody
    public List queryDateData(SearchElectricEntity search) {
        logger.info(electricService.queryDateData(search));
        return electricService.queryDateData(search);
    }

    @RequestMapping("/queryUpData")
    @ResponseBody
    public List queryUpData(SearchElectricEntity search) {
        logger.info(electricService.queryUpData(search));
        return electricService.queryUpData(search);
    }

    @RequestMapping("/queryCapacityData")
    @ResponseBody
    public List queryCapacityData(SearchElectricEntity search) {
        logger.info(electricService.queryCapacityData(search));
        return electricService.queryCapacityData(search);
    }
}
