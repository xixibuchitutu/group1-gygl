package com.yangzhengxi.capacityElectric.respository;

import com.yangzhengxi.capacityElectric.entity.SearchElectricEntity;
import com.yangzhengxi.mybatis.entity.CapacityElectricEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CapaticyElectricDao {
    List<CapacityElectricEntity> queryElectricInfoListByPage(@Param("search") SearchElectricEntity search, @Param("padding") PaddingEntity padding);

    Integer queryElectricInfoTotal(@Param("search") SearchElectricEntity search);

    List queryDateData(@Param("searchData") SearchElectricEntity search);

    List queryUpData(@Param("searchData") SearchElectricEntity search);

    List queryCapacityData(@Param("searchData") SearchElectricEntity search);
}
