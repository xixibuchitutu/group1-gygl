package com.yangzhengxi.cement_production.controller;
import com.yangzhengxi.cement_production.entity.SearchCementEntity;
import com.yangzhengxi.cement_production.service.CementService;
import com.yangzhengxi.mybatis.entity.CementEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.spring.controller.BaseController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;


@Controller
@RequestMapping("/sto")
public class CementController extends BaseController {

    private Logger logger = LogManager.getLogger(this.getClass());

    @Resource
    private com.yangzhengxi.cement_production.service.CementService CementService;

    @RequestMapping("/page")
    public String cementPage() {
        return "CementInfo/page";
    }

    @Secured("ROLE_gqgy_data_sto")
    @RequestMapping("/queryCementInfoListByPage")
    @ResponseBody
    public PadingRstType<CementEntity> queryCementInfoListByPage(SearchCementEntity search, PaddingEntity padding) {
        logger.info(search);
        logger.info(padding);
        PadingRstType<CementEntity> padingRstType = CementService.queryCementInfoListByPage(search, padding);

        return padingRstType;
    }
    @RequestMapping("/queryDateData")
    @ResponseBody
    public List queryDateData(SearchCementEntity search) {
        logger.info(CementService.queryDateData(search));
        return CementService.queryDateData(search);
    }

    @RequestMapping("/queryUpData")
    @ResponseBody
    public List queryUpData(SearchCementEntity search) {
        logger.info(CementService.queryUpData(search));
        return CementService.queryUpData(search);
    }

    @RequestMapping("/queryCementProductionData")
    @ResponseBody
    public List queryCementProductionData(SearchCementEntity search) {
        logger.info(CementService.queryCementProductionData(search));
        return CementService.queryCementProductionData(search);
    }
}