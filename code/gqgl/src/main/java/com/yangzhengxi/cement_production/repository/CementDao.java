package com.yangzhengxi.cement_production.repository;

import com.yangzhengxi.cement_production.entity.SearchCementEntity;
import com.yangzhengxi.mybatis.entity.CementEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author yyds
 * @Date 2022/7/4
 * @ClassName:GdpServiceImpl
 * @Version 1.0
 */
@Mapper
public interface CementDao {
    List<CementEntity> queryCementInfoListByPage(@Param("search") SearchCementEntity search, @Param("padding") PaddingEntity padding);

    Integer queryCementInfoTotal(@Param("search") SearchCementEntity search);
    List queryDateData(@Param("searchData") SearchCementEntity search);

    List queryUpData(@Param("searchData") SearchCementEntity search);

    List queryCementProductionData(@Param("searchData") SearchCementEntity search);
}
