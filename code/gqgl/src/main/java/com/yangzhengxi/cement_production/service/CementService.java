package com.yangzhengxi.cement_production.service;

import com.yangzhengxi.cement_production.entity.SearchCementEntity;
import com.yangzhengxi.mybatis.entity.CementEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;

import java.util.List;


/**
 * @Author yyds
 * @Date 2022/7/4
 * @ClassName:GdpService
 * @Version 1.0
 */

public interface CementService {
    PadingRstType<CementEntity> queryCementInfoListByPage(SearchCementEntity search, PaddingEntity padding);
    List queryDateData(SearchCementEntity search);

    List queryUpData(SearchCementEntity search);

    List queryCementProductionData(SearchCementEntity search);
}
