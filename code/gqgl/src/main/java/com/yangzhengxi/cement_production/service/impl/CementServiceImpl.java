package com.yangzhengxi.cement_production.service.impl;

import com.yangzhengxi.cement_production.entity.SearchCementEntity;
import com.yangzhengxi.cement_production.repository.CementDao;
import com.yangzhengxi.cement_production.service.CementService;
import com.yangzhengxi.mybatis.entity.CementEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.spring.controller.BaseController;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 * @Author yyds
 * @Date 2022/7/4
 * @ClassName:GdpServiceImpl
 * @Version 1.0
 */

@Service
public class CementServiceImpl extends BaseController implements CementService {

    @Resource
    private CementDao CementDao;

    @Override
    public PadingRstType<CementEntity> queryCementInfoListByPage(SearchCementEntity search, PaddingEntity padding) {
        padding.deal(CementEntity.class);
        PadingRstType<CementEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(padding.getPage());
        List<CementEntity> list = CementDao.queryCementInfoListByPage(search, padding);
        padingRstType.setRawRecords(list);
        Integer total = CementDao.queryCementInfoTotal(search);
        padingRstType.setTotal(total);
        padingRstType.putItems();

        return padingRstType;
    }

    @Override
    public List queryDateData(SearchCementEntity search) { return CementDao.queryDateData(search); }

    @Override
    public List queryUpData(SearchCementEntity search) {
        return CementDao.queryUpData(search);
    }

    @Override
    public List queryCementProductionData(SearchCementEntity search) {
        return CementDao.queryCementProductionData(search);
    }
}