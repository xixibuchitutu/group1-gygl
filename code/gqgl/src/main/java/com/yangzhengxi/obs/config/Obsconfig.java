package com.yangzhengxi.obs.config;

import com.obs.services.ObsClient;
import com.obs.services.model.BucketCors;
import com.obs.services.model.BucketCorsRule;


import java.util.ArrayList;
import java.util.List;

public class Obsconfig {
    public static void main(String[] args) {
        String endPoint = "obs.cn-north-4.myhuaweicloud.com";
        String ak = "XXQERVTEJCYSD8TZA9S5";
        String sk = "7vq9O02D53oK6xUgTYlDULYHNYsTSDP4hrQ7oq1C";
        // 创建ObsClient实例
        ObsClient obsClient = new ObsClient(ak, sk, endPoint);
        BucketCors cors = new BucketCors();
        List<BucketCorsRule> rules = new ArrayList<BucketCorsRule>();
        BucketCorsRule rule = new BucketCorsRule();
        ArrayList<String> allowedOrigin = new ArrayList<String>();
// 指定允许跨域请求的来源
        allowedOrigin.add( "http://localhost:8080/gqgl/#");
        allowedOrigin.add( "http://localhost:8080/gqgl/obs/Flie/ObjectList");
        rule.setAllowedOrigin(allowedOrigin);

        ArrayList<String> allowedMethod = new ArrayList<String>();
// 指定允许的跨域请求方法(GET/PUT/DELETE/POST/HEAD)
        allowedMethod.add("GET");
        allowedMethod.add("HEAD");
        allowedMethod.add("PUT");
        rule.setAllowedMethod(allowedMethod);

        ArrayList<String> allowedHeader = new ArrayList<String>();
// 控制在OPTIONS预取指令中Access-Control-Request-Headers头中指定的header是否被允许使用
        allowedHeader.add("x-obs-header");
        rule.setAllowedHeader(allowedHeader);

        ArrayList<String> exposeHeader = new ArrayList<String>();
// 指定允许用户从应用程序中访问的header
        exposeHeader.add("x-obs-expose-header");
        rule.setExposeHeader(exposeHeader);

// 指定浏览器对特定资源的预取(OPTIONS)请求返回结果的缓存时间,单位为秒
        rule.setMaxAgeSecond(10);
        rules.add(rule);
        cors.setRules(rules);

        obsClient.setBucketCors("bucketname", cors);
    }

}
