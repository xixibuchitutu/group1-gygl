package com.yangzhengxi.obs.controller;

import com.obs.services.model.ObsBucket;
import com.obs.services.model.ObsObject;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.obs.entity.FileUploadResult;
import com.yangzhengxi.obs.service.FileUploadService;
import com.yangzhengxi.spring.controller.BaseController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * @Author yyds
 * @Date 2022/7/6
 * @ClassName:FileUploadController
 * @Version 1.0
 */
@Controller
@RequestMapping("/obs")
public class FileUploadController extends BaseController {

    private Logger logger = LogManager.getLogger(this.getClass());
    @Autowired
    private FileUploadService fileUploadService;

    @RequestMapping("/page")
    public String obsPage(HttpSession session) {
        session.setAttribute("fileList",fileUploadService.list());
        return "obs/page";
    }

    /**
     * @desc 文件上传到obs
     * @return FileUploadResult
     * @Param uploadFile
     */
    @RequestMapping("file/upload")
    @ResponseBody
    public FileUploadResult upload(@RequestParam("file") MultipartFile uploadFile) throws Exception {
        return this.fileUploadService.upload(uploadFile);
    }

    /**
     * @return FileUploadResult
     * @desc 根据文件名删除obs上的文件
     * @Param objectName
     */
    @RequestMapping("file/delete")
    @ResponseBody
    public String delete(@RequestParam("fileName") String objectName) throws Exception {
        fileUploadService.delete(objectName);
        return jsonFail("file.delete.success");
    }

    /**
     * @desc 查询obs上的所有文件
     * @return List<OSSObjectSummary>
     * @Param
     */
    @RequestMapping("file/list")
    @ResponseBody
    public List<ObsObject> list() throws Exception {
        System.out.println(fileUploadService.list());
        return this.fileUploadService.list();
    }


    /**
     * @desc 根据文件名下载oss上的文件
     * @return
     * @Param objectName
     */

    @RequestMapping("/download/{fileName}")
    @ResponseBody
    public ResponseEntity<byte[]> download(@PathVariable("fileName") String objectName) throws IOException {
        //通知浏览器以附件形式下载
        logger.info("objectName:" + objectName);
        ResponseEntity<byte[]>  responseEntity = fileUploadService.exportOssFile(objectName);

//        byte[] contents=null;
//
//        HttpHeaders headers =
//                new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);//设置MIME类型
//
//        return new ResponseEntity<byte[]>(
//                contents,
//                headers,
//                HttpStatus.OK);
        return  responseEntity;

    }
    @RequestMapping("Flie/bucketList")
    @ResponseBody
    public PadingRstType<ObsBucket> bucketList(PaddingEntity padding) throws IOException {
        return fileUploadService.bucketList(padding);
    }
    @RequestMapping("Flie/ObjectList")
    @ResponseBody
    public PadingRstType<ObsObject> ObjectList(PaddingEntity padding) throws IOException {
        return  fileUploadService.ObjectList(padding);
    }


    @RequestMapping("share")
    @ResponseBody
    public String share(@RequestParam("fileName") String objectName) throws IOException {
        String url = this.fileUploadService.share(objectName);
        return jsonMsgSuccess(url);
    }
    @RequestMapping("searchUrl")
    @ResponseBody
    public String searchUrl(@RequestParam("fileName") String objectName) throws IOException {
        return fileUploadService.searchUrl(objectName);
    }
    @RequestMapping("copy")
    @ResponseBody
    public void copy(@RequestParam("fileName") String objectName) throws IOException {
        fileUploadService.copy(objectName);
    }
    @RequestMapping("update")
    @ResponseBody
    public void update(@RequestParam("newfileName") String newobjectName, @RequestParam("objectKey")String objectKey) throws IOException {
        fileUploadService.update(newobjectName,objectKey);
    }

    @RequestMapping("getListVersionFile")
    public void getListVersionFile() {
        fileUploadService.getListVersionFile();
    }
}
