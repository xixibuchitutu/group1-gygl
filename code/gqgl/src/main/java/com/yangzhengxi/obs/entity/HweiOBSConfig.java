package com.yangzhengxi.obs.entity;

import com.obs.services.ObsClient;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @Author yyds
 * @Date 2022/7/6
 * @ClassName:HweiOBSConfig
 * @Version 1.0
 */
@Configuration
@PropertySource(value = {"classpath:application.properties"})
@ConfigurationProperties(prefix = "huaweiyun")
@Data
public class HweiOBSConfig {
    private String endPoint;
    private String accessKey;
    private String securityKey;

    private String bucketName;

    private String urlPrefix;


    public String getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecurityKey() {
        return securityKey;
    }

    public void setSecurityKey(String securityKey) {
        this.securityKey = securityKey;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getUrlPrefix() {
        return urlPrefix;
    }

    public void setUrlPrefix(String urlPrefix) {
        this.urlPrefix = urlPrefix;
    }

    @Bean
    public ObsClient getObsClient(){
        ObsClient obsClient = new ObsClient(accessKey, securityKey, endPoint);
        return obsClient;
    }

    @Override
    public String toString() {
        return "HweiOBSConfig{" +
                "endPoint='" + endPoint + '\'' +
                ", accessKey='" + accessKey + '\'' +
                ", securityKey='" + securityKey + '\'' +
                ", bucketName='" + bucketName + '\'' +
                ", urlPrefix='" + urlPrefix + '\'' +
                '}';
    }
}
