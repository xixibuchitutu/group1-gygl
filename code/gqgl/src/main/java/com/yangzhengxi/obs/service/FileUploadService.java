package com.yangzhengxi.obs.service;

import com.obs.services.model.*;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.obs.entity.FileUploadResult;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.List;

/**
 * @Author yyds
 * @Date 2022/7/6
 * @ClassName:ObsUtil
 * @Version 1.0
 */
public interface FileUploadService{

    /**
     *  文件上传
     */
    public FileUploadResult upload(MultipartFile uploadFile) throws IOException;

    /**
     *  生成文件名
     */
    public String getFilePath(String sourceFileName);

    public List<ObsObject> list();
    public FileUploadResult delete(String objectName);

    /**
     *  下载文件
     */
    public ResponseEntity<byte[]> exportOssFile(String objectName) throws IOException;


    /**
     * 桶列表
     */
    public PadingRstType<ObsBucket> bucketList(PaddingEntity padding );


    /**
     * 对象列表
     * @param padding
     * @return
     */
    public PadingRstType<ObsObject> ObjectList(PaddingEntity padding);
    /**
     * 文件分享
     * @return
     */
    public String share(String objectName);

    public String searchUrl(String objectName);
    void copy(String objectName);

    void update(String newobjectName, String ObjectKey);

    /**
     *  下载文件
     */


    void getListVersionFile();
}