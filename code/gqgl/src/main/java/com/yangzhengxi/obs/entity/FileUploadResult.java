package com.yangzhengxi.obs.entity;

import lombok.Data;

/**
 * @Author yyds
 * @Date 2022/7/6
 * @ClassName:FileUploadResult
 * @Version 1.0
 */
@Data
public class FileUploadResult {
    private String uid;

    private String name;

    private String status;

    private String response;
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "FileUploadResult{" +
                "uid='" + uid + '\'' +
                ", name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", response='" + response + '\'' +
                '}';
    }
}
