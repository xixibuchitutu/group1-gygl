package com.yangzhengxi.obs.service.Impl;

import com.obs.services.ObsClient;
import com.obs.services.exception.ObsException;
import com.obs.services.model.*;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.obs.entity.FileUploadResult;
import com.yangzhengxi.obs.entity.HweiOBSConfig;
import com.yangzhengxi.obs.service.FileUploadService;
import com.yangzhengxi.obs.util.ObsClientUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.Date;
import java.util.List;

@Service
public class FileUploadServiceImpl implements FileUploadService {
    @Autowired
    private ObsClient obsClient;
    @Autowired
    private HweiOBSConfig hweiOBSConfig;


    /**
     *  文件上传
     */
    public FileUploadResult upload(MultipartFile uploadFile) throws IOException {
        FileUploadResult fileUploadResult = new FileUploadResult();

        String fileName = uploadFile.getOriginalFilename();
        String filePath = getFilePath(fileName);
        PutObjectRequest request = new PutObjectRequest();
        request.setBucketName(hweiOBSConfig.getBucketName());
        request.setObjectKey(fileName);
        request.setInput( new ByteArrayInputStream(uploadFile.getBytes()));
// 设置对象访问权限为公共读
        request.setAcl(AccessControlList.REST_CANNED_PUBLIC_READ);
        obsClient.putObject(request);

        // 上传到华为云
        try {
            obsClient.putObject(request);
        } catch (Exception e) {
            e.printStackTrace();
            //上传失败
            fileUploadResult.setStatus("error");
            return fileUploadResult;
        }
        fileUploadResult.setStatus("done");
        fileUploadResult.setResponse("success");
        fileUploadResult.setName(this.hweiOBSConfig.getUrlPrefix() + filePath);
        fileUploadResult.setUid(String.valueOf(System.currentTimeMillis()));
        return fileUploadResult;
    }

    /**
     *  生成文件名
     */
    public String getFilePath(String sourceFileName) {
        return sourceFileName;
    }

    /**
     *  查看文件列表
     */
    public List<ObsObject> list() {
        // 列举文件。
        ObjectListing objectListing = obsClient.listObjects(new ListObjectsRequest(hweiOBSConfig.getBucketName()));
        List<ObsObject> sums = objectListing.getObjects();
        return sums;
    }

    /**
     *  删除文件
     */
    public FileUploadResult delete(String objectName) {
        obsClient.deleteObject(hweiOBSConfig.getBucketName(), objectName);
        FileUploadResult fileUploadResult = new FileUploadResult();
        fileUploadResult.setName(objectName);
        fileUploadResult.setStatus("removed");
        fileUploadResult.setResponse("success");
        return fileUploadResult;
    }



    /**
     *  下载文件
     */
    public ResponseEntity<byte[]> exportOssFile( String objectName) throws IOException {
        ObsObject obsObject = obsClient.getObject("java-test1", objectName);
        // 读取文件内容。
        BufferedInputStream in = new BufferedInputStream(obsObject.getObjectContent());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int lenght = 0;
        while ((lenght = in.read(buffer)) != -1) {
            out.write(buffer, 0, lenght);
        }
        byte[] contents=out.toByteArray();
        if (out != null) {
            out.flush();
            out.close();
        }
        if (in != null) {
            in.close();
        }


        HttpHeaders headers =
                new HttpHeaders();
        headers.setContentDispositionFormData("attachment"
                ,objectName
        );//告知浏览器以下载方式打开
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);//设置MIME类型
        return new ResponseEntity<byte[]>(
                contents,
                headers,
                HttpStatus.OK);
    }

    /**
     * 桶列表
     */
    public PadingRstType<ObsBucket> bucketList(PaddingEntity padding ){

        padding.deal(ObsBucket.class);
        PadingRstType<ObsBucket> rstType = new PadingRstType<>();
        rstType.setPage(padding.getPage());
        ListBucketsRequest request = new ListBucketsRequest();
        request.setQueryLocation(true);
        List<ObsBucket> buckets = obsClient.listBuckets(request);
        rstType.setRawRecords(buckets);

        rstType.setTotal(buckets.size());
        rstType.putItems();
        return rstType;
    }
    private static String bucketname = "java-test1";

    /**
     * 对象列表
     * @param padding
     * @return
     */
    public PadingRstType<ObsObject> ObjectList(PaddingEntity padding) {
        padding.deal(ObsObject.class);
        PadingRstType<ObsObject> rstType = new PadingRstType<>();
        rstType.setPage(padding.getPage());
        ObjectListing result = obsClient.listObjects("java-test1");
        List<ObsObject> sums = result.getObjects();

        rstType.setRawRecords(sums);
        rstType.setTotal(sums.size());
        rstType.putItems();
        return rstType;
    }

    /**
     * 文件分享
     * @return
     */
    public String share(String objectName) {
        String obsKey =objectName;
        TemporarySignatureRequest request = new
                TemporarySignatureRequest();
        request.setBucketName("java-test1");
        request.setObjectKey(obsKey);
        request.setRequestDate(new Date());
        request.setExpires(60*60);
        TemporarySignatureResponse signature =
                obsClient.createTemporarySignature(request);
        String url = signature.getSignedUrl();
        System.out.println(url);
        return url;
    }

    public String searchUrl(String objectName) {
        String obsKey =objectName;

        TemporarySignatureRequest request = new
                TemporarySignatureRequest();
        request.setBucketName("java-test1");
        request.setObjectKey(obsKey);
        request.setRequestDate(new Date());
        request.setExpires(60*60);
        TemporarySignatureResponse signature =
                obsClient.createTemporarySignature(request);
        String url = signature.getSignedUrl();
        return url;
    }
    @Override
    public void copy(String objectName) {

        try{
            CopyObjectResult result = obsClient.copyObject("java-test1", "objectName", "java-test1", "objectName");
            System.out.println("\t" + result.getStatusCode());
            System.out.println("\t" + result.getEtag());
        }
        catch (ObsException e)
        {
            // 复制失败
            System.out.println("HTTP Code: " + e.getResponseCode());
            System.out.println("Error Code:" + e.getErrorCode());
            System.out.println("Error Message: " + e.getErrorMessage());

            System.out.println("Request ID:" + e.getErrorRequestId());
            System.out.println("Host ID:" + e.getErrorHostId());
        }

    }
    @Override
    public void update(String newobjectName, String ObjectKey) {
        ObsClient obsClient = new ObsClient("XXQERVTEJCYSD8TZA9S5", "7vq9O02D53oK6xUgTYlDULYHNYsTSDP4hrQ7oq1C", "obs.cn-north-4.myhuaweicloud.com");

        RenameObjectRequest request = new RenameObjectRequest();
        request.setBucketName("java-test1");
// objectKey 为原对象的完整对象名
        request.setObjectKey("ObjectKey");
// newObjectKey 为目标对象的完整对象名
        request.setNewObjectKey("newobjectName");
        RenameObjectResult result = obsClient.renameObject(request);
    }

    /**
     * 按文件夹分组列举所有多版本对象
     * https://support.huaweicloud.com/sdk-java-devg-obs/obs_21_1007.html#section5
     */
    @Override
    public void getListVersionFile() {
        ObsClient obsClient = ObsClientUtil.obsClient;
        ListVersionsRequest request = new ListVersionsRequest("java-test1", 1000);
        request.setDelimiter("/");
        ListVersionsResult result = obsClient.listVersions(request);
        System.out.println("Objects in the root directory:");
        for(VersionOrDeleteMarker v : result.getVersions()){
            System.out.println(v.toString());
        }
        listVersionsByPrefix(obsClient, result);
    }

    void listVersionsByPrefix(ObsClient obsClient, ListVersionsResult result) throws ObsException{
        for(String prefix : result.getCommonPrefixes()){
            System.out.println("Objects in folder [" + prefix + "]:");
            ListVersionsRequest request = new ListVersionsRequest ("java-test1", 1000);
            request.setDelimiter("/");
            request.setPrefix(prefix);
            result = obsClient.listVersions(request);
            for(VersionOrDeleteMarker v : result.getVersions()){
                System.out.println(v.toString());
            }
            listVersionsByPrefix(obsClient, result);
        }
    }
}
