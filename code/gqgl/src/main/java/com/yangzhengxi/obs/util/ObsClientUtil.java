package com.yangzhengxi.obs.util;

import com.obs.services.ObsClient;
import com.obs.services.exception.ObsException;
import com.obs.services.model.BucketCors;
import com.obs.services.model.BucketCorsRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * 初始化obsClient工具类
 */
@Component
public class ObsClientUtil implements InitializingBean {
    Logger logger = LoggerFactory.getLogger(ObsClientUtil.class);
    public static ObsClient obsClient;

    @Value("${huaweiyun.accessKey:XXQERVTEJCYSD8TZA9S5}")
    private  String accessKey;
    @Value("${huaweiyun.securityKey:7vq9O02D53oK6xUgTYlDULYHNYsTSDP4hrQ7oq1C}")
    private  String securityKey;
    @Value("${huaweiyun.endPoint:obs.cn-north-4.myhuaweicloud.com}")
    private  String endPoint;
    @Value("${huaweiyun.bucketName:java-test1}")
    private  String bucketName;

    @Override
    public void afterPropertiesSet() {
        logger.info("ObsClientUtil start init");
        try {
            obsClient = new ObsClient(accessKey, securityKey, endPoint);
            BucketCors cors = new BucketCors();
            List<BucketCorsRule> rules = new ArrayList<BucketCorsRule>();
            BucketCorsRule rule = new BucketCorsRule();
            // 指定允许跨域请求的来源
            ArrayList<String> allowedOrigin = new ArrayList<String>();
            allowedOrigin.add( "*");
            rule.setAllowedOrigin(allowedOrigin);
            // 指定允许跨域请求的方法
            ArrayList<String> allowedMethod = new ArrayList<String>();
            allowedMethod.add("GET");
            allowedMethod.add("POST");
            allowedMethod.add("PUT");
            allowedMethod.add("DELETE");
            allowedMethod.add("HEAD");
            rule.setAllowedMethod(allowedMethod);
            // 指定允许跨域请求的头域
            ArrayList<String> allowedHeader = new ArrayList<String>();
            allowedHeader.add("*");
            rule.setAllowedHeader(allowedHeader);
            // 指定允许跨域请求的响应中的附加头域
            ArrayList<String> exposeHeader = new ArrayList<String>();
            exposeHeader.add("ETag");
            exposeHeader.add("Content-Type");
            exposeHeader.add("Content-Length");
            exposeHeader.add("Cache-Control");
            exposeHeader.add("Content-Disposition");
            exposeHeader.add("Content-Encoding");
            exposeHeader.add("Content-Language");
            exposeHeader.add("Expires");
            exposeHeader.add("x-obs-request-id");
            exposeHeader.add("x-obs-id-2");
            exposeHeader.add("x-reserved-indicator");
            exposeHeader.add("x-obs-api");
            exposeHeader.add("x-obs-version-id");
            exposeHeader.add("x-obs-copy-source-version-id");
            exposeHeader.add("x-obs-storage-class");
            exposeHeader.add("x-obs-delete-marker");
            exposeHeader.add("x-obs-expiration");
            exposeHeader.add("x-obs-website-redirect-location");
            exposeHeader.add("x-obs-restore");
            exposeHeader.add("x-obs-version");
            exposeHeader.add("x-obs-object-type");
            exposeHeader.add("x-obs-next-append-position");
            rule.setExposeHeader(exposeHeader);
            rule.setMaxAgeSecond(100);
            rules.add(rule);
            cors.setRules(rules);
            obsClient.setBucketCors(bucketName, cors);
        } catch (Exception e) {
            logger.error("ObsClientUtil init error {}", e);
        }
        logger.info("ObsClientUtil end init");
    }
}
