package com.yangzhengxi.user.po;


import java.io.Serializable;

/**
 * 用户信息po
 */
public class PtUserPo implements Serializable {
    /**
     * 用户编码
     */
    private String userUuid;
    /**
     * 用户密码
     */
    private String password;
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 邮件地址
     */
    private String email;

    private String newPassword;

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
