package com.yangzhengxi.user.service.impl;


import com.yangzhengxi.login.entity.MenuLevelEntity;
import com.yangzhengxi.mybatis.entity.PtDutyEntity;
import com.yangzhengxi.mybatis.entity.PtMenuEntity;
import com.yangzhengxi.mybatis.entity.PtUserEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.spring.util.DataUitl;
import com.yangzhengxi.user.entity.SearchUserEntity;
import com.yangzhengxi.user.entity.UserDutyEntity;
import com.yangzhengxi.user.repository.UserDao;
import com.yangzhengxi.user.service.UserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service("userService")
public class UserServiceImpl implements UserService {
    private final static Integer MENU_FIRST = 1;
    @Resource
    private UserDao userDao;
    @Value("${passwd.key}")
    private String passwdKey;

    @Override
    public List<PtUserEntity> queryUserInfoListByName(SearchUserEntity search) {
        return userDao.queryUserInfoListByName(search);
    }

    @Override
    public void deleteUserInfoByIds(String ids) throws UncategorizedSQLException {
        String[] array = ids.split(",");
        for (String id : array) {
            userDao.deleteUserInfoByIds(id);
        }

    }

    @Override
    public PtUserEntity queryUserInfoByName(String userName) {
        return userDao.queryUserInfoByName(userName);
    }

    @Override
    public List<PtMenuEntity> queryMenuList() {
        Locale locale = LocaleContextHolder.getLocale();

        return userDao.queryMenuListByLang(locale.toString());
    }

    @Override
    public List<MenuLevelEntity> queryMenuLevelList() {
        List<PtMenuEntity> ptMenuEntityList = queryMenuList();
        return dealMenuLevel(ptMenuEntityList);
    }

    @Override
    public void modifyUserInfoById(PtUserEntity ptUserEntity) {
        userDao.modifyUserInfoById(ptUserEntity);
    }

    @Override
    public List<String> queryrivilegeList(String userUuid) {
        return userDao.queryrivilegeList(userUuid);
    }

    @Override
    public PadingRstType<PtUserEntity> queryUserInfoListByPage(SearchUserEntity entity, PaddingEntity paddingEntity) {
        paddingEntity.deal(PtUserEntity.class);

        PadingRstType<PtUserEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(paddingEntity.getPage());
        List<PtUserEntity> list = userDao.queryUserInfoListByPage(entity, paddingEntity);
        padingRstType.setRawRecords(list);
        Integer total = userDao.queryUserInfoTotal(entity);
        padingRstType.setTotal(total);
        padingRstType.putItems();
        return padingRstType;
    }

    @Resource
    private MessageSource messageSource;

    @Override
    public List<ZtreeEntity> queryRoleTreeList(String userUuid) {

        List<ZtreeEntity> list = userDao.queryRoleTreeList();
        ZtreeEntity ztreeEntity = new ZtreeEntity();
        ztreeEntity.setId("-1");
        Locale locale = LocaleContextHolder.getLocale();
        String roleInfo = messageSource.getMessage("role.info", null, locale);
        ztreeEntity.setName(roleInfo);
        ztreeEntity.setpId("-2");
        list.add(ztreeEntity);

        List<String> roleUuuidList = userDao.queryRoleByUserUuuid(userUuid);
        for (ZtreeEntity ztreeEntityItem : list) {
            if (roleUuuidList.contains(ztreeEntityItem.getId())) {
                ztreeEntityItem.setChecked(true);
            }
        }

        return list;
    }

    @Override
    public void saveUserIdAndRoleId(String userUuid, String roleArray) {
        userDao.delRoleByUserId(userUuid);
        String[] roleIds = roleArray.split(",");
        for (String roleId : roleIds) {
            userDao.insertRoleRefUser(userUuid, roleId);
        }

    }

    @Override
    public List<MenuLevelEntity> queryMenuLevelList(String userUuid) {
        Locale lang = LocaleContextHolder.getLocale();

        List<PtMenuEntity> ptMenuEntityList = userDao.queryMenuLevelListByUserUuid(userUuid, lang.toString());

        return dealMenuLevel(ptMenuEntityList);

    }

    @Override
    public PtUserEntity queryUserInfoByMail(String email) {
        List<PtUserEntity> list = userDao.queryUserInfoByMail(email);
        if (list == null || list.size() == 0) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public void addUser(PtUserEntity ptUserEntity) {
        userDao.addUser(ptUserEntity);
    }

    @Override
    public void addUserInfo(PtUserEntity ptUserEntity) {
        ptUserEntity.setUserUuid(DataUitl.getUuid());
        String rst = DigestUtils.md5DigestAsHex((ptUserEntity.getPassword() + passwdKey).getBytes());
        ptUserEntity.setPassword(rst);
        userDao.insertUserInfo(ptUserEntity);

    }

    @Override
    public void deleteUserById(String userUuidArray) {
        String[] userUuids = userUuidArray.split(",");
        userDao.deleteUserById(userUuids);
    }

    @Override
    public void allocatedepInfo(String DePid, String userUuid) {
        userDao.allocatedepInfo(DePid, userUuid);
    }

    @Override
    public void modifyUserInfo(PtUserEntity ptUserEntity) {
        userDao.modifyUserInfo(ptUserEntity);
    }

    private List<MenuLevelEntity> dealMenuLevel(List<PtMenuEntity> ptMenuEntityList) {
        List<MenuLevelEntity> menuLevelEntityList = new ArrayList<>();
        MenuLevelEntity menuLevelEntity1;
        MenuLevelEntity menuLevelEntity2;
        MenuLevelEntity menuLevelEntity3;
        for (PtMenuEntity menu1 : ptMenuEntityList) {
            // 一级菜单
            if (MENU_FIRST.equals(menu1.getMenuLevel())) {
                menuLevelEntity1 = new MenuLevelEntity(menu1);
                menuLevelEntityList.add(menuLevelEntity1);
                for (PtMenuEntity menu2 : ptMenuEntityList) {
                    //二级菜单
                    if (menuLevelEntity1.getMenuId().equals(menu2.getParentId())) {
                        menuLevelEntity2 = new MenuLevelEntity(menu2);
                        menuLevelEntity1.addChildren(menuLevelEntity2);
                        //三级菜单
                        for (PtMenuEntity menu3 : ptMenuEntityList) {
                            if (menuLevelEntity2.getMenuId().equals(menu3.getParentId())) {
                                menuLevelEntity3 = new MenuLevelEntity(menu3);
                                menuLevelEntity2.addChildren(menuLevelEntity3);

                            }
                        }
                    }
                }
            }
        }
        return menuLevelEntityList;
    }

    @Override
    public void modifyUserDepartment(PtUserEntity ptUserEntity) {
        userDao.modifyUserDepartment(ptUserEntity);
    }

    @Override
    public void modifyUserOrgan(PtUserEntity ptUserEntity) {
        userDao.modifyUserOrgan(ptUserEntity);
    }

    @Override
    public void allocateDuty(UserDutyEntity userDutyEntity) {
        userDao.allocateDuty(userDutyEntity);
    }

    @Override
    public List<PtDutyEntity> queryDutyInfo() {
        return userDao.queryDutyInfo();
    }
}

