package com.yangzhengxi.user.control;

import com.yangzhengxi.common.ServerResponse;
import com.yangzhengxi.mybatis.entity.PtUserEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.spring.controller.BaseController;
import com.yangzhengxi.spring.util.DataUitl;
import com.yangzhengxi.user.entity.SearchUserEntity;
import com.yangzhengxi.user.entity.UserDutyEntity;
import com.yangzhengxi.user.po.PtUserPo;
import com.yangzhengxi.user.repository.UserDao;
import com.yangzhengxi.user.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;
@Controller
@RequestMapping("/user")
public class UserController extends BaseController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private UserService userService;
    @Resource
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDao userDao;

    @Secured("ROLE_gqgy_data_user")
    @RequestMapping("/userPage")
    public String userPage(HttpSession session){
        session.setAttribute("list4",userService.queryDutyInfo());
        return "user/userPage";
    }



    @RequestMapping("/queryUserInfoListByName")
    @ResponseBody
    public List<PtUserEntity> queryUserInfoListByName(SearchUserEntity search){
        logger.info(search);
        List<PtUserEntity> list = userService.queryUserInfoListByName(search);
        return list;
    }
    @RequestMapping("/deleteUserInfoByIds")
    @ResponseBody
    public String deleteUserInfoByIds(@RequestParam(value = "ids",required = false) String ids){
        logger.info("ids:" + ids);

        try {
            userService.deleteUserInfoByIds(ids);
        } catch (UncategorizedSQLException e) {
            e.printStackTrace();
            return jsonFail("user.del.user.fail");

        }
        return jsonSuccess("user.del.user.success");
    }

    @RequestMapping("/modifyUserPasswordById")
    @ResponseBody
    public String modifyUserPasswordById(PtUserPo ptUserPo){
        logger.info(ptUserPo);
        PtUserEntity ptUserEntity = userService.queryUserInfoByName(ptUserPo.getUserName());
        boolean passwordMatches = passwordEncoder.matches(ptUserPo.getPassword(),ptUserEntity.getPassword());
        if(!passwordMatches){
            return jsonFail("input.the.correct.original.password");
        }else{
            String newPassword = passwordEncoder.encode(ptUserPo.getNewPassword());
            ptUserEntity.setPassword(newPassword);
            ptUserEntity.setEmail(ptUserPo.getEmail());
            userService.modifyUserInfoById(ptUserEntity);
        }
        return jsonSuccess("user.password.info.success");
    }

    @RequestMapping("/modifyUserInfoById")
    @ResponseBody
    public String modifyUserInfoById(PtUserEntity ptUserEntity){
        logger.info(ptUserEntity);
        PtUserEntity ptUserEntity2 = userService.queryUserInfoByName(ptUserEntity.getUserName());
        ptUserEntity2.setUserName(ptUserEntity.getUserName());
        ptUserEntity2.setMobile(ptUserEntity.getMobile());
        ptUserEntity2.setEmail(ptUserEntity.getEmail());
        ptUserEntity2.setNiceName(ptUserEntity.getNiceName());
        userService.modifyUserInfoById(ptUserEntity2);
        return jsonSuccess("user.person.info.success");
    }


    //    @Secured("ROLE_gqgy_data_user_query")
    @RequestMapping("/queryUserInfoListByPage")
    @ResponseBody
    public PadingRstType<PtUserEntity> queryUserInfoListByPage(SearchUserEntity entity, PaddingEntity paddingEntity){
        logger.info(entity);
        logger.info(paddingEntity);
        PadingRstType<PtUserEntity>  padingRstType= userService.queryUserInfoListByPage(entity,paddingEntity);
        return padingRstType;
    }
    @Secured("ROLE_gqgy_data_user_allow")
    @RequestMapping("/allocateRoleTree")
    @ResponseBody
    public List<ZtreeEntity> allocateRoleTree(@RequestParam("userUuid") String userUuid){
        logger.info("userUuid:" + userUuid);

        List<ZtreeEntity> list  = userService.queryRoleTreeList(userUuid);
        return list;
    }
    @Secured("ROLE_gqgy_data_user_allow")
    @RequestMapping("/saveUserIdAndRoleId")
    @ResponseBody
    public String saveUserIdAndRoleId(@RequestParam("userUuid") String userUuid,@RequestParam("roleArray")String roleArray){
        logger.info("userUuid:" + userUuid + "  roleArray" + roleArray);
        userService.saveUserIdAndRoleId(userUuid,roleArray);
        return jsonSuccess("user.role.allocate.success");
    }

    @RequestMapping("/regist")
    @ResponseBody
    public String addUser(PtUserEntity ptUserEntity){
        logger.info(ptUserEntity);
        ptUserEntity.setUserUuid(DataUitl.getUuid());
        userService.addUser(ptUserEntity);
        return jsonSuccess("user.regist.info.success");
    }

    @RequestMapping("/toRegist")
    public String toRegist(){
        return "addUser";
    }

    @Secured("ROLE_gqgy_data_user_add")
    @RequestMapping("/addUserInfo")
    @ResponseBody
    public String addUserInfo(PtUserEntity ptUserEntity){
        logger.info(ptUserEntity);
        userService.addUserInfo(ptUserEntity);
        return jsonSuccess("user.add.success");
    }

    @Secured("ROLE_gqgy_data_user_update")
    @RequestMapping("/modifyUserInfo")
    @ResponseBody
    public String modifyUserInfo(PtUserEntity ptUserEntity){
        logger.info(ptUserEntity);
        userService.modifyUserInfo(ptUserEntity);
        return jsonSuccess("user.modify.success");
    }

    @Secured("ROLE_gqgy_data_user_delete")
    @RequestMapping("/deleteUserById")
    @ResponseBody
    public String deleteRoleById(@RequestParam("userUuidArray") String userUuidArray){
        logger.info("userUuidArray:" + userUuidArray);
        userService.deleteUserById(userUuidArray);
        return jsonSuccess("user.delete.success");
    }

    @RequestMapping("/allocatedepInfo")
    @ResponseBody
    public String allocatedepInfo(@RequestParam("DePid")String DePid,@RequestParam("userUuid")String userUuid){
        logger.info("ptUserEntity:" + DePid);
        userService.allocatedepInfo(DePid,userUuid);
        return jsonSuccess("user.allocatedep.success");
    }
    /**
     * 用户解锁
     *
     * @return
     */
    @PostMapping("/{uId}/unlock")
    @ResponseBody
    public ServerResponse userUnLock(@PathVariable("uId") String uid) {
        int i = userDao.userUnlock(uid);
        if (i >= 1) {
            return ServerResponse.responseWithSuccess();
        } else {
            return ServerResponse.responseWithFailure("解锁失败!");
        }
    }
    @RequestMapping("/modifyUserDepartment")
    @ResponseBody
    public String modifyUserDepartment(PtUserEntity ptUserEntity) {
        logger.info(ptUserEntity);
        userService.modifyUserDepartment(ptUserEntity);
        return jsonSuccess("department.modify.success");
    }

    @RequestMapping("/modifyUserOrgan")
    @ResponseBody
    public String modifyUserOrgan(PtUserEntity ptUserEntity) {
        logger.info(ptUserEntity);
        userService.modifyUserOrgan(ptUserEntity);
        return jsonSuccess("organ.modify.success");
    }

    @RequestMapping("/allocateDuty")
    @ResponseBody
    public String allocateDuty(UserDutyEntity userDutyEntity){
        userService.allocateDuty(userDutyEntity);
        return jsonSuccess("user.allocatedep.success");
    }
}

