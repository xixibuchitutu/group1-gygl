package com.yangzhengxi.user.entity;
import org.springframework.security.core.userdetails.User;
import com.yangzhengxi.mybatis.entity.PtUserEntity;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class SecurityUser extends User {
    /**
     * 用户编码
     */
    private String userUuid;
    /**
     * 邮件地址
     */
    private String email;
    /**
     * 电话
     */
    private String mobile;
    /**
     * 真实姓名
     */
    private String niceName;

  private String DePid;

    public String getDePid() {
        return DePid;
    }

    public void setDePid(String DePid) {
        DePid = DePid;
    }

    public SecurityUser(String username, String password, PtUserEntity ptUserEntity, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
        this.userUuid = ptUserEntity.getUserUuid();
        this.email = ptUserEntity.getEmail();
        this.mobile = ptUserEntity.getMobile();
        this.niceName = ptUserEntity.getNiceName();

    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNiceName() {
        return niceName;
    }

    public void setNiceName(String niceName) {
        this.niceName = niceName;
    }

    @Override
    public String toString() {
        return "SecurityUser{" +
                "userUuid='" + userUuid + '\'' +
                ", email='" + email + '\'' +
                ", mobile='" + mobile + '\'' +

                ", niceName='" + niceName + '\'' +
                '}';
    }
}

