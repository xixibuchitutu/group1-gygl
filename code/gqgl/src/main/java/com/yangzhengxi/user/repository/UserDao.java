package com.yangzhengxi.user.repository;



import com.yangzhengxi.mybatis.entity.PtDutyEntity;
import com.yangzhengxi.mybatis.entity.PtMenuEntity;
import com.yangzhengxi.mybatis.entity.PtUserEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.user.entity.SearchUserEntity;
import com.yangzhengxi.user.entity.UserDutyEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserDao {
    List<PtUserEntity> queryUserInfoListByName(@Param("search") SearchUserEntity search);

    void deleteUserInfoByIds(@Param("id")String id);

    PtUserEntity queryUserInfoByName(@Param("userName") String userName);
    PtUserEntity getByEmail(String email);

    List<PtMenuEntity> queryMenuListByLang(@Param("lang") String lang);

    void modifyUserInfoById(@Param("entity") PtUserEntity ptUserEntity);

    List<String> queryrivilegeList(@Param("userUuid")String userUuid);


    Integer queryUserInfoTotal(@Param("entity")SearchUserEntity entity);

    List<PtUserEntity> queryUserInfoListByPage(@Param("entity")SearchUserEntity entity,@Param("padding")PaddingEntity paddingEntity);


    List<ZtreeEntity> queryRoleTreeList();

    void delRoleByUserId(@Param("userUuid") String userUuid);

    void insertRoleRefUser(@Param("userUuid")String userUuid, @Param("roleUuid")String roleUuid);

    List<String> queryRoleByUserUuuid(@Param("userUuid")String userUuid);

    List<PtMenuEntity> queryMenuLevelListByUserUuid(@Param("userUuid")String userUuid,@Param("lang") String lang);

    void addUser(PtUserEntity ptUserEntity);
    int userUnlock(String userId);

    int userLock(String userId);

    int updateUserErrorInputCount(@Param("userId") String userId,@Param("num") int num);
    void createUser(PtUserEntity ptUserEntity);
    void insertUserInfo(@Param("entity")PtUserEntity ptUserEntity);

    void modifyUserInfo(@Param("entity")PtUserEntity ptUserEntity);

    void deleteUserById(@Param("userUuids") String[] userUuids);

    void allocatedepInfo(@Param("DePid")String DePid,@Param("userUuid")String userUuid);

    List<PtUserEntity> queryUserInfoByMail(@Param("email")String email);

    void modifyUserDepartment(@Param("entity")PtUserEntity ptUserEntity);

    void modifyUserOrgan(@Param("entity")PtUserEntity ptUserEntity);


    void allocateDuty(@Param("entity") UserDutyEntity userDutyEntity);

    List<PtDutyEntity> queryDutyInfo();
}
