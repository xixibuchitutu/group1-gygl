package com.yangzhengxi.user.service;


import com.yangzhengxi.login.entity.MenuLevelEntity;
import com.yangzhengxi.mybatis.entity.PtDutyEntity;
import com.yangzhengxi.mybatis.entity.PtMenuEntity;
import com.yangzhengxi.mybatis.entity.PtUserEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.user.entity.SearchUserEntity;
import com.yangzhengxi.user.entity.UserDutyEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.jdbc.UncategorizedSQLException;

import java.util.List;

public interface UserService {
    List<PtUserEntity> queryUserInfoListByName(SearchUserEntity search);

    void deleteUserInfoByIds(String ids) throws UncategorizedSQLException;

    PtUserEntity queryUserInfoByName(String username);

    List<PtMenuEntity> queryMenuList();

    List<MenuLevelEntity> queryMenuLevelList();

    void modifyUserInfoById(PtUserEntity ptUserEntity);

    List<String> queryrivilegeList(String userUuid);

    PadingRstType<PtUserEntity> queryUserInfoListByPage(SearchUserEntity entity, PaddingEntity paddingEntity);

    List<ZtreeEntity> queryRoleTreeList(String userUuid);

    void saveUserIdAndRoleId(String userUuid, String roleArray);

    List<MenuLevelEntity> queryMenuLevelList(String userUuid);

    void addUser(PtUserEntity ptUserEntity);

    void addUserInfo(PtUserEntity ptUserEntity);

    void modifyUserInfo(PtUserEntity ptUserEntity);

    void deleteUserById(String userUuidArray);

    void allocatedepInfo(String DePid,String userUuid);

    PtUserEntity queryUserInfoByMail(String email);

    void modifyUserDepartment(PtUserEntity ptUserEntity);

    void modifyUserOrgan(PtUserEntity ptUserEntity);

    void allocateDuty(UserDutyEntity userDutyEntity);

    List<PtDutyEntity> queryDutyInfo();
}
