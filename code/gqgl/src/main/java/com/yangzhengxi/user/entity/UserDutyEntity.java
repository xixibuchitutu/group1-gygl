package com.yangzhengxi.user.entity;

import java.io.Serializable;

/**
 * @Author yyds
 * @Date 2022/7/15
 * @ClassName:UserDutyEntity
 * @Version 1.0
 */
public class UserDutyEntity implements Serializable {
    private String userUuid;

    private String dutyId;

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    public String getDutyId() {
        return dutyId;
    }

    public void setDutyId(String dutyId) {
        this.dutyId = dutyId;
    }

    @Override
    public String toString() {
        return "UserDutyEntity{" +
                "userUuid='" + userUuid + '\'' +
                ", dutyId='" + dutyId + '\'' +
                '}';
    }
}
