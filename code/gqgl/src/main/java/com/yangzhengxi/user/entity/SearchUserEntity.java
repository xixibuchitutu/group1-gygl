package com.yangzhengxi.user.entity;

import java.io.Serializable;

public class SearchUserEntity implements Serializable {
    private String userName;
    private String roleUuid;
    private String mobile;

    private String dutyId;
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getRoleUuid() {
        return roleUuid;
    }
    public String getMobile() {
        return mobile;
    }
    public void setRoleUuid(String roleUuid) {
        this.roleUuid = roleUuid;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDutyId() {
        return dutyId;
    }

    public void setDutyId(String dutyId) {
        this.dutyId = dutyId;
    }

    @Override
    public String toString() {
        return "SearchUserEntity{" +
                "userName='" + userName + '\'' +
                ", roleUuid='" + roleUuid + '\'' +
                ", mobile='" + mobile + '\'' +
                ", dutyId='" + dutyId + '\'' +
                '}';
    }
}