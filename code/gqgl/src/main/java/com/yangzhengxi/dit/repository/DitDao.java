package com.yangzhengxi.dit.repository;

import com.yangzhengxi.mybatis.entity.PtDictionaryEntity;
import com.yangzhengxi.mybatis.entity.PtDictionaryI18nEntity;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DitDao {
    List<ZtreeEntity> queryDitLeftTree();
    PtDictionaryEntity queryDictionaryListById(@Param("dictionaryId") String dictionaryId);

    List<PtDictionaryI18nEntity> getI18nByDictionaryId(String dictionaryId);

    void deleteDictionaryI18nById(@Param("dictId")String dictId,@Param("languageId") String languageId);

    void insertDictionaryI18n(@Param("entity")PtDictionaryI18nEntity PtDictionaryI18nEntity);

    void updateDictionaryI18n(@Param("entity")PtDictionaryI18nEntity PtDictionaryI18nEntity);

    List<PtDictionaryI18nEntity> queryDictionaryI18n(@Param("entity")PtDictionaryI18nEntity PtDictionaryI18nEntity);

    void addDitInfo(@Param("entity")PtDictionaryEntity ptDictionaryEntity);

    void updateDitById(@Param("entity")PtDictionaryEntity ptDictionaryEntity);

    void DeleteDitById(@Param("dictId")String dictId);
}
