package com.yangzhengxi.dit.service;

import com.yangzhengxi.mybatis.entity.PtDictionaryEntity;
import com.yangzhengxi.mybatis.entity.PtDictionaryI18nEntity;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;

import java.util.List;

public interface DitService {
    List<ZtreeEntity> queryDitLeftTree();

    PtDictionaryEntity querydictionaryListById(String dictionaryId);

    List<PtDictionaryI18nEntity> getI18nByDictionaryId(String dictionaryId);

    void deleteDictionaryI18nById(String dictId, String languageId);

    void addDictionaryI18n(PtDictionaryI18nEntity PtDictionaryI18nEntity);

    void addDitInfo(PtDictionaryEntity ptDictionaryEntity);

    void updateDitById(PtDictionaryEntity ptDictionaryEntity);

    void DeleteDitById(String dictId);
}
