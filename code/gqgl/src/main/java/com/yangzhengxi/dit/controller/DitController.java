package com.yangzhengxi.dit.controller;

import com.yangzhengxi.dit.service.DitService;
import com.yangzhengxi.mybatis.entity.PtDictionaryEntity;
import com.yangzhengxi.mybatis.entity.PtDictionaryI18nEntity;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.spring.controller.BaseController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/dit")
public class DitController extends BaseController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private DitService ditService;
    @RequestMapping("/ditPage")
    private String ditPage(){
        return "dit/ditPage";
    }
    @RequestMapping("/queryDitLeftTree")
    @ResponseBody
    private List<ZtreeEntity>queryDitLeftTree(){

        return ditService.queryDitLeftTree();
    }
    @RequestMapping("/queryDictionaryListById")
    @ResponseBody
    public PtDictionaryEntity queryDictionaryListById(@RequestParam("dictionaryId") String dictionaryId){
        logger.info("dictionaryId:" + dictionaryId);
        PtDictionaryEntity ptDictionaryEntity = ditService.querydictionaryListById(dictionaryId);
        return ptDictionaryEntity;
    }
    @RequestMapping("/getI18nByDictionaryId")
    @ResponseBody
    public List<PtDictionaryI18nEntity> getI18nByDictionaryId(@RequestParam("dictionaryId")String dictionaryId){
        logger.info("dictionaryId:" + dictionaryId );
        List<PtDictionaryI18nEntity> list = ditService.getI18nByDictionaryId(dictionaryId);
        return list;
    }
    @RequestMapping("/deleteDictionaryI18nById")
    @ResponseBody
    public String deleteDictionaryI18nById(@RequestParam("dictId")String dictId,@RequestParam("languageId")String languageId){
        logger.info("dictId:" + dictId + " languageId:" + languageId);

        ditService.deleteDictionaryI18nById(dictId,languageId);
        return jsonSuccess("dictionary.delete.success");
    }
    @RequestMapping("/saveDictionaryI18n")
    @ResponseBody
    public String saveDictionaryI18n(PtDictionaryI18nEntity PtDictionaryI18nEntity){
        logger.info(PtDictionaryI18nEntity);

        ditService.addDictionaryI18n(PtDictionaryI18nEntity);
        return jsonSuccess("dictionary.add.success");
    }
    @RequestMapping("/addDitInfo")
    @ResponseBody
    public String addOrganInfo(PtDictionaryEntity PtDictionaryEntity){
        logger.info(PtDictionaryEntity);
        ditService.addDitInfo(PtDictionaryEntity);
        return jsonSuccess("dictionary.add.success");
    }
    @RequestMapping("/updateDitById")
    @ResponseBody
    public String updateDitById(PtDictionaryEntity PtDictionaryEntity){
        logger.info(PtDictionaryEntity);
        ditService.updateDitById(PtDictionaryEntity);
        return jsonSuccess("dictionary.add.success");
    }
    @RequestMapping("/DeleteDitById")
    @ResponseBody
    public String DeleteDitById(@RequestParam("dictId")String dictId){
        logger.info(dictId);
        ditService.DeleteDitById(dictId);
        return jsonSuccess("dictionary.delete.success");
    }
}
