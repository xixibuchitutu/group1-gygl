package com.yangzhengxi.dit.service.impl;


import com.yangzhengxi.dit.repository.DitDao;
import com.yangzhengxi.dit.service.DitService;
import com.yangzhengxi.mybatis.entity.PtDictionaryEntity;
import com.yangzhengxi.mybatis.entity.PtDictionaryI18nEntity;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.spring.controller.BaseController;
import com.yangzhengxi.spring.util.DataUitl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class DitServiceImpl extends BaseController implements DitService {
    @Resource
    private DitDao DitDao;

    @Override
    public List<ZtreeEntity> queryDitLeftTree() {
        List<ZtreeEntity> list = DitDao.queryDitLeftTree();
        list.add(getNoBoundNode());
        return list;

    }
    @Override
    public PtDictionaryEntity querydictionaryListById(String dictionaryId) {
        return DitDao.queryDictionaryListById(dictionaryId);
    }

    @Override
    public List<PtDictionaryI18nEntity> getI18nByDictionaryId(String dictionaryId) {
        return DitDao.getI18nByDictionaryId(dictionaryId);
    }

    @Override
    public void deleteDictionaryI18nById(String dictId, String languageId) {
        DitDao.deleteDictionaryI18nById(dictId,languageId);
    }

    @Override
    public void addDictionaryI18n(PtDictionaryI18nEntity PtDictionaryI18nEntity) {
        List<PtDictionaryI18nEntity> list = DitDao.queryDictionaryI18n(PtDictionaryI18nEntity);
        if(list != null && list.size() > 0){
            DitDao.updateDictionaryI18n(PtDictionaryI18nEntity);
        }else {
            DitDao.insertDictionaryI18n(PtDictionaryI18nEntity);
        }
    }

    @Override
    public void addDitInfo(PtDictionaryEntity ptDictionaryEntity) {
        //ptDictionaryEntity.setDictId(DataUitl.getUuid1());
        DitDao.addDitInfo(ptDictionaryEntity);
    }

    @Override
    public void updateDitById(PtDictionaryEntity ptDictionaryEntity) {
        DitDao.updateDitById(ptDictionaryEntity);

    }

    @Override
    public void DeleteDitById(String dictId) {
        DitDao.DeleteDitById(dictId);
    }
}
