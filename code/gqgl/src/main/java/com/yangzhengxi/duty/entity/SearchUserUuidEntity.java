package com.yangzhengxi.duty.entity;

import java.io.Serializable;

/**
 * @Author yyds
 * @Date 2022/6/29
 * @ClassName:SearchDutyEntity
 * @Version 1.0
 */
public class SearchUserUuidEntity implements Serializable {

    private String userUuid;

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    @Override
    public String toString() {
        return "SearchUserUuidEntity{" +
                "userUuid='" + userUuid + '\'' +
                '}';
    }
}
