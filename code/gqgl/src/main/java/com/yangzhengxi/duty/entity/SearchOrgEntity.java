package com.yangzhengxi.duty.entity;

import java.io.Serializable;


public  class SearchOrgEntity implements Serializable {
    private String dutyId;

    private String depUuid;

    public String getDutyId() {
        return dutyId;
    }

    public void setDutyId(String dutyId) {
        this.dutyId = dutyId;
    }

    public String getDepUuid() {
        return depUuid;
    }

    public void setDepUuid(String depUuid) {
        this.depUuid = depUuid;
    }

    @Override
    public String toString() {
        return "SearchOrgEntity{" +
                "dutyId='" + dutyId + '\'' +
                ", depUuid='" + depUuid + '\'' +
                '}';
    }
}

