package com.yangzhengxi.duty.service;

import com.yangzhengxi.duty.entity.SearchDepEntity;
import com.yangzhengxi.duty.entity.SearchEntity;
import com.yangzhengxi.duty.entity.SearchOrgEntity;
import com.yangzhengxi.duty.entity.SearchUserUuidEntity;
import com.yangzhengxi.mybatis.entity.PtDepartEntity;
import com.yangzhengxi.mybatis.entity.PtDutyEntity;
import com.yangzhengxi.mybatis.entity.PtOrganEntity;
import com.yangzhengxi.mybatis.entity.PtRoleEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author yyds
 * @Date 2022/6/29
 * @ClassName:OrganizationService
 * @Version 1.0
 */
public interface DutyService {
    List<ZtreeEntity> queryLeftTree();
    PadingRstType<PtDutyEntity> queryDutyInfoListByPage(SearchEntity search, PaddingEntity padding);
    PadingRstType<PtDutyEntity> queryDutyListById(String organUuid, PaddingEntity padding);
    void addDutyInfo(PtDutyEntity ptDutyEntity);
    void modifyDutyInfo(PtDutyEntity ptDutyEntity);
    void deleteDutyById(String dutyIdArray);

    List<SearchUserUuidEntity> whetherDelete(String dutyIdArray);

    void modifyDepartment(SearchDepEntity searchDepEntity);
    void modifyOrgan(SearchOrgEntity searchOrgEntity);

    List<PtRoleEntity> queryRoleInfo();
    List<PtOrganEntity> queryOrganInfo();
    List<PtDepartEntity> queryDepartInfo();
}