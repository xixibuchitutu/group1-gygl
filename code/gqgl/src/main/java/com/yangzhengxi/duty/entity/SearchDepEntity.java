package com.yangzhengxi.duty.entity;

import java.io.Serializable;

/**
 * @Author yyds
 * @Date 2022/7/11
 * @ClassName:SearchDepEntity
 * @Version 1.0
 */
public class SearchDepEntity implements Serializable {
    private String dutyId;

    private String depUuid;

    public String getDutyId() {
        return dutyId;
    }

    public void setDutyId(String dutyId) {
        this.dutyId = dutyId;
    }

    public String getDepUuid() {
        return depUuid;
    }

    public void setDepUuid(String depUuid) {
        this.depUuid = depUuid;
    }

    @Override
    public String toString() {
        return "SearchDepEntity{" +
                "dutyId='" + dutyId + '\'' +
                ", depUuid='" + depUuid + '\'' +
                '}';
    }
}
