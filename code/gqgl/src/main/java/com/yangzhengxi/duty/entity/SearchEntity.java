package com.yangzhengxi.duty.entity;

import com.yangzhengxi.mybatis.annotation.Columns;

import java.io.Serializable;

/**
 * @Author yyds
 * @Date 2022/6/30
 * @ClassName:SearchEntity
 * @Version 1.0
 */
public class SearchEntity implements Serializable {
    @Columns("DUTYID")
    private String dutyId;

    @Columns("NAME")
    private String name;

    private String organName;
    private String roleName;

    public String getDutyId() {
        return dutyId;
    }

    public void setDutyId(String dutyId) {
        this.dutyId = dutyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganName() {
        return organName;
    }

    public void setOrganName(String organName) {
        this.organName = organName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Override
    public String toString() {
        return "SearchEntity{" +
                "dutyId='" + dutyId + '\'' +
                ", name='" + name + '\'' +
                ", organName='" + organName + '\'' +
                ", roleName='" + roleName + '\'' +
                '}';
    }
}