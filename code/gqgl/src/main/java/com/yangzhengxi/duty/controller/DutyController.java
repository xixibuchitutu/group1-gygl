package com.yangzhengxi.duty.controller;

import com.yangzhengxi.duty.entity.SearchDepEntity;
import com.yangzhengxi.duty.entity.SearchEntity;
import com.yangzhengxi.duty.entity.SearchOrgEntity;
import com.yangzhengxi.duty.entity.SearchUserUuidEntity;
import com.yangzhengxi.duty.service.DutyService;
import com.yangzhengxi.mybatis.entity.PtDutyEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.spring.controller.BaseController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Author yyds
 * @Date 2022/6/29
 * @ClassName:OrganizationCOntroller
 * @Version 1.0
 */
@Controller
@RequestMapping("/duty")
public class DutyController extends BaseController {

    private Logger logger = LogManager.getLogger(this.getClass());

    @Resource
    private DutyService dutyService;

    @Secured("ROLE_gqgy_data_post")
    @RequestMapping("/dutyPage")
    public String dutyPage(HttpSession session) {
        session.setAttribute("list1",dutyService.queryRoleInfo());
        session.setAttribute("list2",dutyService.queryOrganInfo());
        session.setAttribute("list3",dutyService.queryDepartInfo());
        return "duty/dutyPage";
    }

    @RequestMapping("/dutyPageDetail")
    public String dutyPageDetail(@RequestParam("dutyId") String dutyId, HttpServletRequest request){
        logger.info("dutyId:" + dutyId);
        request.getSession().setAttribute("dutyId",dutyId);
        return "duty/dutyPageDetail";
    }

    @RequestMapping("/queryLeftTree")
    @ResponseBody
    public List<ZtreeEntity> queryLeftTree(){
        return dutyService.queryLeftTree();
    }

    @RequestMapping("/queryDutyListById")
    @ResponseBody
    public PadingRstType<PtDutyEntity> queryDutyListById(@RequestParam(value="ORGAN_UUID")String organUuid, PaddingEntity padding){
        logger.info(organUuid);
        PadingRstType<PtDutyEntity> rstType = dutyService.queryDutyListById(organUuid,padding);
        return rstType;
    }

    @RequestMapping("/queryDutyInfoListByPage")
    @ResponseBody
    public PadingRstType<PtDutyEntity> queryDutyInfoListByPage(SearchEntity search, PaddingEntity padding){
        logger.info(search);
        logger.info(padding);
        PadingRstType<PtDutyEntity> padingRstType = dutyService.queryDutyInfoListByPage(search,padding);

        return padingRstType;
    }

    @RequestMapping("/addDutyInfo")
    @ResponseBody
    public String addDutyInfo(PtDutyEntity ptDutyEntity){
        logger.info(ptDutyEntity);
        dutyService.addDutyInfo(ptDutyEntity);
        return jsonSuccess("duty.add.success");
    }

    @RequestMapping("/modifyDutyInfo")
    @ResponseBody
    public String modifyDutyInfo(PtDutyEntity ptDutyEntity){
        logger.info(ptDutyEntity);
        dutyService.modifyDutyInfo(ptDutyEntity);
        return jsonSuccess("duty.modify.success");
    }

    @RequestMapping("/deleteDutyById")
    @ResponseBody
    public String deleteDutyById(@RequestParam("dutyIdArray") String dutyIdArray){
        logger.info("dutyIdArray:" + dutyIdArray);
        List<SearchUserUuidEntity> list = dutyService.whetherDelete(dutyIdArray);
        logger.info(list);
        if (list.isEmpty()) {
            dutyService.deleteDutyById(dutyIdArray);
            return jsonSuccess("duty.delete.success");
        }
        return jsonFail("duty.delete.fail");
    }

    @RequestMapping("/modifyDepartment")
    @ResponseBody
    public String modifyDepartment(SearchDepEntity searchDepEntity) {
        logger.info(searchDepEntity);
        dutyService.modifyDepartment(searchDepEntity);
        return jsonSuccess("department.modify.success");
    }
    @RequestMapping("/modifyOrgan")
    @ResponseBody
    public String modifyOrgan(SearchOrgEntity searchOrgEntity) {
        logger.info(searchOrgEntity);
        dutyService.modifyOrgan(searchOrgEntity);
        return jsonSuccess("organ.modify.success");
    }
}