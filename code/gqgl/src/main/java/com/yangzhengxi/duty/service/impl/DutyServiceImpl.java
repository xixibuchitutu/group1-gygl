package com.yangzhengxi.duty.service.impl;

import com.yangzhengxi.duty.entity.SearchDepEntity;
import com.yangzhengxi.duty.entity.SearchEntity;
import com.yangzhengxi.duty.entity.SearchOrgEntity;
import com.yangzhengxi.duty.entity.SearchUserUuidEntity;
import com.yangzhengxi.duty.repository.DutyDao;
import com.yangzhengxi.duty.service.DutyService;
import com.yangzhengxi.mybatis.entity.PtDepartEntity;
import com.yangzhengxi.mybatis.entity.PtDutyEntity;
import com.yangzhengxi.mybatis.entity.PtOrganEntity;
import com.yangzhengxi.mybatis.entity.PtRoleEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.spring.controller.BaseController;
import com.yangzhengxi.spring.util.DataUitl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author yyds
 * @Date 2022/6/29
 * @ClassName:OrganizationServiceImpl
 * @Version 1.0
 */
@Service
public class DutyServiceImpl extends BaseController implements DutyService {
    @Resource
    private DutyDao dutyDao;


    @Override
    public List<ZtreeEntity> queryLeftTree() {
        return dutyDao.queryLeftTree();
    }

    @Override
    public PadingRstType<PtDutyEntity> queryDutyInfoListByPage(SearchEntity search, PaddingEntity padding) {
        padding.deal(PtDutyEntity.class);
        PadingRstType<PtDutyEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(padding.getPage());
        List<PtDutyEntity> list = dutyDao.queryDutyInfoListByPage(search,padding);
        padingRstType.setRawRecords(list);
        Integer total = dutyDao.queryDutyInfoTotal(search);
        padingRstType.setTotal(total);
        padingRstType.putItems();

        return padingRstType;
    }

    @Override
    public  PadingRstType<PtDutyEntity> queryDutyListById(String organUuid, PaddingEntity padding){
        padding.deal(PtDutyEntity.class);
        PadingRstType<PtDutyEntity> rstType = new PadingRstType<>();
        rstType.setPage(padding.getPage());
        List<PtDutyEntity> list = dutyDao.queryDutyListById(organUuid,padding);
        rstType.setRawRecords(list);
        Integer total = dutyDao.queryDutyListTotal(organUuid);
        rstType.setTotal(total);
        rstType.putItems();

        return rstType;
    }

    @Override
    public void addDutyInfo(PtDutyEntity ptDutyEntity) {
        ptDutyEntity.setDutyId(DataUitl.getUuid());
        dutyDao.insertDutyInfo(ptDutyEntity);
    }

    @Override
    public void modifyDutyInfo(PtDutyEntity ptDutyEntity) {
        dutyDao.modifyDutyInfo(ptDutyEntity);
    }

    @Override
    public void deleteDutyById(String dutyIdArray) {
        String[] dutyId = dutyIdArray.split(",");
        dutyDao.deleteDutyById(dutyId);
    }

    @Override
    public List<SearchUserUuidEntity> whetherDelete(String dutyIdArray) {
        String[] dutyId = dutyIdArray.split(",");
        return dutyDao.whetherDelete(dutyId);
    }

    @Override
    public void modifyDepartment(SearchDepEntity searchDepEntity) {
        dutyDao.modifyDepartment(searchDepEntity);
    }

    @Override
    public void modifyOrgan(SearchOrgEntity searchOrgEntity) {
        dutyDao.modifyOrgan(searchOrgEntity);
    }


    @Override
    public List<PtRoleEntity> queryRoleInfo() {
        return dutyDao.queryRoleInfo();
    }

    @Override
    public List<PtOrganEntity> queryOrganInfo() {
        return dutyDao.queryOrganInfo();
    }

    @Override
    public List<PtDepartEntity> queryDepartInfo() {
        return dutyDao.queryDepartInfo();
    }
}