package com.yangzhengxi.duty.repository;

import com.yangzhengxi.duty.entity.SearchDepEntity;
import com.yangzhengxi.duty.entity.SearchEntity;
import com.yangzhengxi.duty.entity.SearchOrgEntity;
import com.yangzhengxi.duty.entity.SearchUserUuidEntity;
import com.yangzhengxi.mybatis.entity.PtDepartEntity;
import com.yangzhengxi.mybatis.entity.PtDutyEntity;
import com.yangzhengxi.mybatis.entity.PtOrganEntity;
import com.yangzhengxi.mybatis.entity.PtRoleEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author yyds
 * @Date 2022/6/29
 * @ClassName:OrganizationDao
 * @Version 1.0
 */
@Mapper
public interface DutyDao {
    List<ZtreeEntity> queryLeftTree();
    List<PtDutyEntity> queryDutyInfoListByPage(@Param("search") SearchEntity search, @Param("padding") PaddingEntity padding);
    Integer queryDutyInfoTotal(@Param("search")SearchEntity search);
    List<PtDutyEntity> queryDutyListById(@Param("ORGAN_UUID")String organUuid, @Param("padding") PaddingEntity padding);
    Integer queryDutyListTotal(String organUuid);
    void insertDutyInfo(@Param("entity") PtDutyEntity ptDutyEntity);
    void modifyDutyInfo(@Param("entity")PtDutyEntity ptDutyEntity);
    void deleteDutyById(@Param("dutyId") String[] dutyId);

    List<SearchUserUuidEntity> whetherDelete(@Param("dutyId")String[] dutyId);

    void modifyDepartment(@Param("entity")SearchDepEntity searchDepEntity);
    void modifyOrgan(@Param("entity") SearchOrgEntity searchOrgEntity);

    /*下拉框*/
    List<PtRoleEntity> queryRoleInfo();
    List<PtOrganEntity> queryOrganInfo();
    List<PtDepartEntity> queryDepartInfo();
}