package com.yangzhengxi.menu.service.impl;

import com.yangzhengxi.menu.respository.MenuDao;
import com.yangzhengxi.menu.service.MenuService;
import com.yangzhengxi.mybatis.entity.PtMenuEntity;
import com.yangzhengxi.mybatis.entity.PtMenuI18n;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.spring.controller.BaseController;
import com.yangzhengxi.spring.tag.entity.DictTagEntity;
import com.yangzhengxi.spring.util.DataUitl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuServiceImpl extends BaseController implements MenuService {
    @Autowired
    private MenuDao menuDao;
    @Value("${unbound.id}")
    private String unboundId;

    @Override
    public List<ZtreeEntity> queryLeftTree() {
        List<ZtreeEntity> list = menuDao.queryLeftTree();
        list.add(getNoBoundNode());
        return list;
    }

    @Override
    public PtMenuEntity queryMenuListById(String menuId) {
        return menuDao.queryMenuListById(menuId);
    }

    @Override
    public List<DictTagEntity> queryMenuList() {
        return menuDao.queryMenuList();
    }

    @Override
    public PadingRstType<PtMenuEntity> queryMenuInfoListById(String menuId, PaddingEntity padding) {
        padding.deal(PtMenuEntity.class);
        PadingRstType<PtMenuEntity> rstType = new PadingRstType<>();
        rstType.setPage(padding.getPage());
        List<PtMenuEntity> list = menuDao.queryMenuInfoListPaddingById(menuId, padding);
        rstType.setRawRecords(list);
        Integer total = menuDao.queryMenuInfoListTotal(menuId);
        ;
        rstType.setTotal(total);
        rstType.putItems();

        return rstType;
    }
    @Override
    public List<PtMenuI18n> getI18nByMenuId(String menuId) {

        return  menuDao.getI18nByMenuId(menuId);
    }

    @Override
    public void deleteMenuI18nById(String menuId, String langId) {
        menuDao.deleteMenuI18nById(menuId,langId);
    }

    @Override
    public void saveMenuI18n(PtMenuI18n ptMenuI18n) {
        List<PtMenuI18n> list = menuDao.queryMenuI18n(ptMenuI18n);
        if(list != null && list.size() > 0){
            menuDao.updateMenuI18n(ptMenuI18n);
        }else {
            menuDao.insertMenuI18n(ptMenuI18n);
        }

    }
    @Override
    public List<ZtreeEntity> queryBoundMenuTree(String menuId) {
        List<ZtreeEntity> list = menuDao.queryBoundMenuTree();
        String parentId = menuDao.queryParentId(menuId);
        for(ZtreeEntity item:list){
            if(parentId.equals(item.getId())){
                item.setChecked(true);
                break;
            }
        }
        return list;
    }

    @Override
    public void saveParentIdByMenuId(String menuId, String parentId) {
        menuDao.saveParentIdByMenuId(menuId,parentId);
    }
    @Override
    public void unboundParentIdByMenuId(String menuId) {
        menuDao.saveParentIdByMenuId(menuId,unboundId);
    }

    @Override
    public void modifyMenuInfoById(PtMenuEntity ptMenuEntity) {
        menuDao.modifyMenuInfoById(ptMenuEntity);
    }

    @Override
    public void addMenuInfoById(PtMenuEntity ptMenuEntity) {
        ptMenuEntity.setMenuId(DataUitl.getUuid());
        ptMenuEntity.setParentId(unboundId);
        menuDao.addMenuInfoById(ptMenuEntity);
    }

    @Override
    public boolean deleteMenuById(String menuId) {
        int integer = menuDao.querySubmenu(menuId);
        if(integer >0){
            return false;
        }
        menuDao.deleteMenuById(menuId);
        menuDao.deleteMenuI18nByMenuId(menuId);
        return true;
    }
}