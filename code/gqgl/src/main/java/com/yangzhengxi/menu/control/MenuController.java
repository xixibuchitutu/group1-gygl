package com.yangzhengxi.menu.control;
import com.yangzhengxi.menu.service.MenuService;
import com.yangzhengxi.mybatis.entity.PtMenuEntity;
import com.yangzhengxi.mybatis.entity.PtMenuI18n;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.spring.controller.BaseController;
import com.yangzhengxi.spring.tag.entity.DictTagEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/menu")
public class MenuController extends BaseController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private MenuService menuService;

    @RequestMapping("/menuPage")
    public String menuPage(HttpServletRequest request) {

        List<DictTagEntity> menuList = menuService.queryMenuList();
        request.getSession().setAttribute("menuList", menuList);
        return "menu/menuPage";

    }

    @RequestMapping("/queryLeftTree")
    @ResponseBody
    public List<ZtreeEntity> queryLeftTree() {
        return menuService.queryLeftTree();
    }

    @RequestMapping("/queryMenuListById")
    @ResponseBody
    public PtMenuEntity queryMenuListById(@RequestParam("menuId") String menuId) {
        logger.info("menuId:" + menuId);
        PtMenuEntity ptMenuEntity = menuService.queryMenuListById(menuId);
        return ptMenuEntity;
    }

    @RequestMapping("/queryMenuInfoListById")
    @ResponseBody
    public PadingRstType<PtMenuEntity> queryMenuInfoListById(@RequestParam(value = "menuId") String menuId, PaddingEntity padding) {
        logger.info("menuId:" + menuId + padding);
        PadingRstType<PtMenuEntity> rstType = menuService.queryMenuInfoListById(menuId, padding);
        return rstType;

    }

    @RequestMapping("/getI18nByMenuId")
    @ResponseBody
    public List<PtMenuI18n> getI18nByMenuId(@RequestParam("menuId") String menuId) {
        logger.info("menuId:" + menuId);
        List<PtMenuI18n> list = menuService.getI18nByMenuId(menuId);
        return list;
    }

    @RequestMapping("/deleteMenuI18nById")
    @ResponseBody
    public String deleteMenuI18nById(@RequestParam("menuId") String menuId, @RequestParam("langId") String langId) {
        logger.info("menuId:" + menuId + " langId:" + langId);

        menuService.deleteMenuI18nById(menuId, langId);
        return jsonSuccess("menu.delete.i18n.success");
    }

    @RequestMapping("/saveMenuI18n")
    @ResponseBody
    public String saveMenuI18n(PtMenuI18n ptMenuI18n) {
        logger.info(ptMenuI18n);

        menuService.saveMenuI18n(ptMenuI18n);
        return jsonSuccess("menu.save.i18n.success");
    }

    @RequestMapping("/queryBoundMenuTree")
    @ResponseBody
    public List<ZtreeEntity> queryBoundMenuTree(@RequestParam("menuId") String menuId) {
        logger.info("menuId:" + menuId);
        List<ZtreeEntity> list = menuService.queryBoundMenuTree(menuId);
        return list;
    }

    @RequestMapping("/saveParentIdByMenuId")
    @ResponseBody
    public String saveParentIdByMenuId(@RequestParam("menuId") String menuId, @RequestParam("parentId") String parentId) {
        logger.info("menuId:" + menuId + " parentId:" + parentId);
        menuService.saveParentIdByMenuId(menuId, parentId);
        return jsonSuccess("menu.bound.parent.success");

    }

    @RequestMapping("/unboundParentIdByMenuId")
    @ResponseBody
    public String unboundParentIdByMenuId(@RequestParam("menuId") String menuId) {
        logger.info("menuId:" + menuId);
        menuService.unboundParentIdByMenuId(menuId);
        return jsonSuccess("menu.unbound.parent.success");

    }

    @Secured("ROLE_gqgy_data_menu")
    @RequestMapping("/modifyMenuInfoById")
    @ResponseBody
    public String modifyMenuInfoById(PtMenuEntity ptMenuEntity) {
        logger.info(ptMenuEntity);
        menuService.modifyMenuInfoById(ptMenuEntity);
        return jsonSuccess("menu.modify.success");
    }
    @Secured("ROLE_gqgy_data_menu")
    @RequestMapping("/addMenuInfoById")
    @ResponseBody
    public String addMenuInfoById(PtMenuEntity ptMenuEntity){
        logger.info( ptMenuEntity);
        menuService.addMenuInfoById(ptMenuEntity);
        return jsonSuccess("menu.add.success");
    }
    @RequestMapping("/deleteMenuById")
    @ResponseBody
    public String deleteMenuById(@RequestParam("menuId") String menuId) {
        logger.info("menuId:" + menuId );
        boolean b = menuService.deleteMenuById(menuId);
        String str = b?"menu.delete.success":"menu.delete.error";
        return jsonSuccess(str);
    }

}