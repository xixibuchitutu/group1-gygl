package com.yangzhengxi.menu.service;


import com.yangzhengxi.mybatis.entity.PtMenuEntity;
import com.yangzhengxi.mybatis.entity.PtMenuI18n;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.spring.tag.entity.DictTagEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MenuService {
    List<ZtreeEntity> queryLeftTree();

    PtMenuEntity queryMenuListById(String menuId);

    List<DictTagEntity> queryMenuList();

    PadingRstType<PtMenuEntity> queryMenuInfoListById(String menuId, PaddingEntity padding);

    List<PtMenuI18n> getI18nByMenuId(String menuId);

    void deleteMenuI18nById(String menuId, String langId);

    void saveMenuI18n(PtMenuI18n ptMenuI18n);

    List<ZtreeEntity> queryBoundMenuTree(String menuId);

    void saveParentIdByMenuId(String menuId, String parentId);

    void unboundParentIdByMenuId(String menuId);

    void modifyMenuInfoById(PtMenuEntity ptMenuEntity);

    void addMenuInfoById(PtMenuEntity ptMenuEntity);

    boolean deleteMenuById(String menuId);
}
