package com.yangzhengxi.menu.respository;

import com.yangzhengxi.mybatis.entity.PtMenuEntity;
import com.yangzhengxi.mybatis.entity.PtMenuI18n;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.spring.tag.entity.DictTagEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MenuDao {
    List<ZtreeEntity> queryLeftTree();

    PtMenuEntity queryMenuListById(@Param("menuId") String menuId);

    List<DictTagEntity> queryMenuList();

    List<PtMenuEntity> queryMenuInfoListPaddingById(@Param("menuId")String menuId,@Param("padding") PaddingEntity padding);

    Integer queryMenuInfoListTotal(@Param("menuId")String menuId);

    List<PtMenuI18n> getI18nByMenuId(@Param("menuId")String menuId);

    void deleteMenuI18nById(@Param("menuId")String menuId,@Param("langId") String langId);

    void insertMenuI18n(@Param("entity") PtMenuI18n ptMenuI18n);

    List<PtMenuI18n> queryMenuI18n(@Param("entity")PtMenuI18n ptMenuI18n);

    void updateMenuI18n(@Param("entity")PtMenuI18n ptMenuI18n);

    List<ZtreeEntity> queryBoundMenuTree();

    String queryParentId(@Param("menuId") String menuId);

    void saveParentIdByMenuId(@Param("menuId")String menuId,@Param("parentId") String parentId);

    void modifyMenuInfoById(@Param("entity") PtMenuEntity ptMenuEntity);

    void addMenuInfoById(@Param("entity")PtMenuEntity ptMenuEntity);

    void deleteMenuById(@Param("menuId") String menuId);

    void deleteMenuI18nByMenuId(@Param("menuId") String menuId);

    int querySubmenu(@Param("menuId") String menuId);
}