package com.yangzhengxi.Listener;


import com.yangzhengxi.mybatis.entity.PtUserEntity;
import com.yangzhengxi.user.repository.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;

@Component
public class AuthenticationFailureListener implements ApplicationListener<AuthenticationFailureBadCredentialsEvent> {
    @Autowired
    private UserDao userDao;

    @Override
    public void onApplicationEvent(AuthenticationFailureBadCredentialsEvent authenticationFailureBadCredentialsEvent) {
        String username = authenticationFailureBadCredentialsEvent.getAuthentication().getPrincipal().toString();
        PtUserEntity ptUserEntity = userDao.queryUserInfoByName(username);
        if (ptUserEntity != null) {
            //输入错误是否在30分钟内
            Date lastInputError = ptUserEntity.getLastInputError();
            if (lastInputError != null) {

                //最后输入错误时间+30分钟如果大于当前时间则代表已超过30分钟
                Calendar instance = Calendar.getInstance();
                instance.setTime(lastInputError);
                instance.add(Calendar.MINUTE, 30);
                Date time = instance.getTime();
                if (time.after(new Date())) {
                    //未过期
                    // 用户失败次数
                    int fails = ptUserEntity.getInputErrorNum();
                    fails++;
                    if (fails >= 3) {
                        //超过3次锁定当前账户
                        userDao.userLock(ptUserEntity.getUserUuid());
                    } else {
                        int inputErrorNum = ptUserEntity.getInputErrorNum();
                        userDao.updateUserErrorInputCount(ptUserEntity.getUserUuid(), inputErrorNum + 1);
                    }
                }else{
                    //已过期,重置次数并重新加1
                    userDao.updateUserErrorInputCount(ptUserEntity.getUserUuid(),  1);
                }
            } else {
                //第一次
                userDao.updateUserErrorInputCount(ptUserEntity.getUserUuid(), 1);
            }

        }
    }
}