package com.yangzhengxi.Listener;

import com.yangzhengxi.login.entity.MenuLevelEntity;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;

@WebListener
public class OpOnlineUserSessionListener {

    public void sessionDestroyed(HttpSessionEvent event) {
        HttpSession session = event.getSession();
        //获取当前用户信息
        MenuLevelEntity params  = (MenuLevelEntity)session.getAttribute("userName");
        //移除在线用户
        if(params!= null && !params.getMenuName().isEmpty()){
            LoginOperateInfo loginInfo = new LoginOperateInfo();
            //为0代表不是登录状态
            loginInfo.setIsLogin(LoginStatusEnum.OFF_LINE);
            loginInfo.setSessionId(session.getId());
            //为2代表操作类型为登出
            loginInfo.setOperateType(LoginStatusEnum.LOGIN_OUT);
            loginInfo.setUserId(params.getUserId());
            loginInfo.setUserNo(params.getUsername());
            loginInfo.setUserName(params.getUserNameCN());
            int count = Integer.parseInt(updateIsLogin(session.getId()));
            if(count>0){
                insertLoginOperateInfo(loginInfo);
            }
        }
    }

    private void insertLoginOperateInfo(LoginOperateInfo loginInfo) {


    }

    private String updateIsLogin(String id) {
        return id;
    }

}
