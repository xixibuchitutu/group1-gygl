package com.yangzhengxi.log.service.impl;

import com.yangzhengxi.mybatis.entity.PtLogEntity;
import com.yangzhengxi.log.respository.LogDao;
import com.yangzhengxi.log.service.LogService;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class LogServiceImpl implements LogService {

    @Autowired
    private LogDao logDao;

    @Override
    public PadingRstType<PtLogEntity> getLogInfo(Map<String, Object> data) {
        List<PtLogEntity> ptLogEntities = logDao.getLogInfo(data);
        PadingRstType<PtLogEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(String.valueOf(1));
        padingRstType.setRawRecords(ptLogEntities);
        if(ptLogEntities != null) {
            padingRstType.setTotal(ptLogEntities.size());
        } else {
            padingRstType.setTotal(0);
        }
        padingRstType.putItems();
        return padingRstType;
    }

    @Override
    public void saveLogData_tran_new(PtLogEntity logEntity) {
        logDao.saveLogData(logEntity);
    }

    @Override
    public boolean delLoginInfo(List<String> logId) {
        int logs = logId.size();
        int sucDelNum = logDao.delLoginInfo(logId.toArray());
        return logs == sucDelNum;
    }
}
