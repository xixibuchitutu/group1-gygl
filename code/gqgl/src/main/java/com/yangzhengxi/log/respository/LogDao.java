package com.yangzhengxi.log.respository;

import com.yangzhengxi.mybatis.entity.PtLogEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface LogDao {
    List<PtLogEntity> getLogInfo(Map<String, Object> data);

    void saveLogData(PtLogEntity logEntity);

    int delLoginInfo(Object[] logId);
}
