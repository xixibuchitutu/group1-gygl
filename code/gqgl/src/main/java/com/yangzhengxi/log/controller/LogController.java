package com.yangzhengxi.log.controller;

import com.yangzhengxi.log.service.LogService;
import com.yangzhengxi.mybatis.entity.PtLogEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/log")
public class LogController {

    @Autowired
    private LogService logService;

    @RequestMapping("/logIndex")
    public String logIndex(){
        return "log/logIndex";
    }

    @ResponseBody
    @RequestMapping("/getLogs")
    public PadingRstType<PtLogEntity> getLogInfo(@RequestBody  Map<String, Object> data) {
        return logService.getLogInfo(data);
    }

    @ResponseBody
    @RequestMapping("/delLogs")
    public Map<String, Object> delLoginInfo(@RequestBody List<String> logIds) {
        Map<String, Object> result = new HashMap<>();
        result.put("statue", "-1"); //默认失败
        if(logIds != null && logService.delLoginInfo(logIds)) {
            result.put("state", "0");
            result.put("msg","删除成功！");
        } else {
            result.put("msg","删除失败，请联系管理员");
        }
        return result;
    }
}
