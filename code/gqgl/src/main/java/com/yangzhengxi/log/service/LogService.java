package com.yangzhengxi.log.service;

import com.yangzhengxi.mybatis.entity.PtLogEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;

import java.util.List;
import java.util.Map;

public interface LogService {
    PadingRstType<PtLogEntity> getLogInfo(Map<String, Object> data);

    void saveLogData_tran_new(PtLogEntity logEntity);

    boolean delLoginInfo(List<String> logId);
}
