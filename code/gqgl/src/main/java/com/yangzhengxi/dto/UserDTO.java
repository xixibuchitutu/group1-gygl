package com.yangzhengxi.dto;

import lombok.Data;

/**
 * @author TanChengjin
 * @version v1.0.0
 * @email 18865477815@163.com
 */
@Data
public class UserDTO {
    /**
     * 用户名
     */
    private String username;
    /**
     * 邮件地址
     */
    private String email;
    /**
     * 电话
     */
    private String mobile;
    /**
     * 真实姓名
     */
    private String niceName;
    /**
     * 用户密码
     */
    private String password;
    /**
     * 备注
     */
    private String remark;

    public static interface Register {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNiceName() {
        return niceName;
    }

    public void setNiceName(String niceName) {
        this.niceName = niceName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
