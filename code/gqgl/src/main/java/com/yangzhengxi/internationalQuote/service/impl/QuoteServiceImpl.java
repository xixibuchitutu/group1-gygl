package com.yangzhengxi.internationalQuote.service.impl;

import com.yangzhengxi.internationalQuote.entity.SearchQuoteEntity;
import com.yangzhengxi.internationalQuote.repository.QuoteDao;
import com.yangzhengxi.internationalQuote.service.QuoteService;
import com.yangzhengxi.mybatis.entity.InternationalQuoteEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.spring.controller.BaseController;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author yyds
 * @Date 2022/7/3
 * @ClassName:QuoteServiceImpl
 * @Version 1.0
 */
@Service
public class QuoteServiceImpl extends BaseController implements QuoteService {

    @Resource
    private QuoteDao quoteDao;

    @Override
    public PadingRstType<InternationalQuoteEntity> queryQuoteInfoListByPage(SearchQuoteEntity search, PaddingEntity padding) {
        padding.deal(InternationalQuoteEntity.class);
        PadingRstType<InternationalQuoteEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(padding.getPage());
        List<InternationalQuoteEntity> list = quoteDao.queryQuoteInfoListByPage(search,padding);
        padingRstType.setRawRecords(list);
        Integer total = quoteDao.queryQuoteInfoTotal(search);
        padingRstType.setTotal(total);
        padingRstType.putItems();

        return padingRstType;
    }
}
