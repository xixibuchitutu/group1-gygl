package com.yangzhengxi.internationalQuote.controller;

import com.yangzhengxi.internationalQuote.entity.SearchQuoteEntity;
import com.yangzhengxi.internationalQuote.service.QuoteService;
import com.yangzhengxi.mybatis.entity.InternationalQuoteEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.spring.controller.BaseController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 * @Author yyds
 * @Date 2022/7/3
 * @ClassName:quate
 * @Version 1.0
 */
@Controller
@RequestMapping("/quoteinfo")
public class QuoteController extends BaseController {

    private Logger logger = LogManager.getLogger(this.getClass());

    @Resource
    private QuoteService quoteService;

    @Secured("ROLE_gqgy_data_quote")
    @RequestMapping("/page")
    public String quotePage() {
        return "quoteinfo/page";
    }

    @RequestMapping("/queryQuoteInfoListByPage")
    @ResponseBody
    public PadingRstType<InternationalQuoteEntity> queryQuoteInfoListByPage(SearchQuoteEntity search, PaddingEntity padding){
        logger.info(search);
        logger.info(padding);
        PadingRstType<InternationalQuoteEntity> padingRstType = quoteService.queryQuoteInfoListByPage(search,padding);

        return padingRstType;
    }
}
