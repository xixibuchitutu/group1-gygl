package com.yangzhengxi.internationalQuote.entity;

import java.io.Serializable;

/**
 * @Author yyds
 * @Date 2022/7/3
 * @ClassName:SearchQuote
 * @Version 1.0
 */
public class SearchQuoteEntity implements Serializable {
    private String beginDate;

    private String endDate;

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "SearchQuoteEntity{" +
                "beginDate='" + beginDate + '\'' +
                ", endDate='" + endDate + '\'' +
                '}';
    }
}