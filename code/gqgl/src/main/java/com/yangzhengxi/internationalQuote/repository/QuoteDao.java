package com.yangzhengxi.internationalQuote.repository;

import com.yangzhengxi.internationalQuote.entity.SearchQuoteEntity;
import com.yangzhengxi.mybatis.entity.InternationalQuoteEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author yyds
 * @Date 2022/7/3
 * @ClassName:QuoteDao
 * @Version 1.0
 */
@Mapper
public interface QuoteDao {
    List<InternationalQuoteEntity> queryQuoteInfoListByPage(@Param("search") SearchQuoteEntity search, @Param("padding") PaddingEntity padding);

    Integer queryQuoteInfoTotal(@Param("search") SearchQuoteEntity search);
}
