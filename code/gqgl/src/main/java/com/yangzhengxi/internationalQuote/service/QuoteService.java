package com.yangzhengxi.internationalQuote.service;

import com.yangzhengxi.internationalQuote.entity.SearchQuoteEntity;
import com.yangzhengxi.mybatis.entity.InternationalQuoteEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;

/**
 * @Author yyds
 * @Date 2022/7/3
 * @ClassName:QuoteService
 * @Version 1.0
 */
public interface QuoteService {
    PadingRstType<InternationalQuoteEntity> queryQuoteInfoListByPage(SearchQuoteEntity search, PaddingEntity padding);
}
