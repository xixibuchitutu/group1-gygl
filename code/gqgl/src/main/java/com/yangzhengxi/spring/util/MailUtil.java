package com.yangzhengxi.spring.util;

import com.yangzhengxi.spring.entity.MailEntity;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

public final class MailUtil {

    public static void sendMail(MailEntity entity) throws EmailException{
        HtmlEmail memail = new HtmlEmail();
        memail.setHostName(entity.getMailHostName());
        memail.setCharset("utf-8");
        memail.addTo(entity.getMaiToUser());
        memail.setFrom(entity.getMailFromUser(), "HarmonyOSChart");
        memail.setAuthentication(entity.getMailFromUser(), entity.getMailFromPwd());
        memail.setSubject(entity.getMailSubject());
        memail.setMsg(entity.getMailContent());
        memail.send();
    }
}
