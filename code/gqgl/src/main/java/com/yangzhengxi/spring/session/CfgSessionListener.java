package com.yangzhengxi.spring.session;




import com.yangzhengxi.config.service.ConfigService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
//@WebListener
@Component
public class CfgSessionListener implements HttpSessionListener {
    private Logger logger =  LogManager.getLogger(getClass().getName());
    @Resource
    private ConfigService configService;
    @Override
    public void sessionCreated(HttpSessionEvent se) {
        logger.info("sessionCreated");
        HttpSession session = se.getSession();
//        ServletContext sc = session.getServletContext();
//        WebApplicationContext webApplicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(sc);
//        ConfigService configService = webApplicationContext.getBean("configService", ConfigService.class);
        configService.setConfigValue(session);


    }
}
