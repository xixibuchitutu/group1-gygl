package com.yangzhengxi.spring.util;


import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Random;
import java.util.UUID;

public final class DataUitl {

    public static String getUuid(){
        String uuid = UUID.randomUUID().toString();
        return uuid.replace("-","");
    }

    /**
     * 随机产生的字符串
     */
    private final static String RAND_STRING = "123456789";

    /**
     * 产生随机数
     */
    private final static Random RANDOM = new Random();
    /**
     * 获取随机的验证码
     * @param number  验证码的位数
     * @return
     */
    public static String getVfcCode(Integer number)
    {

        String randomString="";
        int randomIndex;
        char charAt;
        //根据字符串的长度产生随机下标
        for(int i = 0; i < number; i++){
            randomIndex = RANDOM.nextInt(RAND_STRING.length());
            charAt = RAND_STRING.charAt(randomIndex);
            randomString += String.valueOf(charAt);
        }
        return randomString;
    }

    // 字符数量
    private static final int SIZE = 4;
    // 干扰线数量
    private static final int LINES = 5;
    // 宽度
    private static final int WIDTH = 80;
    // 高度
    private static final int HEIGHT = 40;
    // 字体大小
    private static final int FONT_SIZE = 30;
    public static byte[] getVfcImg(String vfcCodeStr){
        // 1.创建空白图片
        BufferedImage image = new BufferedImage(
                WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        // 2.获取图片画笔
        Graphics graphic = image.getGraphics();
        // 3.设置画笔颜色
        graphic.setColor(Color.LIGHT_GRAY);
        // 4.绘制矩形背景
        graphic.fillRect(0, 0, WIDTH, HEIGHT);
        // 5.画随机字符
        Random ran = new Random();
        char chars;
        for (int i = 0; i <vfcCodeStr.length(); i++) {
            chars = vfcCodeStr.charAt(i);
            // 设置随机颜色
            graphic.setColor(getRandomColor());
            // 设置字体大小
            graphic.setFont(new Font(
                    null, Font.BOLD + Font.ITALIC, FONT_SIZE));
            // 画字符
            graphic.drawString(
                    chars + "", i * WIDTH / SIZE, HEIGHT*2/3);
        }
        // 6.画干扰线
        for (int i = 0; i < LINES; i++) {
            // 设置随机颜色
            graphic.setColor(getRandomColor());
            // 随机画线
            graphic.drawLine(ran.nextInt(WIDTH), ran.nextInt(HEIGHT),
                    ran.nextInt(WIDTH), ran.nextInt(HEIGHT));
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            ImageIO.write(image,"PNG",byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 随机取色
     */
    public static Color getRandomColor() {
        Random ran = new Random();
        Color color = new Color(ran.nextInt(256),
                ran.nextInt(256), ran.nextInt(256));
        return color;
    }



}
