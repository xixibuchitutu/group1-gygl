package com.yangzhengxi.spring.util;

import org.jasypt.util.text.BasicTextEncryptor;

public final class JasyptUtil {
    private JasyptUtil(){

    }
    public static String Encrypt(String plaintext,String textKey){
        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        textEncryptor.setPassword(textKey);
        return textEncryptor.encrypt(plaintext);
    }

    public static String Decrypt(String ciphertext, String textKey){
        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        textEncryptor.setPassword(textKey);
        return textEncryptor.decrypt(ciphertext);
    }

    public static void main(String[] args) {
        String rst = Encrypt("Huawei_123", "0123456789ABCDEF");

        System.out.println(rst);
        System.out.println(Decrypt(rst, "0123456789ABCDEF"));
    }

}
