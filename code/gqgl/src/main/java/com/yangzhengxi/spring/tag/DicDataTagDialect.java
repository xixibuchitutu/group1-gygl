package com.yangzhengxi.spring.tag;


import com.yangzhengxi.spring.tag.service.DicDataTagService;
import org.springframework.stereotype.Component;
import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.standard.StandardDialect;
import org.thymeleaf.standard.processor.StandardXmlNsTagProcessor;
import org.thymeleaf.templatemode.TemplateMode;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.Set;

@Component
public class DicDataTagDialect extends AbstractProcessorDialect {

    //定义方言名称
    private static final String DIALECT_NAME = "gqgy";
    private static final String GDMT_PREFIX="gqgy";

    public DicDataTagDialect() {
        super(DIALECT_NAME, GDMT_PREFIX, StandardDialect.PROCESSOR_PRECEDENCE);
    }
    @Resource
    private DicDataTagService dicDataTagService;
    @Override
    public Set<IProcessor> getProcessors(String dialectPrefix) {
        Set<IProcessor> processors = new HashSet<>();

        processors.add(new DataSelectProcessor(dialectPrefix));
        processors.add(new DicDataProcessor(dialectPrefix,dicDataTagService));
        processors.add(new StandardXmlNsTagProcessor(TemplateMode.HTML, dialectPrefix));
        return processors;
    }
}
