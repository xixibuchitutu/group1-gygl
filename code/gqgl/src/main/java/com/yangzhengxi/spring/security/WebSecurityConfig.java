package com.yangzhengxi.spring.security;

import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.server.ErrorPageRegistrar;
import org.springframework.boot.web.server.ErrorPageRegistry;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.Resource;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true) // 控制权限注解
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Resource
    private UserDetailsService userDetailsService;
    @Resource
    private PasswordEncoder passwordEncoder;
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(userDetailsService);
        auth.authenticationProvider(daoAuthenticationProvider);
        // 调用 super 将导致不生效 所以下面语句不要写
        super.configure(auth);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                .and().headers().frameOptions().disable()
                .and().csrf().disable()
                // 根据配置文件放行无需验证的url
                .authorizeRequests()

                .antMatchers("/favicon.ico").permitAll()
                .antMatchers("/assets/**").permitAll()
                .antMatchers("/login/**").permitAll()
                .antMatchers("/anon/**").permitAll()
                .antMatchers("/anon/**").permitAll()
                .anyRequest().authenticated()
                .and()

                .formLogin()
                .loginProcessingUrl("/login/verification")
                .loginPage("/login/login").permitAll()
                .successForwardUrl("/login/main")
                .failureUrl("/login/fail")
                .and()
                .logout()
                .logoutUrl("/logout").permitAll();
    }


   /* @Configuration
    public class SprErrorPage implements ErrorPageRegistrar {
        @Override
        public void registerErrorPages(ErrorPageRegistry registry) {
            registry.addErrorPages(new ErrorPage(HttpStatus.FORBIDDEN, "/test/500"));
        }
    }
*/
}

