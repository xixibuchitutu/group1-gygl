package com.yangzhengxi.spring.controller;

import com.alibaba.fastjson.JSON;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class    BaseController {
    @Resource
    private MessageSource messageSource;
    @Value("${unbound.id}")
    private String noBoundId;
    public String jsonSuccess(String i18nKey,Object ... obj){
        Locale locale = LocaleContextHolder.getLocale();
        Map<String,Object> rstMap = new HashMap<>();
        String msg = messageSource.getMessage(i18nKey,obj,locale);
        rstMap.put("success",true);
        rstMap.put("msg",msg);
        return JSON.toJSONString(rstMap);
    }
    public String jsonFail(String i18nKey,Object ... obj){
        Locale locale = LocaleContextHolder.getLocale();
        Map<String,Object> rstMap = new HashMap<>();
        String msg = messageSource.getMessage(i18nKey,obj,locale);
        rstMap.put("success",false);
        rstMap.put("msg",msg);
        return JSON.toJSONString(rstMap);
    }
    public ZtreeEntity getNoBoundNode(){
        Locale locale = LocaleContextHolder.getLocale();
        String name  = messageSource.getMessage("common.no.bound",null,locale);
        ZtreeEntity ztreeEntity = new ZtreeEntity(noBoundId,name,"-3");
        return ztreeEntity;
    }

    @GetMapping("/403")
    public String show403() {
        return "/error/403";
    }

    public String jsonMsgSuccess(String msg){
        Map<String,Object> rstMap = new HashMap<>();
        rstMap.put("success",true);
        rstMap.put("msg",msg);

        return JSON.toJSONString(rstMap);
    }
}