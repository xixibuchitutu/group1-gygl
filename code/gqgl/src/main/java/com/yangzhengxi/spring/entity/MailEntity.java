package com.yangzhengxi.spring.entity;

import java.io.Serializable;

/**
 * 发送邮件实体类
 */
public class MailEntity implements Serializable {
    /**
     * 代理主机名称
     */
    private String mailHostName;
    /**
     * 代理来源邮件地址
     */
    private String mailFromUser;
    /**
     * 代理来源邮件地址的密码
     */
    private String mailFromPwd;

    /**
     * 目标邮件地址
     */
    private String maiToUser;
    /**
     * 邮箱主题
     */
    private String mailSubject;

    /**
     * 邮箱内容
     */
    private String mailContent;

    public String getMailHostName() {
        return mailHostName;
    }

    public void setMailHostName(String mailHostName) {
        this.mailHostName = mailHostName;
    }

    public String getMailFromUser() {
        return mailFromUser;
    }

    public void setMailFromUser(String mailFromUser) {
        this.mailFromUser = mailFromUser;
    }

    public String getMailFromPwd() {
        return mailFromPwd;
    }

    public void setMailFromPwd(String mailFromPwd) {
        this.mailFromPwd = mailFromPwd;
    }

    public String getMaiToUser() {
        return maiToUser;
    }

    public void setMaiToUser(String maiToUser) {
        this.maiToUser = maiToUser;
    }

    public String getMailSubject() {
        return mailSubject;
    }

    public void setMailSubject(String mailSubject) {
        this.mailSubject = mailSubject;
    }

    public String getMailContent() {
        return mailContent;
    }

    public void setMailContent(String mailContent) {
        this.mailContent = mailContent;
    }

    @Override
    public String toString() {
        return "MailEntity{" +
                "mailHostName='" + mailHostName + '\'' +
                ", mailFromUser='" + mailFromUser + '\'' +
                ", mailFromPwd='" + mailFromPwd + '\'' +
                ", maiToUser='" + maiToUser + '\'' +
                ", mailSubject='" + mailSubject + '\'' +
                ", mailContent='" + mailContent + '\'' +
                '}';
    }
}
