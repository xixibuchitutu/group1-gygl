package com.yangzhengxi.spring.tag.respository;


import com.yangzhengxi.spring.tag.entity.DictTagEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DicDataTagDao {
    List<DictTagEntity> queryDicInfoByGroupId(@Param("dictGroup") String dictGroup, @Param("lang") String lang);
}
