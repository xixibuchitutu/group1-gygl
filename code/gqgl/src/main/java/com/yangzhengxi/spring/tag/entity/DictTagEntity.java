package com.yangzhengxi.spring.tag.entity;

import java.io.Serializable;

public class DictTagEntity implements Serializable {
    private String dicCode;
    private String dictName;

    public String getDicCode() {
        return dicCode;
    }

    public void setDicCode(String dicCode) {
        this.dicCode = dicCode;
    }

    public String getDictName() {
        return dictName;
    }

    public void setDictName(String dictName) {
        this.dictName = dictName;
    }

    @Override
    public String toString() {
        return "DictTagEntity{" +
                "dicCode='" + dicCode + '\'' +
                ", dictName='" + dictName + '\'' +
                '}';
    }
}
