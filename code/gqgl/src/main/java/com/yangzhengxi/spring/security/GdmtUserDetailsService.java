package com.yangzhengxi.spring.security;

import com.yangzhengxi.mybatis.entity.PtUserEntity;
import com.yangzhengxi.user.entity.SecurityUser;
import com.yangzhengxi.user.service.UserService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service("userDetailsService")
public class GdmtUserDetailsService implements UserDetailsService {
    @Resource
    private PasswordEncoder passwordEncoder;

    @Resource
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //用户名鉴权
        PtUserEntity ptUserEntity = userService.queryUserInfoByName(username);
        if (ptUserEntity == null) {
            ptUserEntity =  userService.queryUserInfoByMail(username);
            ptUserEntity.setPassword("email");
            if(ptUserEntity == null){
                throw  new IllegalArgumentException(username + " is not exit.");
            }

        }

        if (ptUserEntity.getLocked() == 1) {
            throw new RuntimeException("账户已被锁定! 请联系管理员!");
        }

        if (ptUserEntity.getLastInputError() != null) {
            Calendar instance = Calendar.getInstance();
            instance.setTime(new Date());
            instance.add(Calendar.MINUTE,30);
            //在30分钟之内
            if(instance.after(ptUserEntity.getLastInputError()))
            {
                //如果账户的输错次数等于三次则抛出账户锁定异常并更新数据表
                throw new RuntimeException("账户已被锁定! 请联系管理员!");
            }
        }



        //权限管理
        List<String> authStrList = userService.queryrivilegeList(ptUserEntity.getUserUuid());
        List<GrantedAuthority> collection = new ArrayList<>();
        for (String authString : authStrList) {
            if (authString != null) {
                collection.add(new SimpleGrantedAuthority("ROLE_" + authString));
            }
        }
        // 参数分别是：用户名，密码，用户权限
        SecurityUser user =new  SecurityUser(ptUserEntity.getUserName(), ptUserEntity.getPassword(), ptUserEntity,collection);


        return user;
    }
}

