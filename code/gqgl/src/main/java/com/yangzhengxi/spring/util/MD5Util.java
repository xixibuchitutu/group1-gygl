package com.yangzhengxi.spring.util;

import org.springframework.util.DigestUtils;

public class MD5Util {
    public static void main(String[] args) {
        String passwd = "1qaz";
        String rst = DigestUtils.md5DigestAsHex((passwd + "abc123456abc").getBytes());
        System.out.println(rst);
    }
}
