package com.yangzhengxi.spring.tag.service;



import com.yangzhengxi.spring.tag.entity.DictTagEntity;

import java.util.List;

public interface DicDataTagService {
    List<DictTagEntity> queryDicInfoByGroupId(String attributeValue);
}
