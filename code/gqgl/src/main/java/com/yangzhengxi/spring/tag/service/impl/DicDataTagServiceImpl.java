package com.yangzhengxi.spring.tag.service.impl;


import com.yangzhengxi.spring.tag.entity.DictTagEntity;
import com.yangzhengxi.spring.tag.respository.DicDataTagDao;
import com.yangzhengxi.spring.tag.service.DicDataTagService;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
public class DicDataTagServiceImpl implements DicDataTagService {
    @Resource
    private DicDataTagDao dicDataTagDao;


    @Resource
    private MessageSource messageSource;


    @Override
    public List<DictTagEntity> queryDicInfoByGroupId(String attributeValue) {
        Locale locale = LocaleContextHolder.getLocale();
        List<DictTagEntity> list = dicDataTagDao.queryDicInfoByGroupId(attributeValue,locale.toString());
        DictTagEntity dictTagEntity = new DictTagEntity();
        String name  = messageSource.getMessage("common.select",null,locale);
        dictTagEntity.setDicCode("");
        dictTagEntity.setDictName(name);

        List<DictTagEntity> listR = new ArrayList<>();
        listR.add(dictTagEntity);
        for(DictTagEntity item:list){
            listR.add(item);
        }

        return listR;
    }
}
