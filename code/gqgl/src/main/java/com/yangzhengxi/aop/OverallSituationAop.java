package com.yangzhengxi.aop;

import com.alibaba.fastjson.JSON;
import com.yangzhengxi.log.service.LogService;
import com.yangzhengxi.mybatis.entity.PtLogEntity;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Aspect
@Component
public class OverallSituationAop {
    @Autowired
    private LogService logService;

    @Around("execution(public * com.yangzhengxi..service.impl.*.*(..))")
    public Object addLog(ProceedingJoinPoint joinPoint) throws Throwable{
        long startTime = System.currentTimeMillis(); //开始时间
        Object object = joinPoint.proceed(); //执行方法
        long endTime = System.currentTimeMillis(); //结束时间
        long runTime = endTime - startTime; //运行时间
        String methodName = joinPoint.getTarget().getClass().getName() + "."+ joinPoint.getSignature().getName(); //接口名称
        String methodParam = "";
        //参数校验 有些请求没有参数
        if(joinPoint.getArgs()!=null && joinPoint.getArgs().length > 0){
            methodParam = String.valueOf(joinPoint.getArgs()[0]);
        }
        String whiteName = joinPoint.getSignature().getName();
        // get query find load 事务只读 不能调用save 不然就抛异常了
        // 保存日志和删除日志不记录，不然俄罗斯套娃了
        if(whiteName.startsWith("get") || whiteName.startsWith("query") || whiteName.startsWith("find") ||
                whiteName.startsWith("load") || whiteName.equals("saveLogData_tran_new") || whiteName.equals("delLoginInfo")){
            return object;
        }

        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        PtLogEntity ptLogEntity = new PtLogEntity();
        ptLogEntity.setRequestUrl(request.getRequestURI()); //请求URL
        ptLogEntity.setStartTime(new Date(startTime));
        ptLogEntity.setEndTime(new Date(endTime));
        ptLogEntity.setRunTime(runTime);
        ptLogEntity.setMethodName(methodName);
        ptLogEntity.setMethodParam(methodParam);
        ptLogEntity.setLogContents(JSON.toJSONString(object)); //返回参数
        logService.saveLogData_tran_new(ptLogEntity);
        return object;
    }

}
