package com.yangzhengxi.config.control;

import com.yangzhengxi.config.entity.SearchConfigEntity;
import com.yangzhengxi.config.service.ConfigService;
import com.yangzhengxi.mybatis.entity.PtSysConfigEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.spring.controller.BaseController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.yangzhengxi.login.service.AnonService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/config")
public class ConfigController extends BaseController {
    private Logger logger = LogManager.getLogger(this.getClass());

    @Resource
    private ConfigService configService;

    @Secured("ROLE_gqgy_data_config")
    @RequestMapping("/configPage")
    public String configPage(){
        return "config/configPage";
    }

    @Secured("ROLE_gqgy_data_config_query")
    @RequestMapping("/queryConfigInfoListByPage")
    @ResponseBody
    public PadingRstType<PtSysConfigEntity> queryConfigInfoListByPage(SearchConfigEntity search, PaddingEntity padding){
        logger.info(search);
        logger.info(padding);
        PadingRstType<PtSysConfigEntity> padingRstType = configService.queryConfigInfoListByPage(search,padding);

        return padingRstType;
    }

    @Secured("ROLE_gqgy_data_config_add")
    @RequestMapping("/addConfigInfo")
    @ResponseBody
    public String addConfigInfo(PtSysConfigEntity ptSysConfigEntity){
        logger.info(ptSysConfigEntity);
        configService.addConfigInfo(ptSysConfigEntity);
        return jsonSuccess("config.add.success");
    }

    @Secured("ROLE_gqgy_data_config_update")
    @RequestMapping("/modifyConfigInfo")
    @ResponseBody
    public String modifyConfigInfo(PtSysConfigEntity ptSysConfigEntity){
        logger.info(ptSysConfigEntity);
        configService.modifyConfigInfo(ptSysConfigEntity);
        return jsonSuccess("config.modify.success");
    }

    @Secured("ROLE_gqgy_data_config_delete")
    @RequestMapping("/deleteConfigById")
    @ResponseBody
    public String deleteConfigById(@RequestParam("cfgIdArray") String cfgIdArray){
        logger.info("cfgIdArray:" + cfgIdArray);
        configService.deleteConfigById(cfgIdArray);
        return jsonSuccess("config.delete.success");
    }


    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * 获取全部的 配置信息
     * @return
     */
    @GetMapping("/getCount")
    public List<SearchConfigEntity> getCountConfig(){
        List<SearchConfigEntity> searchConfigEntities = jdbcTemplate.queryForList("select * from pt_sys_config", SearchConfigEntity.class);
        System.out.printf("", searchConfigEntities.toString());
        return searchConfigEntities;
    }

}
