package com.yangzhengxi.config.entity;

import java.io.Serializable;

public class SearchConfigEntity implements Serializable {
    private String cfgName;

    public String getCfgName() {
        return cfgName;
    }

    public void setCfgName(String cfgName) {
        this.cfgName = cfgName;
    }

    @Override
    public String toString() {
        return "SearchConfigEntity{" +
                "cfgName='" + cfgName + '\'' +
                '}';
    }
}

