package com.yangzhengxi.config.respository;

import com.yangzhengxi.config.entity.SearchConfigEntity;
import com.yangzhengxi.mybatis.entity.PtSysConfig;
import com.yangzhengxi.mybatis.entity.PtSysConfigEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ConfigDao {

    List<PtSysConfig> querySysConfigList();

    List<PtSysConfigEntity> queryConfigInfoListByPage(@Param("search") SearchConfigEntity search, @Param("padding") PaddingEntity padding);

    Integer queryConfigInfoTotal(@Param("search")SearchConfigEntity search);

    void insertConfigInfo(@Param("entity")PtSysConfigEntity ptSysConfigEntity);

    void modifyConfigInfo(@Param("entity")PtSysConfigEntity ptSysConfigEntity);

    void deleteConfigById(@Param("cfgIds") String[] cfgIds);
}

