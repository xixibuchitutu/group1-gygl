package com.yangzhengxi.config.service;

import com.yangzhengxi.config.entity.SearchConfigEntity;
import com.yangzhengxi.mybatis.entity.PtSysConfigEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;

import javax.servlet.http.HttpSession;

public interface ConfigService {
    PadingRstType<PtSysConfigEntity> queryConfigInfoListByPage(SearchConfigEntity search, PaddingEntity padding);

    void addConfigInfo(PtSysConfigEntity ptSysConfigEntity);
    void setConfigValue(HttpSession session);

    void modifyConfigInfo(PtSysConfigEntity ptSysConfigEntity);

    void deleteConfigById(String cfgIdArray);
}
