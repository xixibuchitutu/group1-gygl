package com.yangzhengxi.config.service.impl;

import com.yangzhengxi.config.entity.SearchConfigEntity;
import com.yangzhengxi.config.respository.ConfigDao;
import com.yangzhengxi.config.service.ConfigService;
import com.yangzhengxi.mybatis.entity.PtSysConfig;
import com.yangzhengxi.mybatis.entity.PtSysConfigEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.spring.util.DataUitl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

@Service("configService")
public class ConfigServiceImpl implements ConfigService {
    @Resource
    private ConfigDao configDao;

    @Override
    public PadingRstType<PtSysConfigEntity> queryConfigInfoListByPage(SearchConfigEntity search, PaddingEntity padding) {
        padding.deal(PtSysConfigEntity.class);
        PadingRstType<PtSysConfigEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(padding.getPage());
        List<PtSysConfigEntity> list = configDao.queryConfigInfoListByPage(search,padding);
        padingRstType.setRawRecords(list);
        Integer total = configDao.queryConfigInfoTotal(search);
        padingRstType.setTotal(total);
        padingRstType.putItems();

        return padingRstType;
    }

    @Override
    public void addConfigInfo(PtSysConfigEntity ptSysConfigEntity) {
        configDao.insertConfigInfo(ptSysConfigEntity);
    }

    @Override
    public void modifyConfigInfo(PtSysConfigEntity ptSysConfigEntity) {
        configDao.modifyConfigInfo(ptSysConfigEntity);
    }

    @Override
    public void deleteConfigById(String cfgIdArray) {
        String[] cfgIds = cfgIdArray.split(",");
        configDao.deleteConfigById(cfgIds);
    }

    @Override
    public void setConfigValue(HttpSession session) {
        List<PtSysConfig> list = configDao.querySysConfigList();
        for (PtSysConfig item : list) {
            session.setAttribute(item.getCfgName(), item.getCfgValue());
        }
    }
}
