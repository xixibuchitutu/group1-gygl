package com.yangzhengxi.department.respository;

import com.yangzhengxi.department.entity.SearchDepartEntity;
import com.yangzhengxi.duty.entity.SearchEntity;
import com.yangzhengxi.mybatis.entity.PtDepartEntity;
import com.yangzhengxi.mybatis.entity.PtDutyEntity;
import com.yangzhengxi.mybatis.entity.PtOrganEntity;
import com.yangzhengxi.mybatis.entity.PtUserEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.user.entity.SearchUserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DepartDao {

    List<ZtreeEntity> queryLeftTree();
    void deleteDepartInfoByIds(@Param("depUuid")String depUuid);


    void unboundParentIdByDepUuid(@Param("depUuid")String depUuid, @Param("belongCenter")String belongCenter);

   PtDepartEntity queryDepartListById(@Param("depUuid") String depUuid);


    List<ZtreeEntity> queryBoundDepartTree();

    String queryParentId(@Param("depUuid") String depUuid);

    void saveParentIdByDepUuid(String depUuid, String parentId);

    List<ZtreeEntity> queryDepartTree(String depUuid);

    void saveBelongCenterIdByDepUuid(@Param("depUuid")String depUuid, @Param("belongCenter")String belongCenter);

    List<PtDepartEntity> queryDepartInfoListByPage(@Param("search") SearchEntity search, @Param("padding") PaddingEntity padding);


    Integer queryDepartInfoListTotal(@Param("search") SearchEntity search);

    List<PtUserEntity> queryUserInfoListByPage(@Param("entity") SearchDepartEntity entity, @Param("padding")PaddingEntity paddingEntity);


    List<PtUserEntity> queryUserListByPage(@Param("entity") SearchDepartEntity entity, @Param("padding")PaddingEntity paddingEntity);
    int querySubdep(@Param("depUuid") String depUuid);
    int querySubuser(@Param("depUuid") String depUuid);
    int querySubadm(@Param("depUuid") String depUuid);
    void insertDepartInfo(@Param("entity")PtDepartEntity ptDepartEntity);
    void updateDepartById(@Param("entity") PtDepartEntity ptDepartEntity);
    List<PtDutyEntity> queryDuty1ListByPage(@Param("search")SearchDepartEntity search, @Param("padding")PaddingEntity padding);
    List<PtUserEntity> queryDutyuserListByPage(@Param("search")SearchDepartEntity search, @Param("padding")PaddingEntity padding);

}

