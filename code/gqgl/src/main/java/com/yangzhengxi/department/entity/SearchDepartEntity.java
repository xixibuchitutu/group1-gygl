package com.yangzhengxi.department.entity;


import com.yangzhengxi.mybatis.annotation.Columns;

import java.io.Serializable;

public class SearchDepartEntity implements Serializable {
        @Columns("USER_UUID")
        private String userUuid;
        @Columns("DEPID")
        private String depId;
        @Columns("USER_NAME")
        private String userName;
        @Columns("PASSWORD")
        private String password;
        @Columns("EMAIL")
        private String email;
        @Columns("MOBILE")
        private  String mobile;
        @Columns("NICE_NAME")
        private String niceName;
        @Columns("REGISTERDATE")
        private  String registerDate;
        @Columns("REMARK")
        private String remark;
        @Columns("DEL_FLAG")
        private String delFlag;
        @Columns("MODIFERID")
        private String modifierId;
        @Columns("IS_SUM")
        private String isSum;



        public String getUserUuid() {
            return userUuid;
        }

        public void setUserUuid(String userUuid) {
            this.userUuid = userUuid;
        }



        public String getDepId() {
            return depId;
        }

        public void setDepId(String depId) {
            this.depId = depId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getNiceName() {
            return niceName;
        }

        public void setNiceName(String niceName) {
            this.niceName = niceName;
        }

        public String getRegisterDate() {
            return registerDate;
        }

        public void setRegisterDate(String registerDate) {
            this.registerDate = registerDate;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getDelFlag() {
            return delFlag;
        }

        public void setDelFlag(String delFlag) {
            this.delFlag = delFlag;
        }

        public String getModifierId() {
            return modifierId;
        }

        public void setModifierId(String modifierId) {
            this.modifierId = modifierId;
        }

        public String getIsSum() {
            return isSum;
        }

        public void setIsSum(String isSum) {
            this.isSum = isSum;
        }

        @Override
        public String toString() {
            return "SearchDepartEntity{" +
                    "userUuid='" + userUuid + '\'' +
                    ", depId='" + depId + '\'' +
                    ", userName='" + userName + '\'' +
                    ", password='" + password + '\'' +
                    ", email='" + email + '\'' +
                    ", mobile='" + mobile + '\'' +
                    ", niceName='" + niceName + '\'' +
                    ", registerDate='" + registerDate + '\'' +
                    ", remark='" + remark + '\'' +
                    ", delFlag='" + delFlag + '\'' +
                    ", modifierId='" + modifierId + '\'' +
                    ", isSum='" + isSum + '\'' +
                    '}';
        }

    }


