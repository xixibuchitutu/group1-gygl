package com.yangzhengxi.department.service.impl;

import com.yangzhengxi.department.entity.SearchDepartEntity;
import com.yangzhengxi.department.respository.DepartDao;
import com.yangzhengxi.department.service.DepartService;
import com.yangzhengxi.duty.entity.SearchEntity;
import com.yangzhengxi.mybatis.entity.PtDepartEntity;
import com.yangzhengxi.mybatis.entity.PtDutyEntity;
import com.yangzhengxi.mybatis.entity.PtUserEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.spring.controller.BaseController;
import com.yangzhengxi.spring.util.DataUitl;
import com.yangzhengxi.user.entity.SearchUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartServiceImpl extends BaseController implements DepartService{
    @Autowired
    private DepartDao departDao;

    @Value("${unbound.id}")
    private String unboundId;

    @Override
    public List<ZtreeEntity> queryLeftTree() {
        List<ZtreeEntity> list = departDao.queryLeftTree();
        list.add(getNoBoundNode());
        return list;
    }
    @Override
    public boolean deleteDepartInfoByIds(String depUuid) {
        int department = departDao.querySubdep(depUuid);
        int user = departDao.querySubuser(depUuid);
        int users = departDao.querySubadm(depUuid);
        if(department >0){
            return false;
        }
        if(user >0 ){
            return false;
        }
        if(users>0){
            return false;
        }
        departDao.deleteDepartInfoByIds(depUuid);
        return true;
    }




    @Override
    public void unboundParentIdByDepUuid(String depUuid) {
        departDao.unboundParentIdByDepUuid(depUuid,unboundId);
    }



    @Override
    public PtDepartEntity queryDepartListById(String depUuid) {
        return departDao.queryDepartListById(depUuid);
    }
    @Override
    public List<ZtreeEntity> queryBoundDepartTree(String depUuid) {
        List<ZtreeEntity> list = departDao.queryBoundDepartTree();
        String parentId = departDao.queryParentId(depUuid);
        for(ZtreeEntity item:list){
            if(parentId.equals(item.getId())){
                item.setChecked(true);
                break;
            }
        }
        return list;
    }



    @Override
    public void saveParentIdByDepUuid(String depUuid, String parentId) {
        departDao.saveParentIdByDepUuid(depUuid,parentId);
    }

    @Override
    public List<ZtreeEntity> queryDepartTree(String depUuid) {
        List<ZtreeEntity> list= departDao.queryDepartTree(depUuid);
        String parentId = departDao.queryParentId(depUuid);
        for(ZtreeEntity item:list){
            if(parentId.equals(item.getId())){
                item.setChecked(true);
                break;
            }
        }
        return list;
    }

    @Override
    public void saveBelongCenterIdByDepUuid(String depUuid, String belongCenter) {
        departDao.saveBelongCenterIdByDepUuid(depUuid,belongCenter);
    }

    @Override
    public PadingRstType<PtDepartEntity> queryDepartInfoListById(String depUuid, PaddingEntity padding) {
        return null;
    }

    @Override
    public PadingRstType<PtDepartEntity> queryDepartInfoListByPage(SearchEntity search, PaddingEntity padding) {
        padding.deal(PtDepartEntity.class);
        PadingRstType<PtDepartEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(padding.getPage());
        List<PtDepartEntity> list = departDao.queryDepartInfoListByPage(search,padding);
        padingRstType.setRawRecords(list);
        Integer total = departDao.queryDepartInfoListTotal(search);
        padingRstType.setTotal(total);
        padingRstType.putItems();

        return padingRstType;
    }
    @Override
    public void addDepartInfo(PtDepartEntity ptDepartEntity) {
        ptDepartEntity.setDepUuid(DataUitl.getUuid());
        departDao.insertDepartInfo(ptDepartEntity);
    }

    @Override
    public void updateDepartById(PtDepartEntity ptDepartEntity) {
        departDao.updateDepartById(ptDepartEntity);
    }

    @Override
    public PadingRstType<PtUserEntity> queryUserListByPage(SearchDepartEntity search, PaddingEntity padding) {
        padding.deal(PtUserEntity.class);
        PadingRstType<PtUserEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(padding.getPage());
        List<PtUserEntity> list = departDao.queryUserInfoListByPage(search,padding);
        padingRstType.setRawRecords(list);
        padingRstType.putItems();

        return padingRstType;
    }

    @Override
    public PadingRstType<PtUserEntity> queryUserInfoListByPage(SearchDepartEntity search, PaddingEntity padding) {
        padding.deal(PtUserEntity.class);
        PadingRstType<PtUserEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(padding.getPage());
        List<PtUserEntity> list = departDao.queryUserInfoListByPage(search,padding);
        padingRstType.setRawRecords(list);
        padingRstType.putItems();

        return padingRstType;
    }
    @Override
    public PadingRstType<PtDutyEntity> queryDuty1ListByPage(SearchDepartEntity search, PaddingEntity padding) {
        padding.deal(PtDutyEntity.class);
        PadingRstType<PtDutyEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(padding.getPage());
        List<PtDutyEntity> list = departDao.queryDuty1ListByPage(search,padding);
        padingRstType.setRawRecords(list);
        padingRstType.putItems();

        return padingRstType;
    }
    @Override
    public PadingRstType<PtUserEntity> queryDutyuserListByPage(SearchDepartEntity search, PaddingEntity padding) {
        padding.deal(PtUserEntity.class);
        PadingRstType<PtUserEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(padding.getPage());
        List<PtUserEntity> list = departDao.queryDutyuserListByPage(search,padding);
        padingRstType.setRawRecords(list);
        padingRstType.putItems();

        return padingRstType;
    }
}
