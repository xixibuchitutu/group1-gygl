package com.yangzhengxi.department.control;
import com.yangzhengxi.department.entity.SearchDepartEntity;
import com.yangzhengxi.department.service.DepartService;
import com.yangzhengxi.duty.entity.SearchEntity;
import com.yangzhengxi.menu.service.MenuService;
import com.yangzhengxi.mybatis.entity.PtDepartEntity;
import com.yangzhengxi.mybatis.entity.PtDutyEntity;
import com.yangzhengxi.mybatis.entity.PtUserEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.spring.controller.BaseController;
import com.yangzhengxi.spring.tag.entity.DictTagEntity;
import com.yangzhengxi.user.entity.SearchUserEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/department")
public class DepartController extends BaseController{
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private DepartService departService;


    @RequestMapping("/departPage")
    public String departPage(){
        return "department/departPage";
    }


    @RequestMapping("/queryLeftTree")
    @ResponseBody
    public List<ZtreeEntity> queryLeftTree(){
        return departService.queryLeftTree();
    }

    @RequestMapping("/unboundParentIdByDepUuid")
    @ResponseBody
    public String unboundParentIdByDepUuid(@RequestParam("depUuid") String depUuid) {
        logger.info("depUuid:" + depUuid);
        departService.unboundParentIdByDepUuid(depUuid);
        return jsonSuccess("menu.unbound.parent.success");

    }

    @RequestMapping("/queryBoundDepartTree")
    @ResponseBody
    public List<ZtreeEntity> queryBoundDepartTree(@RequestParam("depUuid") String depUuid) {
        logger.info("depUuid:" + depUuid);
        List<ZtreeEntity> list = departService.queryBoundDepartTree(depUuid);
        return list;
}

    @RequestMapping("/queryDepartTree")
    @ResponseBody
    public  List<ZtreeEntity> queryDepartTree (@RequestParam(value="depUuid",required = false) String depUuid){
        logger.info("depUuid:" + depUuid);
        List<ZtreeEntity> list = departService.queryDepartTree(depUuid);
        return list;
    }

    @RequestMapping("/saveParentIdByDepUuid")
    @ResponseBody
    public String saveParentIdByDepUuid(@RequestParam("depUuid") String depUuid, @RequestParam("belongCenter") String belongCenter) {
        logger.info("depUuid:" + depUuid + " belongCenter:" + belongCenter);
        departService.saveParentIdByDepUuid(depUuid, belongCenter);
        return jsonSuccess("depart.bound.parent.success");

    }

    @RequestMapping("/deleteDepartInfoByIds")
    @ResponseBody
    public String deleteDepartInfoByIds(@RequestParam("depUuid") String depUuid) {
        logger.info("depUuid:" + depUuid );
        boolean b = departService.deleteDepartInfoByIds(depUuid);
        String str = b?"depart.del.depart.success":"depart.del.depart.fail";
        return jsonSuccess(str);
    }

    @RequestMapping("/updateDepartById")
    @ResponseBody
    public String updateDepartById(PtDepartEntity PtDepartEntity){
        logger.info(PtDepartEntity);
        departService.updateDepartById(PtDepartEntity);
        return jsonSuccess("depart.update");
    }


    @RequestMapping("/queryOrganListById")
    @ResponseBody
    public PtDepartEntity queryOrganListById(@RequestParam(value = "depUuid",required = false) String depUuid){
        logger.info("depUuid"+depUuid);
       PtDepartEntity ptDepartEntity = departService.queryDepartListById(depUuid);
        return ptDepartEntity;
    }
    @RequestMapping("/saveBelongCenterIdByDepUuid")
    @ResponseBody
    public String saveBelongCenterIdByDepUuid(@RequestParam("depUuid")String depUuid,@RequestParam("belongCenter")String belongCenter){
        logger.info("depUuid"+depUuid + "belongCenter" + belongCenter);
        departService.saveBelongCenterIdByDepUuid(depUuid,belongCenter);
       return jsonSuccess("depart.bound.parent.success");
    }

    @RequestMapping("/queryDepartInfoListByPage")
    @ResponseBody
    public PadingRstType<PtDepartEntity> queryDepartInfoListByPage(SearchEntity search, PaddingEntity padding){
        logger.info(search);
        logger.info(padding);
        PadingRstType<PtDepartEntity> padingRstType = departService.queryDepartInfoListByPage(search,padding);

        return padingRstType;
    }
    @RequestMapping("/queryUserListByPage")
    @ResponseBody
    public PadingRstType<PtUserEntity> queryUserListByPage(SearchDepartEntity entity, PaddingEntity paddingEntity){
        logger.info(entity);
        logger.info(paddingEntity);
        PadingRstType<PtUserEntity>  padingRstType= departService.queryUserInfoListByPage(entity,paddingEntity);
        return padingRstType;
    }

    @RequestMapping("/addDepartInfo")
    @ResponseBody
    public String addDepartInfo(PtDepartEntity ptDepartEntity){
        logger.info(ptDepartEntity);
        departService.addDepartInfo(ptDepartEntity);
        return jsonSuccess("depart.add.success");
    }
    @RequestMapping("/queryUserInfoListByPage")
    @ResponseBody
    public PadingRstType<PtUserEntity> queryUserInfoListByPage(SearchDepartEntity entity, PaddingEntity paddingEntity){
        logger.info(entity);
        logger.info(paddingEntity);
        PadingRstType<PtUserEntity>  padingRstType= departService.queryUserInfoListByPage(entity,paddingEntity);
        return padingRstType;
    }
    @RequestMapping("/queryDuty1ListByPage")
    @ResponseBody
    public PadingRstType<PtDutyEntity> queryDuty1ListByPage(SearchDepartEntity entity, PaddingEntity paddingEntity){
        logger.info(entity);
        logger.info(paddingEntity);
        PadingRstType<PtDutyEntity>  padingRstType= departService.queryDuty1ListByPage(entity,paddingEntity);
        return padingRstType;
    }

    @RequestMapping("/queryDutyuserListByPage")
    @ResponseBody
    public PadingRstType<PtUserEntity> queryDutyuserListByPage(SearchDepartEntity entity, PaddingEntity paddingEntity){
        logger.info(entity);
        logger.info(paddingEntity);
        PadingRstType<PtUserEntity>  padingRstType= departService.queryDutyuserListByPage(entity,paddingEntity);
        return padingRstType;
    }
}
