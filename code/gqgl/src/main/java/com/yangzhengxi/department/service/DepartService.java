package com.yangzhengxi.department.service;

import com.yangzhengxi.department.entity.SearchDepartEntity;
import com.yangzhengxi.duty.entity.SearchEntity;
import com.yangzhengxi.mybatis.entity.PtDepartEntity;
import com.yangzhengxi.mybatis.entity.PtDutyEntity;
import com.yangzhengxi.mybatis.entity.PtUserEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.user.entity.SearchUserEntity;

import java.util.List;


public interface DepartService {

    List<ZtreeEntity> queryLeftTree();
    boolean deleteDepartInfoByIds(String depUuid);

    void unboundParentIdByDepUuid(String depUuid);

    PtDepartEntity queryDepartListById(String depUuid);

    List<ZtreeEntity> queryBoundDepartTree(String depUuid);

    void saveParentIdByDepUuid(String depUuid, String belongCenter);

    List<ZtreeEntity> queryDepartTree(String depUuid);

    void saveBelongCenterIdByDepUuid(String depUuid, String belongCenter);
    PadingRstType<PtDepartEntity> queryDepartInfoListById(String depUuid, PaddingEntity padding) ;

    PadingRstType<PtDepartEntity> queryDepartInfoListByPage(SearchEntity search, PaddingEntity padding);
    PadingRstType<PtUserEntity> queryUserInfoListByPage(SearchDepartEntity entity, PaddingEntity paddingEntity);

    void addDepartInfo(PtDepartEntity ptDepartEntity);
    PadingRstType<PtUserEntity> queryDutyuserListByPage(SearchDepartEntity entity, PaddingEntity paddingEntity);
    void updateDepartById(PtDepartEntity ptDepartEntity);
    PadingRstType<PtUserEntity> queryUserListByPage(SearchDepartEntity entity, PaddingEntity paddingEntity);
    PadingRstType<PtDutyEntity> queryDuty1ListByPage(SearchDepartEntity entity, PaddingEntity paddingEntity);

}

