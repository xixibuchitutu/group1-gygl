package com.yangzhengxi.organ.repository;

import com.yangzhengxi.mybatis.entity.PtOrganEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrganDao {
    List<ZtreeEntity> queryOrganLeftTree();
    PtOrganEntity queryOrganListById(@Param("ORGAN_UUID")String ORGAN_UUID);
    List<ZtreeEntity> queryBoundOrganTree();

    String queryParentId(@Param("ORGAN_UUID")String organ_uuid);
    void saveParentIdByOrganId(@Param("ORGAN_UUID")String ORGAN_UUID,@Param("PARENT_UUID")String PARENT_UUID);
    void addOrganInfo(@Param("entity")PtOrganEntity ptOrganEntity);

    void updateOrganById(@Param("entity")PtOrganEntity ptOrganEntity);
    void DeleteOrganById(@Param("organUuid")String organUuid);
    List<PtOrganEntity> queryOrganInfoListPaddingById(@Param("ORGAN_UUID")String organUuid, @Param("padding") PaddingEntity padding);

    Integer queryOrganInfoListTotal(@Param("ORGAN_UUID")String ORGAN_UUID);
    List<PtOrganEntity> queryDepInfoListPaddingById(@Param("ORGAN_UUID")String ORGAN_UUID, @Param("padding") PaddingEntity padding);
    Integer queryDepInfoListTotal(@Param("ORGAN_UUID") String ORGAN_UUID);
    String queryDepuuidById(@Param("ORGAN_UUID")String ORGAN_UUID);
    List<PtOrganEntity> queryPostInfoListPaddingByIdList(@Param("ORGAN_UUID")String organUuid, @Param("padding") PaddingEntity padding);

    Integer queryPostInfoListTotal(String organUuid);
    int querySubduty(@Param("organUuid") String organUuid);
    int querySuborg(@Param("organUuid") String organUuid);
    int querySubdep(@Param("organUuid") String organUuid);
}
