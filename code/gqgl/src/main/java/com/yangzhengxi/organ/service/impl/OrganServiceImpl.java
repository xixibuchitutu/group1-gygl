package com.yangzhengxi.organ.service.impl;

import com.yangzhengxi.mybatis.entity.PtOrganEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.organ.repository.OrganDao;
import com.yangzhengxi.organ.service.OrganService;
import com.yangzhengxi.spring.controller.BaseController;
import com.yangzhengxi.spring.util.DataUitl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class OrganServiceImpl  extends BaseController implements OrganService {
    @Resource
    private OrganDao organdao;
    @Value("${unbound.id}")
    private String unboundId;
    @Override
    public List<ZtreeEntity> queryOrganLeftTree() {
        List<ZtreeEntity> list = organdao.queryOrganLeftTree();
        list.add(getNoBoundNode());
        return list;
    }

    @Override
    public PtOrganEntity queryOrganListById(String ORGAN_UUID) {
        return organdao.queryOrganListById(ORGAN_UUID);

    }

    @Override
    public List<ZtreeEntity> queryBoundOrganTree(String organ_uuid) {
        List<ZtreeEntity> list=organdao.queryBoundOrganTree();
        String parentId=organdao.queryParentId(organ_uuid);
        for(ZtreeEntity item:list){
            if(parentId.equals(item.getId())){
                item.setChecked(true);
                break;
            }
        }
        return list;
    }

    @Override
    public void saveParentIdByOrganId(String ORGAN_UUID,String PARENT_UUID) {
        organdao.saveParentIdByOrganId(ORGAN_UUID,PARENT_UUID);
    }
    @Override
    public void unboundParentIdByOrganId(String organ_uuid) {
        organdao.saveParentIdByOrganId(organ_uuid,unboundId);
    }

    @Override
    public void addOrganInfo(PtOrganEntity ptOrganEntity) {
        ptOrganEntity.setOrganUuid(DataUitl.getUuid());
        organdao.addOrganInfo(ptOrganEntity);
    }

    @Override
    public void updateOrganById(PtOrganEntity ptOrganEntity) {
        organdao.updateOrganById(ptOrganEntity);
    }

    @Override
    public Boolean DeleteOrganById(String organUuid) {
        int department = organdao.querySubdep(organUuid);
        int duty = organdao.querySubduty(organUuid);
        int org=organdao.querySuborg(organUuid);
        if(department >0){
            return false;
        }
        if(duty >0){
            return false;
        }
        if(org >0){
            return false;
        }
        organdao.DeleteOrganById(organUuid);
        return true;
    }
    @Override
    public PadingRstType<PtOrganEntity> queryOrganInfoListById(String organUuid, PaddingEntity padding){

        padding.deal(PtOrganEntity.class);
        PadingRstType<PtOrganEntity> rstType = new PadingRstType<>();
        rstType.setPage(padding.getPage());
        List<PtOrganEntity> list = organdao.queryOrganInfoListPaddingById(organUuid,padding);
        rstType.setRawRecords(list);
        Integer total =  organdao.queryOrganInfoListTotal(organUuid);;
        rstType.setTotal(total);
        rstType.putItems();

        return rstType;
    }
    @Override
    public  PadingRstType<PtOrganEntity> queryDepInfoListById(String organUuid, PaddingEntity padding){

        padding.deal(PtOrganEntity.class);
        PadingRstType<PtOrganEntity> rstType = new PadingRstType<>();


        rstType.setPage(padding.getPage());

        List<PtOrganEntity> list = organdao.queryDepInfoListPaddingById(organUuid,padding);


        rstType.setRawRecords(list);
        Integer total =  organdao.queryDepInfoListTotal(organUuid);;
        rstType.setTotal(total);
        rstType.putItems();

        return rstType;
    }
    @Override
    public  PadingRstType<PtOrganEntity> queryPostInfoListById(String organUuid, PaddingEntity padding){
        padding.deal(PtOrganEntity.class);
        PadingRstType<PtOrganEntity> rstType = new PadingRstType<>();
        rstType.setPage(padding.getPage());
        List<PtOrganEntity> list = organdao.queryPostInfoListPaddingByIdList(organUuid,padding);
        rstType.setRawRecords(list);
        Integer total =  organdao.queryPostInfoListTotal(organUuid);;
        rstType.setTotal(total);
        rstType.putItems();

        return rstType;
    }




}
