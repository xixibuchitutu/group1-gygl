package com.yangzhengxi.organ.service;

import com.yangzhengxi.mybatis.entity.PtOrganEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;

import java.util.List;

public interface OrganService {
    List<ZtreeEntity> queryOrganLeftTree();

    PtOrganEntity queryOrganListById(String ORGAN_UUID);

    List<ZtreeEntity> queryBoundOrganTree(String organ_uuid);

    void saveParentIdByOrganId(String ORGAN_UUID,String PARENT_UUID);
    void unboundParentIdByOrganId(String organ_uuid);
    void addOrganInfo(PtOrganEntity ptOrganEntity);
    void updateOrganById(PtOrganEntity ptOrganEntity);
    Boolean DeleteOrganById(String organUuid);

    PadingRstType<PtOrganEntity> queryOrganInfoListById(String organUuid, PaddingEntity padding);

    PadingRstType<PtOrganEntity> queryDepInfoListById(String organUuid, PaddingEntity padding);
    PadingRstType<PtOrganEntity> queryPostInfoListById(String organUuid, PaddingEntity padding);

}
