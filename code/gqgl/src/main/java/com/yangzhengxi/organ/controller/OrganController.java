package com.yangzhengxi.organ.controller;

import com.yangzhengxi.mybatis.entity.PtOrganEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.mybatis.ztree.ZtreeEntity;
import com.yangzhengxi.organ.service.OrganService;
import com.yangzhengxi.spring.controller.BaseController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/organ")
public class OrganController extends BaseController {
    private Logger logger = LogManager.getLogger(this.getClass());
    @Resource
    private OrganService OrganService;
    @RequestMapping("/organPage")
    private String organPage(){
        return "organize/organize";
    }
    @RequestMapping("/queryOrganLeftTree")
    @ResponseBody
    private List<ZtreeEntity>queryOrganLeftTree(){
        return OrganService.queryOrganLeftTree();
    }
    @RequestMapping("/queryOrganListById")
    @ResponseBody
    private PtOrganEntity queryOrganListById(@RequestParam(value="ORGAN_UUID")String ORGAN_UUID) {
        PtOrganEntity PtOrganEntity = OrganService.queryOrganListById(ORGAN_UUID);
        return PtOrganEntity;
    }
    @RequestMapping("/queryBoundOrganTree")
    @ResponseBody
    private List<ZtreeEntity>queryBoundOrganTree(@RequestParam(value="ORGAN_UUID")String ORGAN_UUID){
        logger.info("ORGAN_UUID:" + ORGAN_UUID);
        List<ZtreeEntity> list=OrganService.queryBoundOrganTree(ORGAN_UUID);
        return list;
    }
    @RequestMapping("/saveParentIdByOrganId")
    @ResponseBody
    private void saveParentIdByOrganId(@RequestParam(value="ORGAN_UUID")String ORGAN_UUID,@RequestParam(value="PARENT_UUID")String PARENT_UUID){
        logger.info("ORGAN_UUID:" + ORGAN_UUID,"PARENT_UUID:" + PARENT_UUID);
        OrganService.saveParentIdByOrganId(ORGAN_UUID,PARENT_UUID);
        jsonSuccess("organ.bound,parent.success");
    }
    @RequestMapping("/unboundParentIdByOrganId")
    @ResponseBody
    private void unboundParentIdByOrganId(@RequestParam(value="ORGAN_UUID")String ORGAN_UUID){
        logger.info("ORGAN_UUID:" + ORGAN_UUID);
        OrganService.unboundParentIdByOrganId(ORGAN_UUID);
        jsonSuccess("organ.unbound,parent.success");
    }

    @RequestMapping("/addOrganInfo")
    @ResponseBody
    public String addOrganInfo(PtOrganEntity PtOrganEntity){
        logger.info(PtOrganEntity);
        OrganService.addOrganInfo(PtOrganEntity);
        return jsonSuccess("organ.add.success");
    }


    @RequestMapping("/updateOrganById")
    @ResponseBody
    public String updateOrganById(PtOrganEntity PtOrganEntity){
        logger.info(PtOrganEntity);
        OrganService.updateOrganById(PtOrganEntity);
        return jsonSuccess("organ.add.success");
    }

    @RequestMapping("/DeleteOrganById")
    @ResponseBody
    public String DeleteOrganById(@RequestParam(value="organUuid")String organUuid){
        logger.info(organUuid);
        OrganService.DeleteOrganById(organUuid);
        return jsonSuccess("organ.add.success");
    }

    @RequestMapping("/queryOrganInfoListById")
    @ResponseBody
    public PadingRstType<PtOrganEntity> queryOrganInfoListById(@RequestParam(value="ORGAN_UUID")String organUuid, PaddingEntity padding){
        logger.info(organUuid);
        PadingRstType<PtOrganEntity> rstType = OrganService.queryOrganInfoListById(organUuid,padding);
        return rstType;
    }

    @RequestMapping("/queryDepInfoListById")
    @ResponseBody
    public PadingRstType<PtOrganEntity> queryDepInfoListById(@RequestParam(value="ORGAN_UUID")String organUuid, PaddingEntity padding){
        logger.info(organUuid);
        PadingRstType<PtOrganEntity> rstType = OrganService.queryDepInfoListById(organUuid,padding);
        return rstType;
    }

    @RequestMapping("/queryPostInfoListById")
    @ResponseBody
    public PadingRstType<PtOrganEntity> queryPostInfoListById(@RequestParam(value="ORGAN_UUID")String organUuid, PaddingEntity padding){
        logger.info(organUuid);
        PadingRstType<PtOrganEntity> rstType = OrganService.queryPostInfoListById(organUuid,padding);
        return rstType;
    }
}
