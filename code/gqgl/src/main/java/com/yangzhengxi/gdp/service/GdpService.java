package com.yangzhengxi.gdp.service;

import com.yangzhengxi.gdp.entity.SearchGdpEntity;
import com.yangzhengxi.mybatis.entity.GdpEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;

import java.util.List;

/**
 * @Author yyds
 * @Date 2022/7/4
 * @ClassName:GdpService
 * @Version 1.0
 */
public interface GdpService {
    PadingRstType<GdpEntity> queryGdpInfoListByPage(SearchGdpEntity search, PaddingEntity padding);

    List queryDateData(SearchGdpEntity search);

    List queryUpData(SearchGdpEntity search);
}
