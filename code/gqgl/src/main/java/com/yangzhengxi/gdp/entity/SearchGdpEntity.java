package com.yangzhengxi.gdp.entity;

import java.io.Serializable;

/**
 * @Author yyds
 * @Date 2022/7/4
 * @ClassName:SearchGdpEntity
 * @Version 1.0
 */
public class SearchGdpEntity implements Serializable {
    private String beginDate;

    private String endDate;

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "SearchQuoteEntity{" +
                "beginDate='" + beginDate + '\'' +
                ", endDate='" + endDate + '\'' +
                '}';
    }
}
