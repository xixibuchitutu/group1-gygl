package com.yangzhengxi.gdp.repository;

import com.yangzhengxi.gdp.entity.SearchGdpEntity;
import com.yangzhengxi.mybatis.entity.GdpEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author yyds
 * @Date 2022/7/4
 * @ClassName:GdpDao
 * @Version 1.0
 */
@Mapper
public interface GdpDao {
    List<GdpEntity> queryGdpInfoListByPage(@Param("search") SearchGdpEntity search, @Param("padding") PaddingEntity padding);

    Integer queryGdpInfoTotal(@Param("search") SearchGdpEntity search);

    List queryDateData(@Param("searchData") SearchGdpEntity search);

    List queryUpData(@Param("searchData") SearchGdpEntity search);
}
