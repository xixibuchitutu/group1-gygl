package com.yangzhengxi.gdp.service.impl;

import com.yangzhengxi.gdp.entity.SearchGdpEntity;
import com.yangzhengxi.gdp.repository.GdpDao;
import com.yangzhengxi.gdp.service.GdpService;
import com.yangzhengxi.mybatis.entity.GdpEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.spring.controller.BaseController;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author yyds
 * @Date 2022/7/4
 * @ClassName:GdpServiceImpl
 * @Version 1.0
 */
@Service
public class GdpServiceImpl extends BaseController implements GdpService {

    @Resource
    private GdpDao gdpDao;

    @Override
    public PadingRstType<GdpEntity> queryGdpInfoListByPage(SearchGdpEntity search, PaddingEntity padding) {
        padding.deal(GdpEntity.class);
        PadingRstType<GdpEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(padding.getPage());
        List<GdpEntity> list = gdpDao.queryGdpInfoListByPage(search,padding);
        padingRstType.setRawRecords(list);
        Integer total = gdpDao.queryGdpInfoTotal(search);
        padingRstType.setTotal(total);
        padingRstType.putItems();

        return padingRstType;
    }

    @Override
    public List queryDateData(SearchGdpEntity search) {
        return gdpDao.queryDateData(search);
    }

    @Override
    public List queryUpData(SearchGdpEntity search) {
        return gdpDao.queryUpData(search);
    }
}
