package com.yangzhengxi.gdp.controller;

import com.yangzhengxi.gdp.entity.SearchGdpEntity;
import com.yangzhengxi.gdp.service.GdpService;
import com.yangzhengxi.mybatis.entity.GdpEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.spring.controller.BaseController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author yyds
 * @Date 2022/7/4
 * @ClassName:GdpController
 * @Version 1.0
 */
@Controller
@RequestMapping("/gdpinfo")
public class GdpController extends BaseController {

    private Logger logger = LogManager.getLogger(this.getClass());

    @Resource
    private GdpService gdpService;

    @Secured("ROLE_gqgy_data_gdp")
    @RequestMapping("/page")
    public String gdpPage() {
        return "gdpinfo/page";
    }

    @RequestMapping("/queryGdpInfoListByPage")
    @ResponseBody
    public PadingRstType<GdpEntity> queryGdpInfoListByPage(SearchGdpEntity search, PaddingEntity padding){
        logger.info(search);
        logger.info(padding);
        PadingRstType<GdpEntity> padingRstType = gdpService.queryGdpInfoListByPage(search,padding);

        return padingRstType;
    }

    @RequestMapping("/queryDateData")
    @ResponseBody
    public List queryDateData(SearchGdpEntity search) {
        logger.info(gdpService.queryDateData(search));
        return gdpService.queryDateData(search);
    }

    @RequestMapping("/queryUpData")
    @ResponseBody
    public List queryUpData(SearchGdpEntity search) {
        logger.info(gdpService.queryUpData(search));
        return gdpService.queryUpData(search);
    }
}
