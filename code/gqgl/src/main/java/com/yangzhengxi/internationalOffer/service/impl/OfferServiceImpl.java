package com.yangzhengxi.internationalOffer.service.impl;

import com.yangzhengxi.internationalOffer.entity.SearchOfferEntity;
import com.yangzhengxi.internationalOffer.repository.OfferDao;
import com.yangzhengxi.internationalOffer.service.OfferService;
import com.yangzhengxi.mybatis.entity.InternationalOfferEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.spring.controller.BaseController;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class OfferServiceImpl extends BaseController implements OfferService {

    @Resource
    private OfferDao offerDao;

    @Override
    public PadingRstType<InternationalOfferEntity> queryOfferInfoListByPage(SearchOfferEntity search, PaddingEntity padding) {
        padding.deal(InternationalOfferEntity.class);
        PadingRstType<InternationalOfferEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(padding.getPage());
        List<InternationalOfferEntity> list = offerDao.queryOfferInfoListByPage(search,padding);
        padingRstType.setRawRecords(list);
        Integer total = offerDao.queryOfferInfoTotal(search);
        padingRstType.setTotal(total);
        padingRstType.putItems();

        return padingRstType;
    }
}