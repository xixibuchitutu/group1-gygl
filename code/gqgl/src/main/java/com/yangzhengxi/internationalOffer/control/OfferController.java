package com.yangzhengxi.internationalOffer.control;

import com.yangzhengxi.internationalOffer.entity.SearchOfferEntity;
import com.yangzhengxi.internationalOffer.service.OfferService;
import com.yangzhengxi.mybatis.entity.InternationalOfferEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.spring.controller.BaseController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/offerinfo")
public class OfferController extends BaseController {

    private Logger logger = LogManager.getLogger(this.getClass());

    @Resource
    private OfferService offerService;

    @Secured("ROLE_gqgy_data_offer")
    @RequestMapping("/page")
    public String offerPage() {
        return "offerinfo/page";
    }

    @RequestMapping("/queryOfferInfoListByPage")
    @ResponseBody
    public PadingRstType<InternationalOfferEntity> queryOfferInfoListByPage(SearchOfferEntity search, PaddingEntity padding){
        logger.info(search);
        logger.info(padding);
        PadingRstType<InternationalOfferEntity> padingRstType = offerService.queryOfferInfoListByPage(search,padding);

        return padingRstType;
    }
}
