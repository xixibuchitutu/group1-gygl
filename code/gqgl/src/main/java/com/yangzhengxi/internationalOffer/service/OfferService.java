package com.yangzhengxi.internationalOffer.service;

import com.yangzhengxi.internationalOffer.entity.SearchOfferEntity;
import com.yangzhengxi.mybatis.entity.InternationalOfferEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;

public interface OfferService {
     PadingRstType<InternationalOfferEntity> queryOfferInfoListByPage(SearchOfferEntity search, PaddingEntity padding);

}
