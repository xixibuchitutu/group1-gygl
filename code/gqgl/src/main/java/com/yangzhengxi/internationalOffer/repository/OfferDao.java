package com.yangzhengxi.internationalOffer.repository;

import com.yangzhengxi.internationalOffer.entity.SearchOfferEntity;
import com.yangzhengxi.mybatis.entity.InternationalOfferEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OfferDao {
    List<InternationalOfferEntity> queryOfferInfoListByPage(@Param("search") SearchOfferEntity search, @Param("padding") PaddingEntity padding);

    Integer queryOfferInfoTotal(@Param("search") SearchOfferEntity search);

}
