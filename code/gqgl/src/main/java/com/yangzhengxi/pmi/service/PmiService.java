package com.yangzhengxi.pmi.service;

import com.yangzhengxi.pmi.entity.SearchPmiEntity;
import com.yangzhengxi.mybatis.entity.PmiEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;

import java.util.List;

public interface PmiService {
    PadingRstType<PmiEntity> queryPmiInfoListByPage(SearchPmiEntity search, PaddingEntity padding);

    List queryDateData(SearchPmiEntity search);

    List queryValueData(SearchPmiEntity search);
}
