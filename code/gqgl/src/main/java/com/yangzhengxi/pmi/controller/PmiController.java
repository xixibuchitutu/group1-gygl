package com.yangzhengxi.pmi.controller;

import com.yangzhengxi.pmi.entity.SearchPmiEntity;
import com.yangzhengxi.pmi.service.PmiService;
import com.yangzhengxi.mybatis.entity.PmiEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.spring.controller.BaseController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/pmiinfo")
public class PmiController extends BaseController {

    private Logger logger = LogManager.getLogger(this.getClass());

    @Resource
    private PmiService pmiService;

    @Secured("ROLE_gqgy_data_pmi")
    @RequestMapping("/page")
    public String gdpPage() {
        return "pmiinfo/page";
    }

    @RequestMapping("/queryPmiInfoListByPage")
    @ResponseBody
    public PadingRstType<PmiEntity> queryPmiInfoListByPage(SearchPmiEntity search, PaddingEntity padding){
        logger.info(search);
        logger.info(padding);
        PadingRstType<PmiEntity> padingRstType = pmiService.queryPmiInfoListByPage(search,padding);

        return padingRstType;
    }

    @RequestMapping("/queryDateData")
    @ResponseBody
    public List queryDateData(SearchPmiEntity search) {
        logger.info(pmiService.queryDateData(search));
        return pmiService.queryDateData(search);
    }

    @RequestMapping("/queryValueData")
    @ResponseBody
    public List queryValueData(SearchPmiEntity search) {
        logger.info(pmiService.queryValueData(search));
        return pmiService.queryValueData(search);
    }
}
