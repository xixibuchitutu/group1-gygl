package com.yangzhengxi.pmi.service.impl;

import com.yangzhengxi.pmi.entity.SearchPmiEntity;
import com.yangzhengxi.pmi.repository.PmiDao;
import com.yangzhengxi.pmi.service.PmiService;
import com.yangzhengxi.mybatis.entity.PmiEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import com.yangzhengxi.mybatis.flexigrid.PadingRstType;
import com.yangzhengxi.spring.controller.BaseController;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PmiServiceImpl extends BaseController implements PmiService {

    @Resource
    private PmiDao pmiDao;

    @Override
    public PadingRstType<PmiEntity> queryPmiInfoListByPage(SearchPmiEntity search, PaddingEntity padding) {
        padding.deal(PmiEntity.class);
        PadingRstType<PmiEntity> padingRstType = new PadingRstType<>();
        padingRstType.setPage(padding.getPage());
        List<PmiEntity> list = pmiDao.queryPmiInfoListByPage(search,padding);
        padingRstType.setRawRecords(list);
        Integer total = pmiDao.queryPmiInfoTotal(search);
        padingRstType.setTotal(total);
        padingRstType.putItems();

        return padingRstType;
    }

    @Override
    public List queryDateData(SearchPmiEntity search) {
        return pmiDao.queryDateData(search);
    }

    @Override
    public List queryValueData(SearchPmiEntity search) {
        return pmiDao.queryValueData(search);
    }
}
