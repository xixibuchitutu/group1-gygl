package com.yangzhengxi.pmi.repository;

import com.yangzhengxi.pmi.entity.SearchPmiEntity;
import com.yangzhengxi.mybatis.entity.PmiEntity;
import com.yangzhengxi.mybatis.flexigrid.PaddingEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PmiDao {
    List<PmiEntity> queryPmiInfoListByPage(@Param("search") SearchPmiEntity search, @Param("padding") PaddingEntity padding);

    Integer queryPmiInfoTotal(@Param("search") SearchPmiEntity search);

    List queryDateData(@Param("searchData") SearchPmiEntity search);

    List queryValueData(@Param("searchData") SearchPmiEntity search);
}
